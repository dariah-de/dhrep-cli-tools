# Repository CLI Tools

Here are tools which are used for administrative work on the DH and TG Repositories. Please also have a look at these pages: <https://intranet.sub.uni-goettingen.de/display/SUBdev/textgrid-esx2.gwdg.de> and <https://intranet.sub.uni-goettingen.de/display/SUBdev/repository.de.dariah.eu> (internal only).

## Content

[[_TOC_]]

## Build

You should build the shaded jars to have all dependencies on the classpath

```bash
mvn clean package -Pshaded
```

## Use

The tools all provide some cli help on their usage and parameters, please test:

```bash
java -jar target/cli-tools.jar
```

or

```bash
java -jar target/cli-tools.jar sandboxer
```

## The TextGrid Repository Tools

### TextGrid Sandboxer

After reindexing the repository into elasticsearch there are no sandbo (nearlyPublished) flags in elasticsearch. This tool adds them by querying sesame

```bash
java -jar target/cli-tools.jar sandboxer /etc/dhrep/tgsearch/tgsearch-public.properties -h 300000
```

The number 300000 in the end is the limit of the sparql query, set it high enough to process all sandbox items.
Clean the sandbox if you need to set to high counts!

### TextGrid EWMD (Edition/Work Metadata)

Elasticsearch will have no edition and work metadata available for single textgrid items.
Instead of trying complicated joins we decided to duplicate the metadata to make facets
on authors or other edition/work fields possible on TEI-fulltext searches in the public repository (on <https://textgridrep.org>). ewmd takes care about this.

```bash
java -jar target/cli-tools.jar ewmd /etc/dhrep/tgsearch/tgsearch-public.properties
```

The default time range is from 2010-01-01 until now, but it is possible to specify a time range:

```bash
java -jar target/cli-tools.jar ewmd /etc/dhrep/tgsearch/tgsearch-public.properties --from 2021-01-01 --to 2021-06-01
```

where the first is the start and the second the end date.

### TextGrid Digilib Prescale

Use this tool to fill or update the digilib prescale directory. It gets a list of TextGrid objects where format starts
with "image" and asks TG-crud to send an update-notification to the prescaler-messagebean calling the recache-operation.

```bash
java -jar target/cli-tools.jar prescale /etc/dhrep/tgsearch/tgsearch-public.properties
```

The default timerange is from 2010-01-01 until now, but it is possible to specify a timerange:

```bash
java -jar target/cli-tools.jar prescale /etc/dhrep/tgsearch/tgsearch-public.properties --from 2012-01-06 --to 2012-01-07
```

Where the first is the start and the second the end date. Choose a tgsearch-nonpublic.properties file for prescaling
nonpublic images.

### TextGrid Find Objects

Query the TextGrid Repository's ElasticSearch using format and last modified date filters for identifying the URIs of certain TextGrid objects.

Usage see

```bash
java -jar target/cli-tools.jar find-objects --help
```

### TextGrid Consistency Check

Uses the _TG-crud#CHECKCONSISTENCY_ method to check for filesize and checksum consistency on storage, TextGrid metadata, and Handle metadata level:

```bash
java -jar target/cli-tools.jar check-consistency -p TGPR-9b18459d-3a6b-004f-b6aa-4fba2b9e1d3e
```

Use TextGrid project ID, mimetype, and custom filters on a TG-search query to define TextGrid objects to check.

To combine author query and project ID filter, and additional alphabetical order with more verbose output, you can use:

```bash
java -jar target/cli-tools.jar check-consistency -q edition.agent.value:"schopenhauer" -p TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c -v -o
```

You can also add more filters using the _-filter_ parameter, and/or still apply the _-mimetype_ switch! All queries are processed by TG-search, so please look for query and filter documentation at the [TG-search API Documentation](https://textgridlab.org/doc/services/submodules/tg-search/docs/index.html#api-documentation).

Results are logged on stdout and written to a log file in CSV format. Can be used from external machines to generate the log file to call _fix-inconsistencies_ later on. Outcome is identified in different cases:

For more information please call

```bash
java -jar target/cli-tools.jar check-consistency --help
```

At the moment ten different outcomes of inconsistency constellations are known:

#### [case:0]

You got an unknown case: A new case must be implemented!

#### [case:1]

OK! Everything is fine here: Filesizes and checksums are equal in storage, TextGrid metadata, and Handle metadata! Great!

#### [case:2]

No handle PID found in TextGrid metadata at all!

#### [case:3]

- No handle filesize: [fs:2571 2571 NOT_FOUND]
- No handle checksum: [cs:md5:29551f6508c740d926c21f1eab330120 md5:29551f6508c740d926c21f1eab330120 --

#### [case:4]

- No handle filesize: [fs:413992 413992 NOT_FOUND]
- No metadata and no Handle checksum: [cs:md5:52b5cd0ca3ab3e377147fc05e603a1d9 NOT_FOUND NOT_FOUND

#### [case:5]

- Different handle filesize: [fs:11 11 11674866]
- Different handle checksum: [cs:md5:c520c311494e61ef13c4961a3238845c md5:c520c311494e61ef13c4961a3238845c md5:cc11f6ed06ba8ede50b97b0a284fbfb6]

#### [case:6]

- Two filesizes equal, handle filesize is -1: [fs:582 582 -1]
- Only storage checksum: [cs:md5:ca86416c0db5b33b0472730efc317fa6 -- --

#### [case:7]

- Storage and metadata filesizes differ, Handle filesize is -1: [fs:13056 13038 -1]
- Only storage checksum: [cs:md5:cc8334fe9a28e950702af8090f2c3cf1 -- --]

#### [case:8]

- Filesizes ok: [fs:ok]
- Handle checksum differs: [cs:md5:3f3fbecb584152c3e3c3814eb3734d0e md5:3f3fbecb584152c3e3c3814eb3734d0e md5:6c293a866e9cb096528eed011e1a378b]

#### [case:9]

- No handle filesize: [fs:11 11 NOT_FOUND]
- Checksums OK: [cs:md5:c520c311494e61ef13c4961a3238845c md5:c520c311494e61ef13c4961a3238845c md5:c520c311494e61ef13c4961a3238845c]

### TextGrid Fix Inconsistencies

Please use this method to fix inconsistencies found with the _check-consistency_ method, or with a separate URI list.

Mainly we take the filesize and chechmsum from the original data object/storage file and set in (i) TextGrid metadata, (ii) Handle Metadata, and (iii) do a re-index of TextGrid metadata as in TG-crud#UPDATEMETADATAE for adding correct relations and/or other additional metadata, such as EXIF.

```bash
java -jar target/cli-tools.jar fix-inconsistencies -i 1726755841300-cc-log.csv -v
```

or with an URI list:

```bash
java -jar target/cli-tools.jar fix-inconsistencies -i uri-list.txt -v
```

**Note**: An URI list must contain one TextGrid URI per line, such as:

```txt
textgrid:1234.0
textgrid:2345.6
textgrid:4567.9
```

**Second note**: Use the -r flag to **REALLY FIX INCONSISTENCIES!** Please be careful! A dry run is the default setting! Please test first!

The CSV file from _check consistency_ will be taken as input file and all inconsistent filesizes and checksums will at first be corrected in:

- Handle Metadata (via API), and
- TextGrid metadata (on disk!).

**Third note**: The _fix-inconsistencies_ script **MUST** be run locally on the TextGrid machine to fix TextGrid metadata directly in the storage!

That's it! A logfile will be written for each fixing process. The TextGrid metadata file fix can be tested using the _check-consistency_ with _-d_ param to download TextGrid metadata files locally before fixing. Please set the _tgmeta.path_ parameter in the config to the local metadata download folder of your choice, such as _/home/fuu/dhrep-cli-tools/metadata-download/_.

For more information please call

```bash
java -jar target/cli-tools.jar fix-inconsistencies --help
```

#### Example for “Wirkungsgeschichte von Goethes Werk „Zur Farbenlehre“ in Berlin 1810-1832“ (TGPR-edc6d8fe-374d-0082-9202-62976ac3441d)

```bash
# go to opt dhrep folder
cd /opt/dhrep/

# create new folder for fixing documentation
mkdir 2024-12-18-fix-farbenlehre
cd 2024-12-18-fix-farbenlehre

# check consistency for all objects of one project
java -jar ../../cli-tools.jar check-consistency -p TGPR-edc6d8fe-374d-0082-9202-62976ac3441d -v

# fix inconsistencies (first in dry-run mode with a few objects only!)
sudo java -jar ../../cli-tools.jar fix-inconsistencies -i 1731324611932-cc-log.csv -v -n 42

# really fix inconsistencies for all objects!
sudo java -jar ../../cli-tools.jar fix-inconsistencies -i 1731324611932-cc-log.csv -v -r
```

#### URLs for testing and checking the fix (before and after)

- TextGrid metadata (TG-crud from storage): <https://dev.textgridlab.org/1.0/tgcrud/rest/textgrid:3zcn6.0/metadata>
- TextGrid consistency: <https://dev.textgridlab.org/1.0/tgcrud/rest/textgrid:3zcn6.0/consistency>
- TextGrid metadata (TG-seaerch from ElasticSearch): <https://dev.textgridlab.org/1.0/tgsearch-public/info/textgrid:3zcn6.0/metadata>
- Handle metadata (from Handle service, PID from TextGrid metadata): <https://hdl.handle.net/21.T11991/0000-001C-2D86-9?noredirect>

## The DARIAH-DE Repository Tools

### DH-rep Digilib Prescale

Use this tool to fill or update the DHREP digilib prescale directory. It gets a list of DARIAH-DE repository objects where format starts
with "image" and asks DH-crud to send an update-notification to the prescaler-messagebean calling the recache operation.

```bash
java -jar target/cli-tools.jar prescale-dhrep /etc/dhrep/oaipmh/oaipmh.properties -p 9093
```

The default timerange is from 2010-01-01 until now, but it is possible to specify a timerange:

```bash
java -jar target/cli-tools.jar prescale-dhrep /etc/dhrep/oaipmh/oaipmh.properties -p 9093 --from 2012-01-06 --to 2012-01-07
```

Where the first is the start and the second the end date. Choose a dhcrud properties file for prescaling images.

### DH-rep Delete

There is a [deletion policy](https://projects.academiccloud.de/projects/dariah-de-intern/wiki/loschung-von-repository-inhalten) and a [deletion workflow](https://projects.academiccloud.de/projects/dariah-de-intern/wiki/delete-workflow-fur-publizierte-objekte) defined for deleting objects from the DARIAH-DE Repository, _dhrep-delete_ provides functions for archieving a clean delete from all reposirtory databases and registries.

Usage see

```bash
cd /opt/dhrep/
./cli-tools dhrep-delete --help
```

or, non-native jar usage:

```bash
java -jar target/cli-tools.jar dhrep-delete --help
```

Please start dhrep-delete as _root_ or use _sudo_ to have all the needed access rights! You can use the configuration file `src/main/resources/dhrep.properties`, that contains just general settings for dhrep servers and config file locations. The following stages are performed:

#### STAGE I: Preflight

Preflight just reads config files, checks settings and service availability, and finally creates a list of objects to delete.

#### STAGE II: Process HDL Metadata Modifications

Handle metadata is being read from the handle server for every object to delete, a citation string is assembled from handle metadata, administrative and descriptive metadata, such as _Fugu Fischinger, Stefan E. Funk (2021). DELETED DRAGON ON DHREPWORKSHOP. DARIAH-DE. <https://doi.org/10.20375/0000-001b-4650-a>_ and added to the handle metadata as the value of a newly created _DELETED_ metadata key.

#### STAGE III: Process DOI Metadata Deletion

DOI metadata is set to hidden for every object, so they cannot be found by Datacite search.

#### STAGE IV: Deleting from Public Storage

A list of objects is being provided to relay it to the GWDG support to delete every object from public storage.

#### STAGE V: Deleting ElasticSearch Index Data

Metadata is being deleted from the ElasticSearch index. Objects will not be found anymore in OAI-PMH, DH-crud, or other related services.

#### STAGE VI: Deleting Collections from IIIF Manifest Cache

This deletes all related IIIF manifests from the IIIF manifest cache.

#### STAGE VII: Deleting Images from Digilic Cache

This deletes all related images from the Digilib image cache.

#### STAGE VIII: Deleting from DHREP Collection Registry

A link is provided to the DH-rep Collection Registry to delete the root collection (after login), maybe a API wil be provided for that later.

#### STAGE IX: Still TO DO

Things still to do will be listed (again), mainly from stages IV and VIII.

### DH-rep Inspect

Inspect objects from the DARIAH-DE Repository. You can use collection and object URIs to check the status of any object.

Usage see

```bash
cd /opt/dhrep/
./cli-tools dhrep-inspect --help
```

or, non-native jar usage:

```bash
java -jar target/cli-tools.jar dhrep-inspect --help
```

Stages are roughly the same as in dhrep-delete. I would say.

## Building Native Image with GraalVM

For providing the tool on the dhrep/textgrid servers we build a [native image with graalvm](https://www.graalvm.org/22.1/reference-manual/native-image/),
utilizing the [graalvm-maven-plugin](https://graalvm.github.io/native-build-tools/0.9.7.1/maven-plugin.html).
This executable is distributed as debian package.

For building a native image some config for [reflection is needed](https://www.graalvm.org/22.1/reference-manual/native-image/Reflection/).

For now we generate this config files manually running the shaded-jar once and keep this [config in this repo](./graalconfig)
like this:

```bash
~/java/graalvm/graalvm-ce-java17-22.2.0/bin/java -agentlib:native-image-agent=config-output-dir=graalconfig -jar target/cli-tools.jar prescale
```

Best would be to generate this config with the ci runnnig unit tests (this also would need a test coverage of 100% then!), like it is [described in the maven-plugin manual](https://graalvm.github.io/native-build-tools/0.9.7.1/maven-plugin.html#agent-support).

## More Notes on Tracing Dependencies for Native Image

- get the graalvm java lib from <https://github.com/graalvm/graalvm-ce-builds/releases/tag/vm-22.2.0>

- untar, and install native image with `gu`

    ```bash
    ~/graalvm-ce-java17-22.2.0/bin/gu install native-image
    ```

- do we need to set the path? if so, then:

    ```bash
    export JAVA_HOME=~/graalvm-ce-java17-22.2.0/
    ```

- read <https://www.graalvm.org/22.0/reference-manual/native-image/Agent/>

 if locally graalvm calls do not succeed (as long there is)
