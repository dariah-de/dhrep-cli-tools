## [4.9.1](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/compare/v4.9.0...v4.9.1) (2024-12-19)


### Bug Fixes

* add suffix to URI list input file as output logfile name ([2825fbc](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/2825fbc8f5b4291ec0c0def23e7febdd448ee46a))

# [4.9.0](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/compare/v4.8.0...v4.9.0) (2024-12-09)


### Bug Fixes

* add CR to uris in list ([2c6f3de](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/2c6f3deef1036a9e3317e118590d9170fae05ea6))
* remove doubled log ([ef889e9](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/ef889e92dd5b26a45421c9249baf6579f8067c41))


### Features

* add uri list storing ([b44d800](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/b44d800c3669359214637b6d6af0165700b43e64))
* add uri list usage to input file  parameter ([8d4ebdf](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/8d4ebdf79454b734e0cc52339b4adeba4d0d23b8))

# [4.8.0](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/compare/v4.7.0...v4.8.0) (2024-11-28)


### Bug Fixes

* ad dry run for fixing, add metadata file path to config ([ca3dca6](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/ca3dca6eb1d1eb4646000c444e8a2af9ba32b69e))
* add 900 ([6f4c154](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/6f4c154d798902987b5c56764f292e8544cfa5e8))
* add case 8 ([29b9dbd](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/29b9dbdd47c319fc20a6f0304a91722402adbfab))
* add case 9: no pid at all ([0b7c0d1](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/0b7c0d1f7fc3e22eb8205fd5cfecf880c1409236))
* add custom filter ([70b180f](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/70b180faa4cee3e13e9ad5501c815bab7a3fb315))
* add doc ([44b84f7](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/44b84f720e8f49f42e903d0c6a090ac3427e33bf))
* add dry-run to dry-run logfile name ([851659b](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/851659b131cd7059e41b54ae197045d6d13027d4))
* add dryrun check for reindexing ([8e5d91f](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/8e5d91f6bc9a44d88abcad319979b793976d1f4a))
* add examples to cli help ([a18e3cd](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/a18e3cd7dd274fbbc169c34cebf7dcd8df3eaffa))
* add filter list as paremeter ([8765515](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/87655153bb5aca1c060f76f7cba94c60925e7f6f))
* add fixPrefixOnly param ([a60eba9](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/a60eba938eed63846c4752b58df72d9b1c2d79fd))
* add java textblock for cases doc ([5d6eaaa](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/5d6eaaa86a38836373af4c2e477a5cfeb81ddc9c))
* add log file content ([a1596ab](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/a1596ab2413ad13364a9675511b4447d1cf90b44))
* add magic number doc for '-99‘ ([0bb1a36](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/0bb1a36320a6b817d496a6cbb731a45644a05ae0))
* add metadata download feature, and fix some other things ([65c548b](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/65c548b886ef5f4d65629f17e0f7df6df62acad6))
* add metadata file check and correct metadata file path ([471993d](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/471993d17a9f49f9de7c97491a57cc2066af3f7a))
* add metadata file updates with --reallly-fix param, enhance logging ([261798f](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/261798fb8ea6c5ecbb8427589137cc042c3b39e8))
* add method for fixing things (some refactorings, too, sorry...) ([3bfe7aa](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/3bfe7aa70940b00edfa04fa67bbc9519b14c57f5))
* add more limit computations ([6e567df](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/6e567df71cd03d49ac3fcafdb0ce02c474c6189a))
* add new methods and class files ([1a29bd9](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/1a29bd9c0bfa1ac36cd7a4d6b2f34069dfce9b88))
* add number param for fix inconsistencies, too ([a7328b8](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/a7328b8ab130f20bf38364b4b4aed5c12a5d9ece))
* add number param for only check n objects ([24bedae](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/24bedae90c20c2a778e404ddb60e73fde5ad6abd))
* add order param ([d2c9b75](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/d2c9b75e840a756461d60572f753c13fb443fdd0))
* add README, fix GOAWAY issue with HTTP2 client ([ce96d2c](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/ce96d2c4dfd42355d0bd864a8f106fefdd66f50e))
* add reindex secret ([dfb62a0](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/dfb62a0a604161e667e0162897ce21e755428094))
* add some ([83350d5](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/83350d553d4e8499f75dc832422a64aeac5f9de7))
* add sort order:tguri ([4ba70bf](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/4ba70bfdaec6724ded749b298cdc7a9361520217))
* add t and h param for fixing hdl and tg metadata only ([c75737d](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/c75737d8c79cdf258d48d1606a37284b8d03b2cb))
* add warning if handle metadata update error ([20e50ff](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/20e50ffd84c19d7118b464242cf4183fd8bac3d0))
* ans dill lore case mogging ([7f356a7](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/7f356a708f9d42b75f06d37480059c455b357639))
* avoid npe ([1a2ad77](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/1a2ad77453dbccdf5d5e484896fff0325564eb25))
* catch wae? ([56b833e](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/56b833e863062d83b7c5d75743d3d3068a4abc99))
* check if we must fix hdl or metadata at all, not to update without cause ([3de4ccc](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/3de4ccc1b95b64739df80921dea0e68407a28d2a))
* check settings ([be68e6f](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/be68e6f09590fef888af4c977e48e0ee52127da9))
* correct re-indexing ([7e54f3f](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/7e54f3f92e2dbc1a87420b07cec690a59f09c72f))
* decrease limit to 100 again ([e450343](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/e450343f7db5683142618f93b3233eea808d4919))
* fix build failures due to version class generation ([a98b3dc](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/a98b3dc177dfc2723d0a38126158ebae21b48ee1))
* fix cond ([eb6a3e9](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/eb6a3e9139b5c98ef6cd90bb96f2e4721a433086))
* fix conditional ([004585b](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/004585b6b7bbfa4eaa509c9bb569f6430859fff5))
* fix counting ([1b83861](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/1b838616a61ced3b280126568b372450866c9620))
* fix fixity and extent re-set ([ae6842e](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/ae6842eebef27d6da6178ccb4fc00f2b19eb102d))
* fix fixity string check ([a2b7a44](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/a2b7a4432bd48bbb79f15444dd0f21a06f46d0a2))
* fix hits and number ([863563b](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/863563be1e6583e77ae34980778fed9a3f94c95b))
* fix lambda uri extraction ([096b10d](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/096b10da1fe9007e5281f370a191dc1bf8afb33f))
* fix logfile ([aba38d5](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/aba38d5e230f25f118d6445ba0f01ffe790c5a2b))
* fix logging ([42328e7](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/42328e798ed0ed26079ce822b8305a992817ecd7))
* fix metadataOnly triggers ([e3a2e56](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/e3a2e567fa31b642dd878f264172e0cb83bfaec7))
* fix more cond ([c7b837f](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/c7b837fb33b63064b874cbfe8e57593994726974))
* fix no handle error ([14db379](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/14db3798d0748be119e180261c2fbc52ecb379b7))
* fix param ([d4d31b5](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/d4d31b58c8d6e303cb4c30e1e829551865c97660))
* fix params ([83cb14e](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/83cb14e805cda2954fe28d6dddad4471e078e6e7))
* fix reindex metadata call ([13f9485](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/13f9485361842416134b5bea33910fa93554aaab))
* fix reindex secret setting ([a1b488f](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/a1b488ff6d08a3d9913e8a6a585f626e27d9c623))
* fix some logging ([b7b89b2](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/b7b89b2123509fa11d54f42fa2a8b2208e4da88c))
* fix some of the review issues ([a95dfd6](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/a95dfd64cb8761f00cf9a471dc3ef86f4efc3fb7))
* fix some project versions ([5133298](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/5133298391a219ec3bdc1ce764f322588e8b773d))
* habe ein paar methoden ausgelagert ([be7d46c](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/be7d46cb36cfb002a534e03ccad025587363dc3c))
* handle missing Handle in consistency response ([94e769e](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/94e769e2c19e150a23eb264ba458508907190817))
* hrgs! still more logging ([3a62690](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/3a62690a38b21fd6757db85266eb68f96c97b7c2))
* implement new method ([f9c548d](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/f9c548d0befcfafd51e66dd7dd15851b83f72dc8))
* increase tgsearch limit ([16ddabe](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/16ddabe3e6439d2be46b6e3f5829e6f81d0e6c62))
* increase to release versions ([343d9ac](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/343d9ac45b7b1ce1b20fbb192cc2e6df542791ed))
* jo ([2a12359](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/2a123593713110b482863915bec536e43020fce0))
* latest logging changes ([7dbd8f4](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/7dbd8f42a739d72a72b770ceb16058f122b8d703))
* mc ([47c7a3b](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/47c7a3b2591cb4de89fbf41ebf495107bdbfed87))
* mc ([8e12927](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/8e129277920d7866fc7603c6820f6c4243e5f71b))
* mc ([700d62a](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/700d62ae6da211b0b9e660974d0f01b125f8f557))
* mc ([5936849](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/5936849d929aff273883a3e49f99be18dd14b478))
* mc ([4e2f91f](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/4e2f91ff95d9e859e0f389550070e6691c06d9ac))
* mmmccccc!!!!!AAAHHH!!! ([de0a08a](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/de0a08a0dc0614aabbae195ac3efb01dc1850527))
* more fixes and loggings ([de6e8a6](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/de6e8a68e23871d42402b78e77e54f8f34603f87))
* more prefix fixing ([7e65911](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/7e65911cacb30010c90d69268eca1db2238e07f7))
* more to fix ([227b207](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/227b207383584f0e899c8bddf28e77cfad20c035))
* refactor check ([acc5562](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/acc556218b3be4116d84eaf931c6633a915960b6))
* sort uri lists ([50aaa67](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/50aaa67fb4e629052bddb3b6a6412036001adcc8))
* take out filter for both tg and hdl metadata ([2d795b0](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/2d795b02f8b2efcf5a536218a71e46ce160eb2c5))
* update comment ([0e42bbe](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/0e42bbed19e7bab2e9dd56a6851d0f514b2cf01e))
* use lambda for getting uri list ([b869994](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/b8699948c0d2270adc7fd722a6c33f0d3fad002b))
* yeah! ([ef514d0](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/ef514d0f219f5b1ed19feab34ec4a75babc1a41a))
* yeah? ([dc15412](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/dc1541210de8103b051db02d5b08a22d48b399ab))


### Features

* add consistency check feature for tgrep projects ([d4f4553](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/d4f4553877349c32bbc8986a77479aa7aae84c8e))
* add fix inconsistencies to readme ([0d1cc4d](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/0d1cc4dcd807542f2e723388db6f54338f3ee1ee))

# [4.7.0](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/compare/v4.6.0...v4.7.0) (2024-06-07)


### Bug Fixes

* adapt tgrep esquery name and headers ([6fd334d](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/6fd334d2a9c3c685771f92108009e929dd50cbdd))
* add correct comment ([b50dd0d](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/b50dd0d255b86449107cd4f91d1a182a0207d0b5))
* add editorconfig and format pom file ([df2bffc](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/df2bffc451a3fa6da4d31d812bed5e5bac63c3ca))
* add findObjects, including new logger ([4d638e2](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/4d638e2cf1227f0e458dcea9d23aaa421c7e27d4))
* add limit warning ([18bb387](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/18bb38729fda619039ae790f48bff51297369731))
* add new editorconfig ([5269ba5](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/5269ba5052747137cf6b944245547219a3c33be5))
* add to readme ([34c9320](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/34c93201b13a45a0983ec8363c3fb249aba4839d))
* add warning if in -v mode ([681b790](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/681b790d4b50b6f9bf34674247f05b07d9a3fb76))
* do not print out URIs on level DEBUG (for better debugging... :-) ([6a04d9e](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/6a04d9edad951a13c5572d59549bbed1a3ad8c3c))
* first implementation of tgesquery ([7666c67](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/7666c676dbaafeebc65d5d4498c00d05f5edbcfa))
* fix default config file ([0e9ff73](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/0e9ff732b9c4f119f5b0c572c8512b662b7b762f))
* fix ubbo's nice vorschlag :-) ([2723987](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/272398746d740568c3c245b4d767a56b618fab69))
* remove apache http header package ([e3278e1](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/e3278e111f7550b7031b674e9104d857ef7c9799))
* removoing non-standart debug output behaviour (Ubbo KNOWS WHERE THE CODE IS! :-) ([1ad8c50](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/1ad8c507854758d0919fb3d928d70e2e6ac7001e))
* use current mvn docker image ([ac63064](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/ac630648f7b6e2fd83bcfc7f8da88ffad2724ae9))
* using multi-release fixes https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/-/issues/11 ([7b91bd6](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/7b91bd63ab7fc1294ec1d607d05ca8f188c12247))
* warn on stderr, so no out URIs are appended to pipes ([64e7cdf](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/64e7cdf4ddf41ff69fbba43d28037015317b379d))


### Features

* using versions from common, increase es version, increase other versions ([7a19da4](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/7a19da49836b1839eb30c72035acb7970bebb1a2))

# [4.6.0](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/compare/v4.5.0...v4.6.0) (2023-11-14)


### Bug Fixes

* add boolean param for verbose logging in dispatcher ([58d8495](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/58d8495b5497fb7c600c43a8be98ca1d5298e4b5))
* add default config file location ([5f8264b](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/5f8264bafb65f4bdff5f25a67158c02eb35978bb))
* add default config file, again... :-) ([141b648](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/141b64853d4b379586896f27baebdf7e401a47d4))
* add dhcrud client ([2bd4577](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/2bd457752246a7a9fce157412e4382aa94d673b3))
* add dhcrud config option, remove repo_url ([1691c95](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/1691c953de1b293155366d3406b774885d15e98f))
* add dhcrud init ([72f86a4](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/72f86a4bcf052b288080541d39551142af1c08a6))
* add digilib cache deletion, and es deletion info ([610e54b](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/610e54bf52bda9919f9572eba4ef1c4256b0fcd2))
* add doi check and adapt new dhrep-delete method ([c7a903f](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/c7a903f8314cb2afe3389a5e1e9d0a52ba571278))
* add groupid again... why did I delete it in the first place?? ([57e2f49](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/57e2f496a83a02907cae9db74bcaccf88f73124d))
* add HDL delete metadata handling ([f8e2e74](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/f8e2e74b97ac68803b864015f3848817ebfe9493))
* add iiif manofest and digilib inspection methods ([213c625](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/213c6258d2a038e0d7cd5d754fe517d2568bf36a))
* add iiifmd and digilib cache file deletion ([f62d182](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/f62d182245bb37946aa9081e967abd9cdb588b69))
* add inspecting public storage ([da40607](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/da406076cbfd7fc6fad5c7029f51216e13efc925))
* add logging ([5b69526](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/5b695261aaa09258f863d0c7b83999676df13151))
* add logging for future stages to be implemented ([3f85473](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/3f8547399fb0dd5de563d6d26deafec1de58a397))
* add method inspect doi metadata ([9e0bd6c](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/9e0bd6c7011fadbc84b2e2468de271256822f947))
* add more configuration and config evaluation ([c79deb7](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/c79deb70c69f4e67f13dd177458348e2412dc8d5))
* add more es and redis deletion ([4814c73](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/4814c73839e78ce92fab128217168389812b7c43))
* add new graalvm reflection config ([1818bf5](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/1818bf541a12cdc40b494089ee0b3aadf5873905))
* add redis db handling ([14c5492](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/14c5492c14ffc75966a3a19ea74de4be6c70eee2))
* add reflection for Dispacher params ([7f20d74](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/7f20d745a6691d972f588dbf210e01d1ed3484f7))
* add shaded jar to target for additional deb packaging ([28ca0c1](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/28ca0c1fd5f0ed948afbb9d9a9cdc086f044a1ee))
* add STAGE I: PREFLIGHT ([b1575e9](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/b1575e9e7f2ddd6b6d1a0834b161e5877db3297d))
* add stage II: check hdl metadata ([8575543](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/8575543649bb090ff1f3589069e75f8e99b92fc8))
* add static logger binder to graalvm reflect config ([4e5385c](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/4e5385c217f1b0f44ec2f383f98c4be80c2d7e3f))
* add storage inspection method ([8d339ea](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/8d339ea3a7910b7e904a027caba7326ed69a4a99))
* add TODOs for missing test methods ([331cbcd](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/331cbcda85364bb02029b43b5475e17f64912721))
* add utils again, finish stage III ([e320e21](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/e320e214397f046fcaf1435999b3f55da07a1cd2))
* aehm! finally? did we get reflection correctly? ([e3be016](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/e3be016fde75342e32f5ab1ddaa897e2985ef381))
* disable native image build for the moment ([c3409df](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/c3409dfac8fd12afb3c3d3b5beaa65b789b8f9ce))
* do I need a fix to build the correct CLI? ([a1ddcfb](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/a1ddcfb52ce5e6b8fa3e42649013bfa5e431d22e))
* finally found default config file configuration initialisation :-D ([3293214](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/3293214236f016bdbd9b01b6151b6ea54f3959d7))
* finish configuration, add fine loggging :-) ([942e3f3](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/942e3f3dc290266cfb301e6232b27db7787b0a0f))
* finish cr url handling ([84456cc](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/84456cc493a791c26a98e044191f2ccdd9995b08))
* fix local redis db and elasticsearch access ([2529a3f](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/2529a3f1dfebe427c347ae888c3d64ede14d9eac))
* fix missing log file dir ([5bd2c8b](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/5bd2c8b5a9ce86099ddfeae0127cb439fc38adaf))
* fix object gathering (finally) ([65b42ba](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/65b42ba9c6a5219402248bc0dfc1f7c282f4ccce))
* fix some error handling ([6254fec](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/6254fec9546f94cd6604aa555c20fc119230d79d))
* hmpf! correct var name ([6e05f9d](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/6e05f9df532913fda5dd72ec4839d454a336bc54))
* implement inspect elasticsearch method ([1644250](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/1644250bc12c0bac3dc8250366a7fb94de918ba7))
* ja, ich weiß doch auch nich! ([95f87b3](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/95f87b32a254d3980eef8ac00f1a3b85e44b3702))
* maybe another new config file...? fix reflection config ([0725001](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/07250019e1598ecab3f6eb967ba1e51089abcd91))
* more logging and log file writing... add stage IV ([785bec0](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/785bec007583bde0adef80254c65a75931b1131e))
* move rdf model creation from class header ([6390514](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/6390514ef861e8caea517a42f5f065fc01ac153f))
* new and correct reflect file? finally? ([084895a](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/084895a39aced592ad65dde16dd953d5ee630e68))
* plus l ([9ca7111](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/9ca7111d9e29e53b01ae8903e1c507084d437ff6))
* refactor deleteOrNotDelete flag for inspection ([798e534](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/798e534c10a04315be02318a48bba19e4a5f9863))
* remove double dhrepinspect ([2c7988e](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/2c7988e03f979851f292fdc8a1d236cdf57e1a20))
* remove jar from artifacts again ([defa623](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/defa6239a8a5184bbebc54bbd4324b02076c3f96))
* rename and re-place default config file ([2ee8c46](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/2ee8c46daae8e8aa77dc93ab9e9a188ba6fb5ca7))
* revert graalconfig to dev state ([094e0b2](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/094e0b2b718cc6c75894336971c319b00d7abae6))
* trying creating dhcruding clienting ([f4a600c](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/f4a600cbf4953b05c9c7b1f900416cb0b479e027))
* update graalvm config files ([4aa78a8](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/4aa78a82e3ea67f104419e3ff65ebf7a1620be11))
* use old version of reflect config ([27ec717](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/27ec71792706627f119d8908f82f6cecdbe76c05))
* use options for configuration instead of params ([1926efc](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/1926efc8f73b6af8434435513077400445898293))


### Features

* add new dhrep-delete class ([147916e](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/147916e9c2d922d0d4d54bcb6d14de432aa628ed))
* add new feature dhrep-inspect, refactor class names a little bit ([8c3e895](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/8c3e8952a7061275d5679b1650d879ce5274189b))

# [4.5.0](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/compare/v4.4.2...v4.5.0) (2022-11-10)


### Features

* activate semantic release ([437f819](https://gitlab.gwdg.de/dariah-de/dhrep-cli-tools/commit/437f819335c88fa2fd655d69416923c6933e7b6a))
