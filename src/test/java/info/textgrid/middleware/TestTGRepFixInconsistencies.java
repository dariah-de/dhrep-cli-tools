package info.textgrid.middleware;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

/**
 * <p>
 * JUnit class for testing TGRepFixInconsistencies.
 * </p>
 */
public class TestTGRepFixInconsistencies {

  /**
   * @throws IOException
   */
  @Test
  public void testReadUriListFromUriList() throws IOException {

    List<String> expected = new ArrayList<String>();
    expected.add("textgrid:2tqgz.0");
    expected.add("textgrid:2tqhf.0");
    expected.add("textgrid:2tqhn.0");
    expected.add("textgrid:2tqhq.0");
    expected.add("textgrid:2tqfc.0");

    File uriList = new File("src/test/resources/uriList.txt");

    List<String> result = TGRepFixInconsistencies.readUriListFromUriList(uriList, 0);

    assertTrue(result.equals(expected));
  }

  /**
   * @throws IOException
   */
  @Test(expected = IOException.class)
  public void testReadUriListFromUriListFAIL() throws IOException {

    List<String> expected = new ArrayList<String>();
    expected.add("textgrid:2tqgz.0");
    expected.add("textgrid:2tqhf.0");
    expected.add("textgrid:2tqhn.0");
    expected.add("textgrid:2tqhq.0");
    expected.add("textgrid:2tqfc.0");

    File logFile = new File("src/test/resources/logFile.log");

    TGRepFixInconsistencies.readUriListFromUriList(logFile, 0);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testReadUriListFromLogFile() throws IOException {

    List<String> expected = new ArrayList<String>();
    expected.add("textgrid:2tqgz.0");
    expected.add("textgrid:2tqhf.0");
    expected.add("textgrid:2tqhn.0");
    expected.add("textgrid:2tqhq.0");
    expected.add("textgrid:2tqfc.0");

    File logFile = new File("src/test/resources/logFile.log");
    List<String> result = TGRepFixInconsistencies.readUriListFromLogFile(logFile, 0);

    assertTrue(result.equals(expected));
  }

  /**
   * @throws IOException
   */
  @Test(expected = IOException.class)
  public void testReadUriListFromLogFileFAIL() throws IOException {

    List<String> expected = new ArrayList<String>();
    expected.add("textgrid:2tqgz.0");
    expected.add("textgrid:2tqhf.0");
    expected.add("textgrid:2tqhn.0");
    expected.add("textgrid:2tqhq.0");
    expected.add("textgrid:2tqfc.0");

    File uriList = new File("src/test/resources/uriList.txt");

    TGRepFixInconsistencies.readUriListFromLogFile(uriList, 0);
  }

}
