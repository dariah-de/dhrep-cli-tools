package info.textgrid.middleware;

import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;

/**
 * <p>
 * JUnit class for testing DHRepUtils.
 * </p>
 */
public class TestDHRepUtils {

  // **
  // FINALS
  // **

  private final static String JSON_BASIC = "{ \n"
      + "    \"idx\" : 1 ,\n"
      + "    \"type\" : \"CREATOR\" ,\n"
      + "    \"parsed_data\" : \"PID Service pid-webapp-5.5.1-SNAPSHOT+202207071458\" ,\n"
      + "    \"data\" : \"UElEIFNlcnZpY2UgcGlkLXdlYmFwcC01LjUuMS1TTkFQU0hPVCsyMDIyMDcw\\nNzE0NTg=\" ,\n"
      + "    \"timestamp\" : \"2022-08-15T08:51:10Z\" ,\n"
      + "    \"ttl_type\" : 0 ,\n"
      + "    \"ttl\" : 86400 ,\n"
      + "    \"refs\" : [ ] ,\n"
      + "    \"privs\" : \"rwr-\"\n"
      + "  } ,\n"
      + "  { \n"
      + "    \"idx\" : 2 ,\n"
      + "    \"type\" : \"ADM_MD\" ,\n"
      + "    \"parsed_data\" : \"https://trep.de.dariah.eu/1.0/dhcrud/21.T11991/0000-001C-312A-C/adm\" ,\n"
      + "    \"data\" : \"aHR0cHM6Ly90cmVwLmRlLmRhcmlhaC5ldS8xLjAvZGhjcnVkLzIxLlQxMTk5\\nMS8wMDAwLTAwMUMtMzEyQS1DL2FkbQ==\" ,\n"
      + "    \"timestamp\" : \"2022-08-15T08:51:42Z\" ,\n"
      + "    \"ttl_type\" : 0 ,\n"
      + "    \"ttl\" : 86400 ,\n"
      + "    \"refs\" : [ ] ,\n"
      + "    \"privs\" : \"rwr-\"\n"
      + "  } ,\n"
      + "  { \n"
      + "    \"idx\" : 3 ,\n"
      + "    \"type\" : \"FILESIZE\" ,\n"
      + "    \"parsed_data\" : \"11950\" ,\n"
      + "    \"data\" : \"MTE5NTA=\" ,\n"
      + "    \"timestamp\" : \"2022-08-15T08:51:42Z\" ,\n"
      + "    \"ttl_type\" : 0 ,\n"
      + "    \"ttl\" : 86400 ,\n"
      + "    \"refs\" : [ ] ,\n"
      + "    \"privs\" : \"rwr-\"\n"
      + "  } ,\n"
      + "  { \n"
      + "    \"idx\" : 4 ,\n"
      + "    \"type\" : \"RESPONSIBLE\" ,\n"
      + "    \"parsed_data\" : \"StefanFunk@dariah.eu\" ,\n"
      + "    \"data\" : \"U3RlZmFuRnVua0BkYXJpYWguZXU=\" ,\n"
      + "    \"timestamp\" : \"2022-08-15T08:51:42Z\" ,\n"
      + "    \"ttl_type\" : 0 ,\n"
      + "    \"ttl\" : 86400 ,\n"
      + "    \"refs\" : [ ] ,\n"
      + "    \"privs\" : \"rwr-\"\n"
      + "  } ,\n"
      + "  { \n"
      + "    \"idx\" : 5 ,\n"
      + "    \"type\" : \"CHECKSUM\" ,\n"
      + "    \"parsed_data\" : \"md5:50a50bf681dd4cf4d07406e4fa126517\" ,\n"
      + "    \"data\" : \"bWQ1OjUwYTUwYmY2ODFkZDRjZjRkMDc0MDZlNGZhMTI2NTE3\" ,\n"
      + "    \"timestamp\" : \"2022-08-15T08:51:42Z\" ,\n"
      + "    \"ttl_type\" : 0 ,\n"
      + "    \"ttl\" : 86400 ,\n"
      + "    \"refs\" : [ ] ,\n"
      + "    \"privs\" : \"rwr-\"\n"
      + "  } ,\n"
      + "  { \n"
      + "    \"idx\" : 6 ,\n"
      + "    \"type\" : \"BAG\" ,\n"
      + "    \"parsed_data\" : \"https://cdstar.de.dariah.eu/test/public/EAEA0-247D-B0DB-01B6-0\" ,\n"
      + "    \"data\" : \"aHR0cHM6Ly9jZHN0YXIuZGUuZGFyaWFoLmV1L3Rlc3QvcHVibGljL0VBRUEw\\nLTI0N0QtQjBEQi0wMUI2LTA=\" ,\n"
      + "    \"timestamp\" : \"2022-08-15T08:51:42Z\" ,\n"
      + "    \"ttl_type\" : 0 ,\n"
      + "    \"ttl\" : 86400 ,\n"
      + "    \"refs\" : [ ] ,\n"
      + "    \"privs\" : \"rwr-\"\n"
      + "  } ,\n"
      + "  { \n"
      + "    \"idx\" : 7 ,\n"
      + "    \"type\" : \"PUBDATE\" ,\n"
      + "    \"parsed_data\" : \"2022-08-15 10:51:41 +0200\" ,\n"
      + "    \"data\" : \"MjAyMi0wOC0xNSAxMDo1MTo0MSArMDIwMA==\" ,\n"
      + "    \"timestamp\" : \"2022-08-15T08:51:42Z\" ,\n"
      + "    \"ttl_type\" : 0 ,\n"
      + "    \"ttl\" : 86400 ,\n"
      + "    \"refs\" : [ ] ,\n"
      + "    \"privs\" : \"rwr-\"\n"
      + "  } ,\n"
      + "  { \n"
      + "    \"idx\" : 8 ,\n"
      + "    \"type\" : \"PROV_MD\" ,\n"
      + "    \"parsed_data\" : \"https://trep.de.dariah.eu/1.0/dhcrud/21.T11991/0000-001C-312A-C/prov\" ,\n"
      + "    \"data\" : \"aHR0cHM6Ly90cmVwLmRlLmRhcmlhaC5ldS8xLjAvZGhjcnVkLzIxLlQxMTk5\\nMS8wMDAwLTAwMUMtMzEyQS1DL3Byb3Y=\" ,\n"
      + "    \"timestamp\" : \"2022-08-15T08:51:42Z\" ,\n"
      + "    \"ttl_type\" : 0 ,\n"
      + "    \"ttl\" : 86400 ,\n"
      + "    \"refs\" : [ ] ,\n"
      + "    \"privs\" : \"rwr-\"\n"
      + "  } ,\n"
      + "  { \n"
      + "    \"idx\" : 9 ,\n"
      + "    \"type\" : \"URL\" ,\n"
      + "    \"parsed_data\" : \"https://trep.de.dariah.eu/1.0/dhcrud/21.T11991/0000-001C-312A-C\" ,\n"
      + "    \"data\" : \"aHR0cHM6Ly90cmVwLmRlLmRhcmlhaC5ldS8xLjAvZGhjcnVkLzIxLlQxMTk5\\nMS8wMDAwLTAwMUMtMzEyQS1D\" ,\n"
      + "    \"timestamp\" : \"2022-08-15T08:51:42Z\" ,\n"
      + "    \"ttl_type\" : 0 ,\n"
      + "    \"ttl\" : 86400 ,\n"
      + "    \"refs\" : [ ] ,\n"
      + "    \"privs\" : \"rwr-\"\n"
      + "  } ,\n"
      + "  { \n"
      + "    \"idx\" : 10 ,\n"
      + "    \"type\" : \"DATA\" ,\n"
      + "    \"parsed_data\" : \"https://trep.de.dariah.eu/1.0/dhcrud/21.T11991/0000-001C-312A-C/data\" ,\n"
      + "    \"data\" : \"aHR0cHM6Ly90cmVwLmRlLmRhcmlhaC5ldS8xLjAvZGhjcnVkLzIxLlQxMTk5\\nMS8wMDAwLTAwMUMtMzEyQS1DL2RhdGE=\" ,\n"
      + "    \"timestamp\" : \"2022-08-15T08:51:42Z\" ,\n"
      + "    \"ttl_type\" : 0 ,\n"
      + "    \"ttl\" : 86400 ,\n"
      + "    \"refs\" : [ ] ,\n"
      + "    \"privs\" : \"rwr-\"\n"
      + "  } ,\n"
      + "  { \n"
      + "    \"idx\" : 11 ,\n"
      + "    \"type\" : \"LANDING\" ,\n"
      + "    \"parsed_data\" : \"https://trep.de.dariah.eu/1.0/dhcrud/21.T11991/0000-001C-312A-C/landing\" ,\n"
      + "    \"data\" : \"aHR0cHM6Ly90cmVwLmRlLmRhcmlhaC5ldS8xLjAvZGhjcnVkLzIxLlQxMTk5\\nMS8wMDAwLTAwMUMtMzEyQS1DL2xhbmRpbmc=\" ,\n"
      + "    \"timestamp\" : \"2022-08-15T08:51:42Z\" ,\n"
      + "    \"ttl_type\" : 0 ,\n"
      + "    \"ttl\" : 86400 ,\n"
      + "    \"refs\" : [ ] ,\n"
      + "    \"privs\" : \"rwr-\"\n"
      + "  } ,\n"
      + "  { \n"
      + "    \"idx\" : 12 ,\n"
      + "    \"type\" : \"INDEX\" ,\n"
      + "    \"parsed_data\" : \"https://trep.de.dariah.eu/1.0/dhcrud/21.T11991/0000-001C-312A-C/index\" ,\n"
      + "    \"data\" : \"aHR0cHM6Ly90cmVwLmRlLmRhcmlhaC5ldS8xLjAvZGhjcnVkLzIxLlQxMTk5\\nMS8wMDAwLTAwMUMtMzEyQS1DL2luZGV4\" ,\n"
      + "    \"timestamp\" : \"2022-08-15T08:51:42Z\" ,\n"
      + "    \"ttl_type\" : 0 ,\n"
      + "    \"ttl\" : 86400 ,\n"
      + "    \"refs\" : [ ] ,\n"
      + "    \"privs\" : \"rwr-\"\n"
      + "  } ,\n"
      + "  { \n"
      + "    \"idx\" : 13 ,\n"
      + "    \"type\" : \"METADATA\" ,\n"
      + "    \"parsed_data\" : \"https://trep.de.dariah.eu/1.0/dhcrud/21.T11991/0000-001C-312A-C/metadata\" ,\n"
      + "    \"data\" : \"aHR0cHM6Ly90cmVwLmRlLmRhcmlhaC5ldS8xLjAvZGhjcnVkLzIxLlQxMTk5\\nMS8wMDAwLTAwMUMtMzEyQS1DL21ldGFkYXRh\" ,\n"
      + "    \"timestamp\" : \"2022-08-15T08:51:42Z\" ,\n"
      + "    \"ttl_type\" : 0 ,\n"
      + "    \"ttl\" : 86400 ,\n"
      + "    \"refs\" : [ ] ,\n"
      + "    \"privs\" : \"rwr-\"\n"
      + "  } ,\n"
      + "  { \n"
      + "    \"idx\" : 14 ,\n"
      + "    \"type\" : \"TECH_MD\" ,\n"
      + "    \"parsed_data\" : \"https://trep.de.dariah.eu/1.0/dhcrud/21.T11991/0000-001C-312A-C/tech\" ,\n"
      + "    \"data\" : \"aHR0cHM6Ly90cmVwLmRlLmRhcmlhaC5ldS8xLjAvZGhjcnVkLzIxLlQxMTk5\\nMS8wMDAwLTAwMUMtMzEyQS1DL3RlY2g=\" ,\n"
      + "    \"timestamp\" : \"2022-08-15T08:51:42Z\" ,\n"
      + "    \"ttl_type\" : 0 ,\n"
      + "    \"ttl\" : 86400 ,\n"
      + "    \"refs\" : [ ] ,\n"
      + "    \"privs\" : \"rwr-\"\n"
      + "  } ,\n"
      + "  { \n"
      + "    \"idx\" : 15 ,\n"
      + "    \"type\" : \"DOI\" ,\n"
      + "    \"parsed_data\" : \"http://dx.doi.org/10.20375/0000-001C-312A-C\" ,\n"
      + "    \"data\" : \"aHR0cDovL2R4LmRvaS5vcmcvMTAuMjAzNzUvMDAwMC0wMDFDLTMxMkEtQw==\" ,\n"
      + "    \"timestamp\" : \"2022-08-15T08:51:42Z\" ,\n"
      + "    \"ttl_type\" : 0 ,\n"
      + "    \"ttl\" : 86400 ,\n"
      + "    \"refs\" : [ ] ,\n"
      + "    \"privs\" : \"rwr-\"\n"
      + "  } ,\n"
      + "  { \n"
      + "    \"idx\" : 100 ,\n"
      + "    \"type\" : \"HS_ADMIN\" ,\n"
      + "    \"parsed_data\" : {\n"
      + "        \"adminId\" : \"21.T11991/DHTEST-USER01\" ,\n"
      + "        \"adminIdIndex\" : 1 ,\n"
      + "        \"perms\" : {\n"
      + "            \"add_handle\" : true ,\n"
      + "            \"delete_handle\" : true ,\n"
      + "            \"add_derived_prefix\" : false ,\n"
      + "            \"delete_derived_prefix\" : false ,\n"
      + "            \"modify_value\" : true ,\n"
      + "            \"remove_value\" : true ,\n"
      + "            \"add_value\" : true ,\n"
      + "            \"modify_admin\" : true ,\n"
      + "            \"remove_admin\" : true ,\n"
      + "            \"add_admin\" : true ,\n"
      + "            \"read_value\" : true ,\n"
      + "            \"list_handles\" : false\n"
      + "          }\n"
      + "      } ,\n"
      + "    \"data\" : \"B/MAAAAXMjEuVDExOTkxL0RIVEVTVC1VU0VSMDEAAAAB\" ,\n"
      + "    \"timestamp\" : \"2022-08-15T08:51:10Z\" ,\n"
      + "    \"ttl_type\" : 0 ,\n"
      + "    \"ttl\" : 86400 ,\n"
      + "    \"refs\" : [ ] ,\n"
      + "    \"privs\" : \"rwr-\"\n"
      + "  }\n";
  private final static String JSON_DELETED = "[ \n" + JSON_BASIC + ",  { \n"
      + "    \"idx\" : \"99\" ,\n"
      + "    \"type\" : \"DELETED\" ,\n"
      + "    \"parsed_data\" : \"fu (2022). dmDemon.gif. DARIAH-DE. https://doi.org/10.20375/0000-001C-312A-C\"\n"
      + "  }\n"
      + "]";
  private final static String JSON_NO_DELETED = "[ \n" + JSON_BASIC + "]";

  /**
   * 
   */
  @Test
  @Ignore
  public void testGetDataFromDHCrud() {
    // TODO
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testGetDMDFromDHCrud() {
    // TODO
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testGetADMMDFromDHCrud() {
    // TODO
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testGetHDLMetadata() {
    // TODO
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testGetDOIMetadata() {
    // TODO
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testDeleteDOIMetadata() {
    // TODO
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testPutHDLMetadata() {
    // TODO
  }

  /**
   * @throws IOException
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws XPathExpressionException
   */
  @Test
  @Ignore
  public void testGetFromXML()
      throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
    DHRepUtils.getFromXML(null, null);
  }

  /**
   * 
   */
  @Test
  public void testAssembleCitationSingleEntry() {

    String expectedCitation =
        "Stefan Funk (2023). Delete me, if possible. DARIAH-DE. https://doi.org/10.20375/0000-001b-4650-a";

    List<String> creators = new ArrayList<String>();
    creators.add("Stefan Funk");
    List<String> titles = new ArrayList<String>();
    titles.add("Delete me, if possible");
    String date = "2023";
    String publisher = "DARIAH-DE";
    String doiPrefix = "10.20375";
    String doiResolver = "https://doi.org";
    URI uri = URI.create("21.T11991/0000-001b-4650-A");

    String citation = DHRepUtils.assembleCitation(creators, titles, date, publisher,
        doiPrefix, doiResolver, uri);

    assertTrue(citation.equals(expectedCitation));
  }

  /**
   * 
   */
  @Test
  public void testAssembleCitationDoubleEntry() {

    String expectedCitation =
        "Stefan Funk, Marlene Kanne (2023). Delete me, if possible. Another Deletion. DARIAH-DE. https://doi.org/10.20375/0000-001b-4650-a";

    List<String> creators = new ArrayList<String>();
    creators.add("Stefan Funk");
    creators.add("Marlene Kanne");
    List<String> titles = new ArrayList<String>();
    titles.add("Delete me, if possible");
    titles.add("Another Deletion");
    String date = "2023";
    String publisher = "DARIAH-DE";
    String doiPrefix = "10.20375";
    String doiResolver = "https://doi.org";
    URI uri = URI.create("21.T11991/0000-001b-4650-A");

    String citation = DHRepUtils.assembleCitation(creators, titles, date, publisher,
        doiPrefix, doiResolver, uri);

    assertTrue(citation.equals(expectedCitation));
  }

  /**
   * 
   */
  @Test
  public void testCheckForJSONEntriesExisting() {

    String expectedCitation =
        "fu (2022). dmDemon.gif. DARIAH-DE. https://doi.org/10.20375/0000-001C-312A-C";
    String type = "DELETED";

    String citation = DHRepUtils.checkForJSONEntries(JSON_DELETED, type);
    assertTrue(expectedCitation.equals(citation));
  }

  /**
   * 
   */
  @Test
  public void testCheckForJSONEntriesNotExisting() {

    String type = "DELETED";

    String citation = DHRepUtils.checkForJSONEntries(JSON_NO_DELETED, type);
    assertTrue(citation == null);
  }

  /**
   * 
   */
  @Test
  public void testOmitDOIInstitution() {

    URI uri = URI.create("21.T11991/0000-001b-4650-A");
    String expectedURI = "0000-001b-4650-A";

    String omittedURI = DHRepUtils.omitDOIInstitution(uri);
    assertTrue(expectedURI.equals(omittedURI));
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testDeleteFiles() {
    // TODO
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testDeleteObjectFromElasticSearch() {
    // TODO
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testGetCRIDFromRedisDB() {
    // TODO
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testCheckESQuery() {
    // TODO
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testLogStageStart() {
    // TODO
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testAssembleIIIFManifestFileList() {
    // TODO
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testAssembleDigilibFileList() {
    // TODO
  }

}
