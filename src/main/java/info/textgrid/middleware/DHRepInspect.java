package info.textgrid.middleware;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.Callable;
import javax.xml.stream.XMLStreamException;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

/**
 *
 */
@Command(
    name = "dhrep-inspect",
    header = "Inspect objects from the DARIAH-DE Repository.",
    description = "Checks status of objects in the DARIAH-DE Repository. Use it for root collections, "
        + "sub collections, and single objects! "
        + "Example: /opt/dhrep/cli-tools dhrep-inspect 21.T11991/0000-001C-311D-B")
public class DHRepInspect implements Callable<Integer> {

  // **
  // FINALS
  // **

  private static final boolean NOT_FOR_DELETION = false;
  private static final boolean DRY_RUN_IST_EGAL = true;
  private static final String PREFLIGHT = "STAGE I: PREFLIGHT";
  private static final String STAGE2 = "STAGE II: INSPECT HDL METADATA";
  private static final String STAGE3 = "STAGE III: INSPECT DOI METADATA";
  private static final String STAGE4 = "STAGE IV: INSPECT PUBLIC STORAGE";
  private static final String STAGE5 = "STAGE V: INSPECT ELASTICSEARCH INDEX DATA";
  private static final String STAGE6 = "STAGE VI: INSPECT IIIF MANIFEST CACHE";
  private static final String STAGE7 = "STAGE VII: INSPECT DIGILIB CACHE";
  private static final String STAGE8 = "STAGE VIII: INSPECT DHREP COLLECTION REGISTRY";
  private static final String WE_GOT_AN_ERROR = "WE GOT AN ERROR! ";

  // **
  // STATICS
  // **

  private static Logger log = LogManager.getLogger("CLITOOLS");

  // **
  // CLASS
  // **

  @Parameters(
      index = "0",
      description = "The object to be inspected (recursively, if collection!), such as "
          + "\"21.11113/0000-000D-FEDF-B\")")
  private URI objectURI;

  @Option(
      names = {"-c", "--config"},
      description = "The path to the dhrep-inspect config file, default is /etc/dhrep/dhrep-cli/dhrep.properties.",
      defaultValue = "/etc/dhrep/dhrep-cli/dhrep.properties")
  private File dhrepConfiguration;

  /**
   * 
   */
  @Override
  public Integer call() {

    log.info("starting dhrep-inspect");

    // **
    // I. PREFLIGHT
    // **

    DHRepUtils.logStageStart(PREFLIGHT);

    DHRepConfig config = new DHRepConfig(this.objectURI, DRY_RUN_IST_EGAL, NOT_FOR_DELETION);
    try {
      config.readConfiguration(this.dhrepConfiguration);
      config.checkURISyntax();
      config.initServices();
      config.checkServiceAvailability(this.objectURI);
      config.generateObjectList();

      int objectsToDelete = config.getCompleteObjectList().size();
      log.info("found " + objectsToDelete + " object" + (objectsToDelete != 1 ? "s" : "")
          + " to inspect " + config.getCompleteObjectList());

    } catch (IOException | ParseException | XMLStreamException e) {
      log.error(WE_GOT_AN_ERROR + e.getClass().getName() + "! " + e.getMessage());
      return 1;
    }

    // **
    // II. CHECK HDL METADATA
    // **

    DHRepUtils.logStageStart(STAGE2);

    try {
      config.inspectHDLMetadata();
    } catch (IOException e) {
      log.warn(WE_GOT_AN_ERROR + e.getClass().getName() + "! " + e.getMessage());
    }

    // **
    // III. CHECK DOI METADATA
    // **

    DHRepUtils.logStageStart(STAGE3);

    try {
      config.inspectDOIMetadata();
    } catch (IOException e) {
      log.warn(WE_GOT_AN_ERROR + e.getClass().getName() + "! " + e.getMessage());
    }

    // **
    // IV. CHECK PUBLIC STORAGE
    // **

    DHRepUtils.logStageStart(STAGE4);

    try {
      config.inspectPublicStorage();
    } catch (IOException e) {
      log.warn(WE_GOT_AN_ERROR + e.getClass().getName() + "! " + e.getMessage());
    }

    // **
    // V. CHECK ELASTICSEARCH INDEX DATA
    // **

    DHRepUtils.logStageStart(STAGE5);

    try {
      config.inspectElasticSearch();
    } catch (IOException e) {
      log.warn(WE_GOT_AN_ERROR + e.getClass().getName() + "! " + e.getMessage());
    }

    // **
    // VI. CHECK IIIF MANIFEST CACHE
    // **

    DHRepUtils.logStageStart(STAGE6);

    try {
      config.inspectIIIFMainfests();
    } catch (IOException e) {
      log.warn(WE_GOT_AN_ERROR + e.getClass().getName() + "! " + e.getMessage());
    }

    // **
    // VII. CHECK DIGILIB CACHE
    // **

    DHRepUtils.logStageStart(STAGE7);

    try {
      config.inspectDigilibCache();
    } catch (IOException e) {
      log.warn(STAGE7 + DHRepUtils.STAGE_FAILED + e.getClass().getName() + "! " + e.getMessage());
    }

    // **
    // VIII. CHECK DHREP COLLECTION REGISTRY
    // **

    DHRepUtils.logStageStart(STAGE8);

    // Get source URI for Redis DB query.
    try {
      String crURL = config.getCollectionRegistryURL();

      String message =
          "status of <" + crURL + "> can not be evaluated automatically, please click on url! :-)";
      log.info(message);

    } catch (IOException e) {
      log.warn(WE_GOT_AN_ERROR + e.getClass().getName() + "! " + e.getMessage());
    }

    // **
    // ALL DONE!
    // **

    return 0;
  }

}
