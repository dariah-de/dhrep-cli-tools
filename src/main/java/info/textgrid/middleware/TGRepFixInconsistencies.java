package info.textgrid.middleware;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import org.apache.cxf.helpers.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.middleware.common.JPairtree;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Fixity;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.xml.bind.JAXB;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 * 
 */
@Command(
    name = "fix-inconsistencies",
    header = "Fix TextGrid object consistency.",
    description = "Fixes inconsistencies of Textgrid objects on object metadata, Handle metadata, and storage level.")
public class TGRepFixInconsistencies implements Callable<Integer> {

  // **
  // FINALS
  // **

  private static final int CSV_FILE_ROW_COUNT = 11;
  private static final int CSV_LOGFILE_ROW_COUNT = 17;

  private static final String STAGE_I = "STAGE I: GET URIS FROM INPUT FILE";
  private static final String STAGE_I_COMPLETE = "STAGE I -- uri gathering -- COMPLETE";
  private static final String STAGE_II = "STAGE II: FIX INCONSISTENCIES";
  private static final String STAGE_II_COMPLETE = "STAGE II -- fixing inconsistencies -- COMPLETE";

  // **
  // STATICS
  // **

  private static Logger log = LogManager.getLogger("CLITOOLS");
  private static File pathToMetadata;
  private static File pathToPidConfig;
  private static String pidSecret;

  // **
  // CLASS
  // **

  @Option(
      names = {"-c", "--config"},
      description = "The path to the config file, default is /etc/dhrep/dhrep-cli/dhrep-cli.properties.",
      defaultValue = "/etc/dhrep/dhrep-cli/dhrep-cli.properties")
  private File dhrepConfiguration;

  @Option(
      names = {"-n", "--number"},
      description = "Check only this much objects. usage: '-n 42'")
  private int number;

  @Option(
      names = {"-i", "--input"},
      description = "The log file from check consistency in CSV format, or an URI list containiing one TextGrid URI per line. Usage: '-i ./1726063011593"
          + TGRepConfig.CC_LOG_SUFFIX
          + "' or '-i my_uri_list'. Check consistency log file must be check-consistency conform, URI list must contain one TextGrid URI per line: 'textgrid:12345.0'.")
  private File inputFile;

  @Option(
      names = {"-r", "--really-fix"},
      description = "Set if you REALLY want to fix all the invalidities! A dry-run is default without this parameter!")
  private boolean reallyFix;

  @Option(
      names = {"-p", "--handle-metadata-only"},
      description = "Only fix Handle metadata (--really-fix is still necesarry!)")
  private boolean hdlMetadataOnly;

  @Option(
      names = {"-t", "--textgrid-metadata-only"},
      description = "Only fix TextGrid metadata (--really-fix is still necesarry!)")
  private boolean tgMetadataOnly;

  @Option(
      names = {"-f", "--fix-only-prefix"},
      description = "Only fix Handle metadata with the given prefix (--really-fix is still necesarry!). Usage: '-f 21.T11991")
  private String fixPrefixOnly;

  /**
   *
   */
  @Override
  public Integer call() {

    // Get a dry-run for not to use !reallyFix :-)
    boolean dryrun = !this.reallyFix;

    log.info((dryrun ? TGRepConfig.DRYRUN + " " : "") + "start fixing tgrep inconsistencies "
        + (dryrun ? "IN DRY-RUN mode! PLEASE USE THE -r flag if you REALLY want to fix things!"
            : ""));

    long startTime = System.currentTimeMillis();

    // **
    // PREFLIGHT
    // **

    DHRepUtils.logStageStart(TGRepConfig.PREFLIGHT);

    try {

      doPreflight();

    } catch (IOException | CrudClientException e) {
      log.error(TGRepConfig.PREFLIGHT_FAILED + e.getClass().getName() + ": " + e.getMessage());
      return 1;
    }

    log.info(TGRepConfig.PREFLIGHT_COMPLETE);

    // **
    // STAGE I: GET URIS FROM INPUT FILE
    // **

    DHRepUtils.logStageStart(STAGE_I);

    List<String> uris;
    try {

      uris = doStageI();

    } catch (IOException e) {
      log.error(TGRepConfig.STAGE_I_FAILED + e.getClass().getName() + ": " + e.getMessage());
      return 2;
    }

    log.info(STAGE_I_COMPLETE);

    // **
    // STAGE II: FIX INCONSISTENCIES
    // **

    DHRepUtils.logStageStart(STAGE_II);

    // Get log output filename.
    String logfileName;
    // If CC log file, replace suffixes.
    if (this.inputFile.getName().contains(TGRepConfig.CC_LOG_SUFFIX)) {
      logfileName =
          this.inputFile.getName().replace(TGRepConfig.CC_LOG_SUFFIX, TGRepConfig.FI_LOG_SUFFIX);
    }
    // If URI list, add FI log suffix.
    else {
      logfileName = this.inputFile.getName() + TGRepConfig.FI_LOG_SUFFIX;
    }

    if (dryrun) {
      logfileName = "dry-run_" + logfileName;
    }
    File logOutput = new File(logfileName);

    log.info("writing output to: " + logOutput.getAbsolutePath());

    try {
      try (FileWriter logOutputWriter = new FileWriter(logOutput)) {
        logOutputWriter.write(TGRepConfig.CSV_LOG_HEADER);

        doStageII(uris, dryrun, logOutputWriter, this.hdlMetadataOnly, this.tgMetadataOnly,
            this.fixPrefixOnly);

        logOutputWriter.close();
      }
    } catch (CrudClientException | IOException | JSONException e) {
      log.error(TGRepConfig.STAGE_II_FAILED + e.getClass().getName() + ": " + e.getMessage());
      return 3;
    }

    log.info(STAGE_II_COMPLETE);

    // **
    // ALL DONE!
    // **

    long finishTime = System.currentTimeMillis();
    float totalTime = finishTime - startTime;

    log.info("duration: " + String.format("%.1f", (totalTime / 1000)) + " seconds" + " or "
        + String.format("%.1f", (totalTime / 1000 / 60)) + " mimutes" + " or "
        + String.format("%.2f", (totalTime / 1000 / 60 / 60)) + " hours");

    return 0;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * PREFLIGHT
   * </p>
   * 
   * @throws IOException
   * @throws CrudClientException
   */
  private void doPreflight() throws IOException, CrudClientException {

    // Check configuration.
    if (!this.dhrepConfiguration.exists()) {
      String message =
          TGRepConfig.CONFIG_FILE_NOT_FOUND + this.dhrepConfiguration.getCanonicalPath();
      throw new FileNotFoundException(message);
    }

    // Get service configuration.
    Properties p = new Properties();
    p.load(new FileInputStream(this.dhrepConfiguration));

    // Common settings.
    String tgcrudPublicUrl = p.getProperty(TGRepConfig.TGCRUD_ENDPOINT);
    TGRepConfig.setTgcrudReindexSecret(p.getProperty(TGRepConfig.TGCRUD_REINDEX_SECRET));
    String tgmetaPath = p.getProperty(TGRepConfig.TG_META_PATH);
    String tgpidUrl = p.getProperty(TGRepConfig.TGPID_ENDPOINT);
    String tgpidConfigPath = p.getProperty(TGRepConfig.TGPID_CONFIG_PATH);

    log.info(TGRepConfig.CONFIG_FILE_APPROVED);

    // Check and read input file or uri list.
    if (!this.inputFile.exists()) {
      String message = TGRepConfig.INPUT_FILE_NOT_FOUND + this.inputFile.getCanonicalPath();
      throw new FileNotFoundException(message);
    }

    // Check path to TextGrid metadata.
    pathToMetadata = new File(tgmetaPath);
    if (!pathToMetadata.exists())

    {
      String message = TGRepConfig.PATH_TO_METADATA_NOT_FOUND + pathToMetadata.getCanonicalPath();
      throw new FileNotFoundException(message);
    }
    log.debug("path to textgrid metadata: " + pathToMetadata.getCanonicalPath());

    // Get clients.
    TGRepConfig.initTgcrud(tgcrudPublicUrl);
    String crudVersion = TGRepConfig.tgcrudPublic.getVersion().execute();

    log.debug(
        "tgcrud public " + crudVersion + " initialisation complete [" + tgcrudPublicUrl + "]");

    TGRepConfig.initTgpid(tgpidUrl);
    String pidVersion = TGRepConfig.tgpid.version();

    log.debug("tgpid " + pidVersion + " initialisation complete [" + tgpidUrl + "]");

    // Get PID secret.
    pathToPidConfig = new File(tgpidConfigPath);
    if (tgpidConfigPath != null && !tgpidConfigPath.isEmpty()) {
      Properties pidp = new Properties();
      pidp.load(new FileInputStream(pathToPidConfig));
      pidSecret = pidp.getProperty(TGRepConfig.TGPID_SECRET_PROPERTY);
    } else {
      String message = TGRepConfig.PID_SERVICE_NOT_FOUND + tgpidConfigPath;
      throw new IOException(message);
    }

    log.debug("tgpid secret initialisation complete");
  }

  /**
   * <p>
   * STAGE I: GET URIS FROM INPUT FILE OR URI LIST
   * </p>
   * 
   * @return
   * @throws IOException
   * @throws FileNotFoundException
   */
  private List<String> doStageI() throws FileNotFoundException, IOException {

    List<String> result = new ArrayList<String>();

    // Check for input file first line.
    boolean uriList;
    try (BufferedReader reader = new BufferedReader(new FileReader(this.inputFile))) {
      String line = reader.readLine();

      log.debug("first line of input file: " + line);

      // Check first line...
      if (line.trim().equals(TGRepConfig.CSV_HEADER.trim())) {
        // ...for check consistency input file.
        String message = "first line check -> check consistency input file found: "
            + this.inputFile.getCanonicalPath();
        log.info(message);
        uriList = false;
      } else if (line.trim().startsWith("textgrid:")) {
        // ...for URI list.
        String message = "first line check -> uri list found: " + this.inputFile.getCanonicalPath();
        log.info(message);
        uriList = true;
      } else {
        String message = TGRepConfig.INPUT_FILE_CORRUPT + this.inputFile.getCanonicalPath();
        throw new IOException(message);
      }

      log.debug(TGRepConfig.INPUT_FILE_APPROVED);

      reader.close();
    }

    // READ check-consistency log file or URI list.
    if (uriList) {
      result = readUriListFromUriList(this.inputFile, this.number);
    } else {
      result = readUriListFromLogFile(this.inputFile, this.number);
    }

    log.info("found " + result.size() + " uri" + (result.size() != 1 ? "s" : "")
        + " to fix inconsistencies for");

    return result;
  }

  /**
   * <p>
   * STAGE II: FIX INCONSISTENCIES
   * </p>
   * 
   * @param theUris
   * @param dryrun
   * @param theLogOutputWriter
   * @param hdlMetadataOnly
   * @param tgMetadataOnly
   * @param fixPrefixOnly
   * @throws JSONException
   * @throws IOException
   * @throws CrudClientException
   */
  private static void doStageII(List<String> theUris, boolean dryrun, FileWriter theLogOutputWriter,
      boolean hdlMetadataOnly, boolean tgMetadataOnly, String fixPrefixOnly)
      throws JSONException, IOException, CrudClientException {

    if (theUris == null || theUris.isEmpty()) {
      throw new IOException("uri list is null or empty");
    }

    // Loop URIs from pidMap, get PIDs.
    int count = 0;
    int warnings = 0;
    for (String uri : theUris) {

      count++;
      String output =
          (dryrun ? "[DRYRUN] " : "") + "<" + count + "/" + theUris.size() + "> " + uri + " -> ";
      String toCsvFile4URI = uri;
      String hdl = "";
      long filesizeStorage = 0;
      long filesizeTgMeta = 0;
      long filesizeHdlMeta = 0;
      String checksumStorage = "";
      String checksumTgMeta = "";
      String checksumHdlMeta = "";

      // Check consistency (again, we do not want to take the sizes and sums from the input list!
      // Could be tampered with!)
      Response theResponse =
          TGRepConfig.tgcrudPublic.checkConsistency().setTextgridUri(uri).execute();

      int status = theResponse.getStatus();
      String reasonPhrase = theResponse.getStatusInfo().getReasonPhrase();

      // Set to true if action was successful (so no warnings are counted later on!)
      boolean hdlSuccessful = false;
      boolean tgmSuccessful = false;

      // **
      // CHECK CONSISTENCY --> 204 NO CONTENT --> all OK! No HTTP body!
      // **

      if (status == Status.NO_CONTENT.getStatusCode()) {

        log.info(output + "nothing to fix, object metadata is consistent!");
        log.debug(TGRepConfig.LINES);

        // To avoid warnings at the end, set successfulness to TRUE!
        hdlSuccessful = true;
        tgmSuccessful = true;
      }

      // **
      // CHECK CONSISTENCY --> 200 OK --> JSON response containing errors!
      // **

      else if (status == Status.OK.getStatusCode()) {

        // Get JSON string and parse JSON.
        String jsonString = IOUtils.readStringFromStream(theResponse.readEntity(InputStream.class));
        JSONObject json = new JSONObject(jsonString);

        // Get HDL, and storage filesize and checksum metadata.
        hdl = json.getString(TGRepConfig.HDL);
        toCsvFile4URI += "," + hdl;

        // Get filesize values out of the JSON and store it.
        String filesizeStatus = json.getString(TGRepConfig.FILESIZE_STATUS);
        if (filesizeStatus.equals(TGRepConfig.ERROR)) {
          JSONObject filesize = json.getJSONObject(TGRepConfig.FILESIZE);
          filesizeStorage = filesize.getLong(TGRepConfig.STORAGE);
          filesizeTgMeta = filesize.getLong(TGRepConfig.METADATA);
          filesizeHdlMeta = filesize.getLong(TGRepConfig.HANDLE);

          log.debug(output + "inconsistent filesize values: [" + TGRepConfig
              .getCommaSeparatedFilesizeBlock(filesizeStorage, filesizeTgMeta, filesizeHdlMeta)
              + "]");

          toCsvFile4URI += "," + TGRepConfig.getCommaSeparatedFilesizeBlock(filesizeStorage,
              filesizeTgMeta, filesizeHdlMeta);

          // Check storage filesize metadata for invalid entry (just in case).
          if (filesizeStorage < 0) {
            log.warn("invalid storage filesize entry for " + hdl + ": [" + filesizeStorage + "]");
          }
        } else {
          toCsvFile4URI += "," + "," + ",";
        }

        // Get checksum values out of the JSON and store it.
        String checksumStatus = json.getString(TGRepConfig.CHECKSUM_STATUS);
        if (checksumStatus.equals(TGRepConfig.ERROR)) {
          JSONObject checksum = json.getJSONObject(TGRepConfig.CHECKSUM);
          checksumStorage = TGRepConfig.normalise(checksum.getString(TGRepConfig.STORAGE));
          checksumTgMeta = TGRepConfig.normalise(checksum.getString(TGRepConfig.METADATA));
          checksumHdlMeta = TGRepConfig.normalise(checksum.getString(TGRepConfig.HANDLE));

          log.debug(output + "inconsistent checksum values: [" + TGRepConfig
              .getCommaSeparatedChecksumBlock(checksumStorage, checksumTgMeta, checksumHdlMeta)
              + "]");

          toCsvFile4URI += "," + TGRepConfig.getCommaSeparatedChecksumBlock(checksumStorage,
              checksumTgMeta, checksumHdlMeta);

          // Check storage checksum metadata for invalid entry (just in case).
          if (checksumStorage.isEmpty()) {
            log.warn("invalid storage checksum entry for " + hdl + ": [" + checksumStorage + "]");
          }
        } else {
          toCsvFile4URI += "," + "," + ",";
        }
        toCsvFile4URI += "," + System.currentTimeMillis();

        // Gather filesizes and checksums to update.
        // NOTE Pid Service needs comma separated list for key values!
        List<String> keyValueHandleMetadata = new ArrayList<String>();
        Map<String, String> keyValueTextGridMetadata = new HashMap<String, String>();

        toCsvFile4URI += gatherFilesizes(filesizeStorage, filesizeHdlMeta, filesizeTgMeta,
            keyValueHandleMetadata, keyValueTextGridMetadata);

        toCsvFile4URI += gatherChecksums(checksumStorage, checksumHdlMeta, checksumTgMeta,
            keyValueHandleMetadata, keyValueTextGridMetadata);

        // Log outcome.
        String outcome = output + hdl + ": ";

        // **
        // FIX HANDLE METADATA (only if checksum or filesize differs from storage values!).
        // **

        // Check HDL for invalid value.
        if (hdl != null && !hdl.isEmpty() && !hdl.equals(TGRepConfig.NOT_IN_METADATA)) {
          // Check for handle-metadata-only and textgrid-metadata-only only params.
          if (hdlMetadataOnly || !tgMetadataOnly) {
            // Check for fix-prefix-only param.
            if (fixPrefixOnly == null
                || (!fixPrefixOnly.equals("") && hdl.startsWith(fixPrefixOnly))) {
              // Check for different filesizes and checksums.
              if (filesizeHdlMeta != filesizeStorage | !checksumHdlMeta.equals(checksumStorage)) {
                hdlSuccessful = fixHandleMetadata(keyValueHandleMetadata, hdl, dryrun, output);
                if (hdlSuccessful) {
                  outcome += "Handle metadata UPDATED! ";
                } else {
                  outcome += "ERROR updating Handle metadata! ";
                  warnings++;
                }
              } else {
                outcome += "no need for Handle metadata update: filesize and checksum consistent. ";
              }
            } else {
              outcome += "SKIPPING Handle metadata fix: prefix is not " + fixPrefixOnly + "! ";
            }
          } else {
            outcome += "SKIPPING Handle metadata fix due to configuration! ";
          }
        } else {
          outcome += "HANDLE MISSING in tgcrud#consistency response! ";
        }

        // **
        // FIX TEXTGRID METADATA (only if checksum or filesize differs from storage values!).
        // **

        if (tgMetadataOnly || !hdlMetadataOnly) {
          if (filesizeTgMeta != filesizeStorage | !checksumTgMeta.equals(checksumStorage)) {
            tgmSuccessful = fixTextGridMetadata(keyValueTextGridMetadata, uri, dryrun, output);
            if (tgmSuccessful) {
              outcome += "TextGrid metadata UPDATED! ";

              // **
              // RE-INDEX METADATA (only if storage metadata update was successful!)
              // **

              log.debug(output + "re-indexing TextGrid metadata");

              if (!dryrun) {
                TGRepConfig.tgcrudPublic.reindexMetadata()
                    .setReindexSecret(TGRepConfig.getTgcrudReindexSecret()).setTextgridUri(uri)
                    .execute();
              }

              outcome += "TextGrid metadata RE-INDEXED! ";

            } else {
              warnings++;
            }
          } else {
            outcome += "no need for TextGrid metadata update and re-indexing. ";
          }
        } else {
          outcome += "SKIPPING TextGrid metadata fix! ";
        }

        log.info(outcome.trim());

        toCsvFile4URI += "," + hdlSuccessful + "," + tgmSuccessful;

        log.debug(TGRepConfig.LINES);

        // Check log line for correct comma count.
        String logCheck[] = toCsvFile4URI.split(",");
        if (logCheck.length != CSV_LOGFILE_ROW_COUNT) {
          log.warn("csv logfile row count must be " + CSV_LOGFILE_ROW_COUNT + ", not "
              + logCheck.length + "!");
        }

        // Write line to log file.
        theLogOutputWriter.write(toCsvFile4URI + "\n");
        theLogOutputWriter.flush();
      }

      // **
      // CHECK CONSISTENCY --> ERROR --> All other response status!
      // **

      else {
        String message =
            "tgcrud#checkconsistency status for " + hdl + ": " + status + " " + reasonPhrase;
        throw new IOException(message);
      }
    }

    // Check for warnings.
    if (warnings > 0)

    {
      log.warn("GOT " + warnings + " WARNING" + (warnings != 1 ? "S" : "") + "! PLEASE CHECK!");
    }
  }

  /**
   * @param theUri
   * @throws IOException
   */
  private static void checkForMetadataFile(File theMetadataFile) throws IOException {

    // Look for metadata file in storage.
    if (!theMetadataFile.exists()) {
      String message = "metadata file " + theMetadataFile.getCanonicalPath() + " not found!";
      throw new FileNotFoundException(message);
    }
    if (!theMetadataFile.canWrite()) {
      String message = "metadata file " + theMetadataFile.getCanonicalPath() + " not writable!";
      throw new IOException(message);
    }
  }

  /**
   * <p>
   * Gather filesizes to update.
   * </p>
   * 
   * @param filesizeStorage
   * @param filesizeHdlMeta
   * @param filesizeTgMeta
   * @param theHandleMetadata
   * @param theTextGridMetadata
   * @return
   */
  private static String gatherFilesizes(long filesizeStorage, long filesizeHdlMeta,
      long filesizeTgMeta, List<String> theHandleMetadata,
      Map<String, String> theTextGridMetadata) {

    String result = "";

    if (filesizeTgMeta != filesizeStorage || filesizeHdlMeta != filesizeStorage) {

      theHandleMetadata.add(TGRepConfig.HDL_FILESIZE + "," + filesizeStorage);
      theTextGridMetadata.put(TGRepConfig.HDL_FILESIZE, String.valueOf(filesizeStorage));

      result += "," + TGRepConfig.getCommaSeparatedFilesizeBlock(filesizeStorage,
          filesizeStorage, filesizeStorage);
    } else {
      result += "," + "," + ",";
    }

    return result;
  }

  /**
   * <p>
   * Gather checksums to update.
   * </p>
   * 
   * @param checksumSrtorage
   * @param checksumHdlMeta
   * @param checksumTgMeta
   * @param theHandleMetadata
   * @param theTextGridMetadata
   * @return
   */
  private static String gatherChecksums(String checksumStorage, String checksumHdlMeta,
      String checksumTgMeta, List<String> theHandleMetadata,
      Map<String, String> theTextGridMetadata) {

    String result = "";

    if (!checksumTgMeta.equals(checksumStorage) || !checksumHdlMeta.equals(checksumStorage)) {

      theHandleMetadata.add(TGRepConfig.HDL_CHECKSUM + "," + checksumStorage);
      theTextGridMetadata.put(TGRepConfig.HDL_CHECKSUM, checksumStorage);

      result += "," + TGRepConfig.getCommaSeparatedChecksumBlock(checksumStorage,
          checksumStorage, checksumStorage);
    } else {
      result += "," + "," + ",";
    }

    return result;
  }


  /**
   * <p>
   * Fix Handle metadata.
   * </p>
   * 
   * @param theHandleMetadata
   * @param theHdl
   * @param theDryRun
   * @param theOutput
   * @return
   * @throws IOException
   */
  private static boolean fixHandleMetadata(List<String> theHandleMetadata, String theHdl,
      boolean theDryRun, String theOutput) throws IOException {

    boolean result = false;

    if (!theHandleMetadata.isEmpty()) {

      log.debug(theOutput + "new Handle metadata list for " + theHdl + ": " + theHandleMetadata);

      if (!theDryRun) {

        // Check for deprecated Handle prefix //
        // (https://pid.gwdg.de/handles/11858/00-1734-0000-0005-1421-2), get new Handle.

        // Update Handle metadata with PID Service.
        String hdlWithout = theHdl.replaceFirst("hdl:", "");
        try {
          Response umResponse =
              TGRepConfig.tgpid.updateTextGridMetadata(hdlWithout, theHandleMetadata, pidSecret);
          int umStatusCode = umResponse.getStatus();
          String umReasonPhrase = umResponse.getStatusInfo().getReasonPhrase();

          if (umStatusCode != Status.NO_CONTENT.getStatusCode()) {
            throw new IOException("tgpid#updatetextgridmetadata status for " + theHdl + ": "
                + umStatusCode + " " + umReasonPhrase);
          } else {
            log.debug(theOutput + "pid service response: " + umStatusCode + " " + umReasonPhrase);

            result = true;
          }
        } catch (WebApplicationException e) {
          log.warn(theOutput + "could not update Handle metadata due to a " + e.getClass().getName()
              + ": " + e.getMessage());

          result = false;
        }

      } else {
        String pidVersion = TGRepConfig.tgpid.version();

        log.debug(theOutput + "tgpid service version DRYRUN call complete: " + pidVersion);

        result = true;
      }

      if (result == true) {
        log.debug(theOutput + "new Handle metadata successfully set!");
      }
    }

    return result;
  }

  /**
   * <p>
   * Fix TextGrid metadata on disk.
   * </p>
   * 
   * @param theTextGridMetadata
   * @param theTextGridUri
   * @param theDryRun
   * @param theOutput
   * @return
   * @throws IOException
   */
  private static boolean fixTextGridMetadata(Map<String, String> theTextGridMetadata,
      String theTextGridUri, boolean theDryRun, String theOutput) throws IOException {

    boolean result = false;

    if (!theTextGridMetadata.isEmpty()) {

      log.debug(theOutput + "new TextGrid metadata map: " + theTextGridMetadata);

      // Get metadata file path.
      JPairtree jpt = new JPairtree(theTextGridUri);
      File metadataFile =
          new File(pathToMetadata, jpt.getPtreePathString() + jpt.getPtreeEncodedId() + ".meta");
      checkForMetadataFile(metadataFile);

      log.debug(theOutput + "existing and writable: " + metadataFile.getCanonicalPath());

      // Build metadata container from storage.
      MetadataContainerType metadata = JAXB.unmarshal(metadataFile, MetadataContainerType.class);

      // Prepare metadata extent update on disk.
      String extentString = theTextGridMetadata.get(TGRepConfig.HDL_FILESIZE);
      if (extentString != null) {
        // Set new extent element.
        metadata.getObject().getGeneric().getGenerated().setExtent(new BigInteger(extentString));
      }

      // Prepare metadata fixity update on disk.
      String fixityString = theTextGridMetadata.get(TGRepConfig.HDL_CHECKSUM);
      if (fixityString != null) {
        String splitFixity[] = fixityString.split(":");
        if (fixityString.isEmpty() || splitFixity.length != 2) {
          String message = "checksum syntax " + fixityString
              + " invalid or empty, must be like md5:955367a6f4c6f4a61b0e8796c751f0a3!";
          throw new IOException(message);
        }
        String algo = splitFixity[0];
        String digest = splitFixity[1];
        String originator = DHRepCliVersion.BUILDNAME + "-" + DHRepCliVersion.VERSION + "+"
            + DHRepCliVersion.BUILDDATE;
        // Clear old values and set new fixity element.
        metadata.getObject().getGeneric().getGenerated().getFixity().clear();
        Fixity fixity = new Fixity();
        fixity.setMessageDigest(digest);
        fixity.setMessageDigestAlgorithm(algo);
        fixity.setMessageDigestOriginator(originator);
        metadata.getObject().getGeneric().getGenerated().getFixity().add(fixity);

        log.debug(theOutput + "new TextGrid metadata successfully set: FILESIZE=" + extentString
            + ", CHECKSUM=" + fixity.getMessageDigestAlgorithm() + ":" + fixity.getMessageDigest()
            + ":" + fixity.getMessageDigestOriginator());
      }

      if (!theDryRun) {

        // Update metadata file on disk.
        JAXB.marshal(metadata, metadataFile);

        result = true;

      } else {
        // Checks already done above! Just set success value to true.
        result = true;
      }
    }

    return result;
  }

  /**
   * <p>
   * Read the URI list from check-consistency log file.
   * </p>
   * 
   * @param theInputFile
   * @param theNumber
   * @return
   * @throws IOException
   */
  protected static List<String> readUriListFromLogFile(File theInputFile, int theNumber)
      throws IOException {

    List<String> result = new ArrayList<String>();

    try (BufferedReader reader = new BufferedReader(new FileReader(theInputFile))) {

      // Read and ignore the header line.
      String line = reader.readLine();

      // Read second line.
      line = reader.readLine();

      // Read the rest, get URIs.
      int count = 1;
      while (line != null) {
        String ids[] = line.split(",");
        if (ids.length != CSV_FILE_ROW_COUNT) {
          throw new IOException(
              "csv file row count must be " + CSV_FILE_ROW_COUNT + ", not " + ids.length + "!");
        }
        result.add(ids[0]);

        // Check for given number param.
        count++;
        if (theNumber > 0 && theNumber < count) {
          log.debug("stopping at " + theNumber);
          break;
        }

        // read next line
        line = reader.readLine();
      }

      reader.close();
    }

    return result;
  }

  /**
   * <p>
   * Read the URI list from URI list file.
   * </p>
   * 
   * @param theInputFile
   * @param theNumber
   * @return
   * @throws IOException
   */
  protected static List<String> readUriListFromUriList(File theInputFile, int theNumber)
      throws IOException {

    List<String> result = new ArrayList<String>();

    try (BufferedReader reader = new BufferedReader(new FileReader(theInputFile))) {

      // Read first line.
      String line = reader.readLine();

      // Read the rest, get URIs.
      int count = 1;
      while (line != null) {
        if (line.contains(",")) {
          throw new IOException("uri list must only contain TextGrud URIs!");
        }
        result.add(line);

        // Check for given number param.
        count++;
        if (theNumber > 0 && theNumber < count) {
          log.debug("stopping at " + theNumber);
          break;
        }

        // read next line
        line = reader.readLine();
      }

      reader.close();
    }

    return result;
  }

}
