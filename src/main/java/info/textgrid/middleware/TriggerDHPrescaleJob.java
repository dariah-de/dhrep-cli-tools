package info.textgrid.middleware;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.Callable;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

/**
 *
 */
@Command(
    name = "prescale-dhrep",
    header = "Fill or update the dhrep digilib prescale directory.",
    description = "Use this tool to fill or update the dhrep digilib prescale directory. "
        + "It gets a list of DARIAH-DE repository objects where format starts with \"image\" "
        + "and asks dhcrud to send an update notification to the prescaler-messagebean "
        + "calling the recache-operation.")
public class TriggerDHPrescaleJob extends DHCrudIndexClient implements Callable<Integer> {
  @Parameters(
      index = "0",
      description = "dhcrud properties file location, e.g. /etc/dhrep/dhcrud/crud.properties")
  private File propertiesFile;
  @Option(
      names = {"-f", "--from"},
      description = "start date, in format yyyy-MM-dd, default: 2017-12-01")
  private String date_from;
  @Option(
      names = {"-t", "--to"},
      description = "end date, in format yyyy-MM-dd, default: now")
  private String date_to;
  @Option(
      names = {"-h", "--howmany"},
      description = "size of an elasticsearch result, default: 10000")
  private int howmany = 10000;
  @Option(
      names = {"-p", "--localdhcrudport"},
      description = "localhost dhcrud port, defauolt: 80")
  private int localDHCrudPort = 80;
  private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

  /**
   *
   */
  @Override
  public Integer call() throws Exception {

    setupFromPropertiesLoc(this.propertiesFile);

    Date from = this.dateFormat.parse("2017-12-01");
    Date to = new Date();

    if (this.date_from != null) {
      from = this.dateFormat.parse(this.date_from);
    } else {
      System.out.println("No start date given, default to: " + this.dateFormat.format(from));
    }

    if (this.date_to != null) {
      to = this.dateFormat.parse(this.date_to);
    } else {
      System.out.println("No end date given, default to: " + this.dateFormat.format(to));
    }

    listImages(from, to);
    this.esclient.close();
    return 0;
  }

  /**
   * @param from The FROM date
   * @param to The TO date
   * @throws IOException
   * @throws ParseException
   */
  public void listImages(Date from, Date to) throws IOException, ParseException {

    final Scroll scroll = new Scroll(TimeValue.timeValueHours(24L));

    SearchRequest getImageList = new SearchRequest(this.index);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

    RangeQueryBuilder range = QueryBuilders.rangeQuery(this.dateOfObjectCreation).from(from).to(to);

    BoolQueryBuilder imageFilter =
        QueryBuilders.boolQuery()
            .must(QueryBuilders.prefixQuery("administrativeMetadata.dcterms:format", "image"))
            .must(range);

    String[] includeFields = new String[] {
        "administrativeMetadata.dcterms:identifier",
        "administrativeMetadata.dcterms:modified",
        "administrativeMetadata.dcterms:format"};
    String[] excludeFields = Strings.EMPTY_ARRAY;

    searchSourceBuilder
        .query(imageFilter)
        .fetchSource(includeFields, excludeFields)
        .sort(new FieldSortBuilder("administrativeMetadata.dcterms:modified").order(SortOrder.ASC))
        .size(this.howmany);

    getImageList
        .source(searchSourceBuilder)
        .scroll(scroll);

    printQuery(searchSourceBuilder);

    SearchResponse searchResponse = this.esclient.search(getImageList, RequestOptions.DEFAULT);
    String scrollId = searchResponse.getScrollId();

    SearchHits hits = searchResponse.getHits();

    System.out.println("Found " + hits.getTotalHits() + " images in the timerange from "
        + this.dateFormat.format(from) + " to " + this.dateFormat.format(to) + " in " + this.index);

    System.out.println("\npress enter to proceed or ^C to stop");
    System.in.read();

    System.out.println("\nhits: " + hits.getHits().length + " - scrollId: " + scrollId);

    // Scroll until no hits are returned
    while (hits.getHits() != null && hits.getHits().length > 0) {

      processHits(hits);

      SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId).scroll(scroll);
      SearchResponse searchScrollResponse =
          this.esclient.scroll(scrollRequest, RequestOptions.DEFAULT);
      scrollId = searchScrollResponse.getScrollId();
      hits = searchScrollResponse.getHits();
      System.out.println("\nhits: " + hits.getHits().length + " - scrollId: " + scrollId);
    }

    // clear scroll after completion
    ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
    clearScrollRequest.addScrollId(scrollId);
    ClearScrollResponse clearScrollResponse =
        this.esclient.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
    boolean succeeded = clearScrollResponse.isSucceeded();

    if (succeeded) {
      System.out.println("scroll cleared");
    } else {
      System.out.println("error clearing scroll");
    }
  }

  /**
   * @param hits All the search hits
   * @throws ParseException
   * @throws IOException
   */
  private void processHits(SearchHits hits) throws ParseException, IOException {

    int count = 0;
    for (SearchHit hit : hits) {
      count++;
      Map<String, String> map =
          (Map<String, String>) hit.getSourceAsMap().get("administrativeMetadata");
      // Remove "hdl:" prefix.
      String uri = map.get("dcterms:identifier").substring(4);
      String format = map.get("dcterms:format").toString();
      Date lastModified = this.dateFormat.parse(map.get("dcterms:modified").toString());

      // TODO Make DH-crud URL configurable?
      URL url = new URL("http://localhost:" + this.localDHCrudPort + "/dhcrud/" + uri + "/recache");

      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setRequestMethod("GET");
      int responseCode = con.getResponseCode();

      if (responseCode == HttpURLConnection.HTTP_OK) {
        System.out.println("[" + count + "/" + hits.getTotalHits() + "] --> " + uri + " (" + format
            + ") / last modified: " + this.dateFormat.format(lastModified)
            + " - tgcrud recache: OK");
      } else {
        System.out.println(
            "[" + count + "/" + hits.getTotalHits() + "] --> ERROR with dhcrud recache: " + uri);
      }
    }
  }

}
