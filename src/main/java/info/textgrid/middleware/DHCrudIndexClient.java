package info.textgrid.middleware;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.xcontent.ToXContent;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.json.JsonXContent;

/**
 *
 */
public class DHCrudIndexClient {

  private static Logger log = LogManager.getLogger("CLITOOLS");

  protected String index;
  protected String type;
  protected String dateOfObjectCreation;
  protected RestHighLevelClient esclient;

  /**
   * @param propertiesFile The location of the properties file
   */
  protected void setupFromPropertiesLoc(File propertiesFile) {

    Properties prop = new Properties();
    InputStream input = null;

    try {
      input = new FileInputStream(propertiesFile);
      prop.load(input);
    } catch (FileNotFoundException e) {
      System.out.println("file not found: " + propertiesFile.getAbsolutePath());
      return;
    } catch (IOException e) {
      System.out.println("something wrong with file access: " + propertiesFile.getAbsolutePath());
      return;
    }

    // Get ES settings from OAIPMH properties file.
    String esUrl = prop.getProperty("elasticSearch.url");
    String[] esPorts = prop.getProperty("elasticSearch.ports").split(",");
    this.index = prop.getProperty("elasticSearch.index");
    this.type = prop.getProperty("elasticSearch.type");
    // TODO Take values for fields also from properties file!
    this.dateOfObjectCreation = "administrativeMetadata.dcterms:created";

    log.info("=== settings ===");
    log.info("es host: " + esUrl);
    log.info("es ports: ");
    Arrays.stream(esPorts).forEach(log::info);
    log.info("es index: " + this.index);
    log.info("es type: " + this.type);
    log.info("dateOfObjectCreation: " + this.dateOfObjectCreation);
    log.info("=== /settings ===");

    System.out.println("\npress enter to proceed or ^C to stop");
    try {
      System.in.read();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    List<HttpHost> hosts = new ArrayList<HttpHost>();
    for (String port : esPorts) {
      hosts.add(new HttpHost(esUrl, Integer.parseInt(port.trim()), "http"));
    }

    this.esclient =
        new RestHighLevelClient(RestClient.builder(hosts.toArray(new HttpHost[hosts.size()])));
  }

  /**
   * @param queryBuilder The XContentBuilder to build content with
   */
  public static void printQuery(ToXContent queryBuilder) {
    try {
      XContentBuilder builder = queryBuilder.toXContent(JsonXContent.contentBuilder().prettyPrint(),
          ToXContent.EMPTY_PARAMS);
      log.debug(Strings.toString(builder));
    } catch (IOException e) {
      log.error("error printing json");
    }
  }

}
