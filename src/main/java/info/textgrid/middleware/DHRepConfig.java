package info.textgrid.middleware;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.xpath.XPathExpressionException;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.jena.atlas.json.JSON;
import org.apache.jena.atlas.json.JsonArray;
import org.apache.jena.atlas.json.JsonObject;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;
import info.textgrid.middleware.common.CachingUtils;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.DHCrudService;
import jakarta.ws.rs.core.HttpHeaders;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

/**
 *
 */
public class DHRepConfig {

  // **
  // FINALS
  // **

  private static final String DHCRUD_ENDPOINT = "dhcrud-endpoint";
  private static final String HDL_CONFIG_LOCATION = "hdl-config-location";
  private static final String HDL_ENDPOINT = "hdl-endpoint";
  private static final String HDL_PATH = "hdl-path";
  private static final String HDL_PREFIX = "hdl-prefix";
  private static final String HDL_USER = "hdl-user";
  private static final String HDL_PASSWD = "hdl-passwd";
  private static final String HDL_KEY_DELETED = "DELETED";
  private static final String HDL_KEY_BAG = "BAG";
  private static final String HDL_KEY_SOURCE = "SOURCE";
  private static final String HDL_KEY_RESPONSIBLE = "RESPONSIBLE";
  private static final String HDL_KEY_PUBDATE = "PUBDATE";
  private static final String DOI_CONFIG_LOCATION = "doi-config-location";
  private static final String DOI_ENDPOINT = "doi-endpoint";
  private static final String DOI_PREFIX = "doi-prefix";
  private static final String DOI_USER = "doi-user";
  private static final String DOI_PASSWD = "doi-passwd";
  private static final String ES_ENDPOINT = "es-endpoint";
  private static final String REDIS_CONFIG_LOCATION = "redis-config-location";
  private static final String REDIS_ENDPOINT = "redis-endpoint";
  private static final String REDIS_DB_NO = "redis-db-no";
  private static final String CR_ENDPOINT = "cr-endpoint";
  private static final String IIIFMD_CONFIG_LOCATION = "iiifmd-config-location";
  private static final String IIIFMD_CACHE_DIR = "iiifmd-cache-dir";
  private static final String DIGILIB_CONFIG_LOCATION = "digilib-config-location";
  private static final String DIGILIB_CACHE_DIRS = "digilib-cache-dirs";
  private static final String CITATION_PUBLISHER = "citation-publisher";
  private static final String CITATION_DOI_RESOLVER = "citation-doi-resolver";
  private static final String DOI_METADATA_PATH = "doi-metadata-path";
  private static final String CONFIG_FILE_APPROVED = "config file location approved";
  private static final String CONFIG_FILE_NOT_FOUND = "config file not found: ";
  private static final String CONFIG_APPROVED = "configuration approved: ";
  private static final String CONFIG_INCOMPLETE = "configuration incomplete: ";
  private static final String JSON_FILE_SUFFIX = ".json";
  // private static final String HDL_REGEXP =
  // "(hdl:|doi:)?[0-9]{2}\\.[a-zA-Z0-9]+\\/[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{1}";

  // **
  // STATICS
  // **

  private static Logger log = LogManager.getLogger("CLITOOLS");

  private static DHCrudService dhcrudClient = null;
  private static WebClient hdlClient = null;
  private static WebClient doiClient = null;
  private static WebClient esClient = null;
  private static JedisPool redisPool;
  private static Jedis redisClient = null;

  // **
  // CLASS
  // **

  private URI rootCollectionURI;
  private boolean dryrun = true;
  private boolean deleteOrNotDelete;
  private List<URI> completeObjectList = new ArrayList<URI>();
  private List<URI> completeBagList = new ArrayList<URI>();
  private String dhcrudEndpoint;
  private String hdlConfigLoction;
  private String hdlEndpoint;
  private String hdlPath;
  private String hdlPrefix;
  private String hdlUser;
  private String hdlPasswd;
  private String doiConfigLocation;
  private String doiEndpoint;
  private String doiPrefix;
  private String doiUser;
  private String doiPasswd;
  private String esConfigLocation;
  private String esEndpoint;
  private String iiifmdConfigLocation;
  private String iiifmdCacheDir;
  private String digilibConfigLocation;
  private List<String> digilibCacheDirs = new ArrayList<String>();
  private String redisConfigLocation;
  private String redisEndpoint;
  private String redisDBNo;
  private String crEndpoint;
  private String citationPublisher;
  private String citationDOIResolver;
  private String doiMetadataPath;
  private Model overallRDFModel = ModelFactory.createDefaultModel();
  private URI collectionSourceURI;
  private File logFileDir;
  private String bagListFilename;

  // **
  // CONSTRUCTOR
  // **

  /**
   * @param theURI The Handle URI for this configuration
   * @param dryrun DRY RUN or NO DRY RUN
   */
  public DHRepConfig(final URI theURI, final boolean dryrun, final boolean deleteOrNotDelete) {

    this.rootCollectionURI = theURI;
    this.dryrun = dryrun;
    this.deleteOrNotDelete = deleteOrNotDelete;

    // Create log dir, if deletion.
    if (deleteOrNotDelete) {
      this.logFileDir = new File("dhrep-delete-logs-" + System.currentTimeMillis());
      this.logFileDir.mkdir();

      log.info("logging to: " + this.logFileDir.getAbsolutePath());
    }
  }

  // **
  // PUBLIC METHODS
  // **

  /**
   *
   */
  @Override
  public String toString() {

    String result = "["
        + "dhcrud_endpoint=" + this.dhcrudEndpoint + ", "
        + "hdl_config_location=" + this.hdlConfigLoction + ", "
        + "hdl_endpoint=" + this.hdlEndpoint + ", "
        + "hdl_path=" + this.hdlPath + ", "
        + "hdl_prefix=" + this.hdlPrefix + ", "
        + "hdl_user=" + this.hdlUser + ", "
        + "hdl_password=" + (this.hdlPasswd == null || this.hdlPasswd.isEmpty() ? "[null]" : "***")
        + ", "
        + "doi_config_location=" + this.doiConfigLocation + ", "
        + "doi_endpoint=" + this.doiEndpoint + ", "
        + "doi_prefix=" + this.doiPrefix + ", "
        + "doi_user=" + this.doiUser + ", "
        + "doi_password=" + (this.doiPasswd == null || this.doiPasswd.isEmpty() ? "[null]" : "***")
        + ", "
        + "doi_metadata_path=" + this.doiMetadataPath
        + "es_config_location=" + this.esConfigLocation + ", "
        + "es_endpoint=" + this.esEndpoint + ", "
        + "iiifmd_config_location=" + this.iiifmdConfigLocation + ", "
        + "iiifmd_cache_dir=" + this.iiifmdCacheDir + ", "
        + "digilib_config_location=" + this.digilibConfigLocation + ", "
        + "digilib_cache_dirs=" + this.digilibCacheDirs + ", "
        + "redis_db_no=" + this.redisDBNo + ", "
        + "cr_endpoint=" + this.crEndpoint + ", "
        + "citation_publisher=" + this.citationPublisher + ", "
        + "citation_doi_resolver=" + this.citationDOIResolver
        + "]";

    return result;
  }


  // **
  // PROTECTED METHODS
  // **

  /**
   * <p>
   * Checks for correct HDL URI, logs and throws exception, if incorrect.
   * </p>
   * 
   * @throws IOException
   */
  protected void checkURISyntax() throws IOException {

    log.info("checking hdl syntax and prefix");

    if (!isCorrectHDLSyntax(this.rootCollectionURI)) {
      String message = "hdl syntax or prefix invalid! prefix must be [" + this.hdlPrefix
          + "] due to configuration";
      throw new IOException(message);
    }

    log.info("hdl syntax approved");
  }


  /**
   * <p>
   * Checks config values for existence.
   * </p>
   * 
   * @param theConfigFile The dhrep config file.
   * @throws IOException
   */
  protected void readConfiguration(final File theConfigFile) throws IOException {

    log.info("reading configuration: " + theConfigFile.getCanonicalPath());

    if (!theConfigFile.exists()) {
      String message = CONFIG_FILE_NOT_FOUND + theConfigFile.getCanonicalPath();
      log.error(message);
      throw new FileNotFoundException(message);
    }
    log.debug(CONFIG_FILE_APPROVED);

    getConfigValues(theConfigFile);
    log.debug(this.toString());

    log.info("configuration approved and ready");
  }

  /**
   * <p>
   * Initialise service clients.
   * </p>
   * 
   * @throws IOException
   */
  protected void initServices() throws IOException {

    log.info("initialising service clients");

    this.initDHCrudClient();
    this.initHDLClient();
    this.initDOIClient();
    this.initESClient();
    this.initRedisClient();

    log.info("service initialisation complete");
  }

  /**
   * <p>
   * Close service clients, if needed.
   * </p>
   */
  protected void shutdownServices() {

    log.info("shutting down service clients");

    hdlClient.close();
    doiClient.close();
    redisClient.close();

    log.info("service client shutdown complete");
  }

  /**
   * <p>
   * Check service availability.
   * </p>
   * 
   * @param theURI The URI of the collection to delete.
   * @throws IOException
   */
  protected void checkServiceAvailability(URI theURI) throws IOException {

    log.info("checking service availibility");

    // Check dhcrud service.
    log.debug("fetching dhcrud version");

    String dhcrudVersion = dhcrudClient.getVersion();
    if (dhcrudVersion == null || dhcrudVersion.isEmpty()) {
      String message = "dhcrud" + DHRepUtils.TRY_AGAIN_ON_SERVER;
      log.error(message);
      throw new IOException(message);
    }
    log.info("dhcrud service available [" + dhcrudVersion + "]");

    // Check HDL service.
    String hdlJson = DHRepUtils.getHDLMetadata(theURI, hdlClient);
    if (hdlJson == null || hdlJson.isEmpty()) {
      String message = "hdl" + DHRepUtils.TRY_AGAIN_ON_SERVER;
      log.error(message);
      throw new IOException(message);
    }
    log.info("hdl service available");

    // Check ES.
    String esJson = DHRepUtils.checkESQuery(theURI, esClient);
    if (esJson == null || esJson.isEmpty()) {
      String message = "elasticsearch" + DHRepUtils.TRY_AGAIN_ON_SERVER;
      log.error(message);
      throw new IOException(message);
    }
    log.info("elasticsearch service available");

    // Check DOI service.
    String xml = DHRepUtils.getDOIMetadata(theURI, this.doiPrefix, doiClient);
    if (xml == null || xml.isEmpty()) {
      String message = "doi" + DHRepUtils.TRY_AGAIN_ON_SERVER;
      log.error(message);
      throw new IOException(message);
    }
    log.info("doi metadata service available");

    // TODO No need to check Redis DB...?

    log.info("services checked and ready");
  }

  /**
   * <p>
   * Gets all the HDLs/DOIs marked for deletion.
   * </p>
   * 
   * @throws IOException
   * @throws ParseException
   * @throws XMLStreamException
   */
  protected void generateObjectList() throws IOException, ParseException, XMLStreamException {

    log.info("generating object list");

    // Gather objects recursively.
    gatherObjects(this.rootCollectionURI);

    log.info("object list generation complete");
  }

  /**
   * <p>
   * Gets the HDL metadata JSON.
   * </p>
   * 
   * @throws IOException
   */
  protected void inspectHDLMetadata() throws IOException {

    log.info("inspecting hdl metadata");

    for (URI u : this.completeObjectList) {
      String currentHDLMetadataJSON = DHRepUtils.getHDLMetadata(u, hdlClient);

      // Check for source URI if root collection.
      if (u.equals(this.rootCollectionURI)) {
        String sourceURI = DHRepUtils.checkForJSONEntries(currentHDLMetadataJSON, HDL_KEY_SOURCE);
        if (sourceURI == null) {
          String message =
              "can not obtain source value from hdl metadata, do we have a root collection?";
          log.warn(message);
        } else {
          this.collectionSourceURI = URI.create(sourceURI);
        }
      }

      String responsible =
          DHRepUtils.checkForJSONEntries(currentHDLMetadataJSON, HDL_KEY_RESPONSIBLE);
      String publicationDate =
          DHRepUtils.checkForJSONEntries(currentHDLMetadataJSON, HDL_KEY_PUBDATE);
      String deleted = DHRepUtils.checkForJSONEntries(currentHDLMetadataJSON, HDL_KEY_DELETED);

      String message = (DHRepUtils.OK + "[" + u.toString() + "] --> " + publicationDate + " ("
          + responsible + "), status: " + (deleted == null ? "PUBLISHED" : "DELETED"));
      if (deleted == null) {
        log.info(message);
      } else {
        log.warn(message);
      }

      log.debug(currentHDLMetadataJSON);
    }
  }

  /**
   * <p>
   * Gets the HDL metadata JSON, creates and adds the new DELETED metadata field.
   * </p>
   * 
   * @throws IOException
   * @throws ParseException
   */
  protected void createAndAddDeletedHDLMetadata() throws IOException, ParseException {

    int count = 0;
    for (URI u : this.completeObjectList) {
      String currentHDLMetadataJSON = DHRepUtils.getHDLMetadata(u, hdlClient);

      // Check for source URI if root collection.
      if (u.equals(this.rootCollectionURI)) {
        String sourceURI = DHRepUtils.checkForJSONEntries(currentHDLMetadataJSON, HDL_KEY_SOURCE);
        if (sourceURI == null) {
          String message = "can not obtain source value from hdl metadata";
          throw new IOException(message);
        }
        this.collectionSourceURI = URI.create(sourceURI);
      }

      // Check for BAG URL.
      this.completeBagList
          .add(URI.create(DHRepUtils.checkForJSONEntries(currentHDLMetadataJSON, HDL_KEY_BAG)));

      // Check for already existing DELETED flag.
      // NOTE Can not occur if DELETED flag is already set, because dhcrud delivers a 410 GONE if
      // DELEETD flag is already existing! We already get an error fetching administrative metadata
      // for collection!
      String existingCitation =
          DHRepUtils.checkForJSONEntries(currentHDLMetadataJSON, HDL_KEY_DELETED);
      if (existingCitation == null) {
        // No deleted flag: Do update!
        String newHDLMetadataJSON = this.createHDLDeletedMetadata(u, currentHDLMetadataJSON);
        DHRepUtils.putHDLMetadata(u, newHDLMetadataJSON, hdlClient, this.dryrun);
        count++;
      } else {
        // DELETED existing! Do warn and do NOT update!
        log.warn(
            "DELETED flag already existing for [" + u + "] with citation: " + existingCitation);
      }
    }

    storeBagList();

    log.info("hdl metadata added for " + count + " object" + (count == 1 ? "" : "s"));
  }

  /**
   * <p>
   * Get DOI metadata.
   * </p>
   * 
   * @throws IOException
   */
  protected void inspectDOIMetadata() throws IOException {

    log.info("inspecting doi metadata");

    for (URI u : this.completeObjectList) {
      String doiMetadata = DHRepUtils.getDOIMetadata(u, this.doiPrefix, doiClient);

      log.debug(doiMetadata);
    }
  }

  /**
   * <p>
   * Delete DOI metadata.
   * </p>
   * 
   * @throws IOException
   */
  protected void deleteDOIMetadata() throws IOException {

    for (URI u : this.completeObjectList) {
      DHRepUtils.deleteDOIMetadata(u, this.doiPrefix, doiClient, this.dryrun);
    }

    log.info("doi metadata deleted for " + this.completeObjectList.size() + " object"
        + (this.completeObjectList.size() == 1 ? "" : "s"));
  }

  /**
   * <p>
   * Store the current overall RDF model.
   * </p>
   * 
   * @throws FileNotFoundException
   * @throws IOException
   */
  protected void storeOverallRDFModel() throws FileNotFoundException, IOException {

    File modelFile = new File(this.logFileDir,
        DHRepUtils.omitDOIInstitution(this.rootCollectionURI) + RDFConstants.TTL_FILE_SUFFIX);

    try (FileWriter modelWriter = new FileWriter(modelFile)) {
      this.overallRDFModel.write(modelWriter, RDFConstants.TURTLE);
    }

    log.info("overall rdf model written to: " + modelFile.getName());
  }

  /**
   * <p>
   * Inspect objects in public storage.
   * </p>
   * 
   * @throws IOException
   */
  protected void inspectPublicStorage() throws IOException {

    log.info("inspecting object data in public storage");

    for (URI u : this.completeObjectList) {
      String data = DHRepUtils.getDataFromDHCrud(u, dhcrudClient);

      log.info(DHRepUtils.OK + "[" + u + "] --> size=" + data.length());
    }
  }

  /**
   * <p>
   * Inspect objects in elastic search.
   * </p>
   * 
   * @throws IOException
   */
  protected void inspectElasticSearch() throws IOException {

    log.info("inspecting object metadata in elastic search");

    for (URI u : this.completeObjectList) {
      String json = DHRepUtils.checkESQuery(u, esClient);

      log.debug(json);
    }
  }

  /**
   * <p>
   * Get the pathes to all the manifest files, and inspect them!
   * </p>
   * 
   * @throws IOException
   */
  protected void inspectIIIFMainfests() throws IOException {

    log.info("inspecting iiif manifest files in " + this.iiifmdCacheDir);

    // Get files to delete.
    int count = 0;
    for (URI u : this.completeObjectList) {

      // Get path to manifests, use URI and caching path from iiifmd config (add "hdl:" for that!).
      String pathToManifests = this.iiifmdCacheDir + File.separatorChar
          + CachingUtils.getCachingPathFromURI("hdl:" + u.toString());

      log.info("looking for manifest files in: " + pathToManifests);

      File fileList[] = DHRepUtils.assembleIIIFManifestFileList(pathToManifests);
      count += fileList.length;

      String message = "found " + fileList.length + " file" + (fileList.length == 1 ? "" : "s");
      log.debug(message);
    }

    log.info(
        "found " + count + " iiif manifest related file" + (count != 1 ? "s" : "") + " in total");
  }

  /**
   * <p>
   * Get the pathes to all the manifest files, and delete them!
   * </p>
   * 
   * @throws IOException
   */
  protected void deleteIIIFMainfests() throws IOException {

    log.info("preparing deletion of iiif manifest files in " + this.iiifmdCacheDir);

    // Get files to delete.
    int count = 0;
    for (URI u : this.completeObjectList) {

      // Get path to manifests, use URI and caching path from iiifmd config (add "hdl:" for that!).
      String pathToManifests = this.iiifmdCacheDir + File.separatorChar
          + CachingUtils.getCachingPathFromURI("hdl:" + u.toString());

      // Get files to delete.
      File fileList[] = DHRepUtils.assembleIIIFManifestFileList(pathToManifests);
      String message =
          "found " + fileList.length + " file" + (fileList.length == 1 ? "" : "s") + " to delete";
      if (fileList.length > 0) {
        count += fileList.length;
        log.info(message);
      } else {
        log.debug(message);
      }

      DHRepUtils.deleteFiles(Arrays.asList(fileList), this.dryrun);
    }

    log.info("deletion of " + count + " iiif manifest related file" + (count != 1 ? "s" : "")
        + " complete");
  }

  /**
   * <p>
   * Get the pathes to all the digilib cache pathes and files, and inspect them!
   * </p>
   * 
   * @throws IOException
   */
  protected void inspectDigilibCache() throws IOException {

    log.info("inspecting digilib cached images in " + this.digilibCacheDirs.toString());

    // Loop all the folders and get files to delete.
    int count = 0;
    for (String path : this.digilibCacheDirs) {
      File cacheFolder = new File(path);

      log.info("checking digilib cache folder: " + cacheFolder.getCanonicalPath());

      if (!cacheFolder.exists()) {
        String message =
            "digilib " + DHRepUtils.TRY_AGAIN_ON_SERVER + cacheFolder.getCanonicalPath();
        log.error(message);
        throw new IOException(message);
      }

      // Get files.
      List<File> filesToDelete =
          DHRepUtils.assembleDigilibFileList(cacheFolder, this.completeObjectList);
      count += filesToDelete.size();

      String message =
          "found " + filesToDelete.size() + " file" + (filesToDelete.size() == 1 ? "" : "s");
      log.debug(message);
    }

    log.info("found " + count + " cached image file" + (count == 1 ? "" : "s") + " in total");
  }

  /**
   * <p>
   * Get the pathes to all the digilib cache pathes and files, and delete them!
   * </p>
   * 
   * @throws IOException
   */
  protected void deleteDigilibCache() throws IOException {

    log.info("preparing deletion of digilib cached images in " + this.digilibCacheDirs.toString());

    // Loop all the folders and get files to delete.
    int count = 0;
    for (String path : this.digilibCacheDirs) {
      File cacheFolder = new File(path);

      log.info("checking digilib cache folder: " + cacheFolder.getCanonicalPath());

      if (!cacheFolder.exists()) {
        String message =
            "digilib " + DHRepUtils.TRY_AGAIN_ON_SERVER + cacheFolder.getCanonicalPath();
        log.error(message);
        throw new IOException(message);
      }

      // Get files to delete.
      List<File> filesToDelete =
          DHRepUtils.assembleDigilibFileList(cacheFolder, this.completeObjectList);
      count += filesToDelete.size();

      String message = "found " + filesToDelete.size() + " file"
          + (filesToDelete.size() == 1 ? "" : "s") + " to delete in: " + path;
      if (filesToDelete.size() > 0) {
        log.info(message);
      } else {
        log.debug(message);
      }

      DHRepUtils.deleteFiles(filesToDelete, this.dryrun);
    }

    log.info("deletion of " + count + " cached image file" + (count == 1 ? "" : "s") + " complete");
  }

  /**
   * <p>
   * Delete all objects from ElasticSearch.
   * </p>
   * 
   * @throws IOException
   */
  protected void deleteFromElasticSearch() throws IOException {

    // Just read: curl -XGET
    // "http://localhost:9202/dariah-public/metadata/21.T11991%2F0000-001B-4650-A" | python -m
    // json.tool

    // DELETE: curl -XDELETE
    // "http://localhost:9202/dariah-public/metadata/21.T11991%2F0000-001B-4650-A"

    log.info("preparing deletion from elasticsearch index");

    // Get files to delete.
    for (URI u : this.completeObjectList) {
      DHRepUtils.deleteObjectFromElasticSearch(esClient, u, this.dryrun);
    }

    log.info("elasticsearch index update complete");
  }

  /**
   * <p>
   * Create the URL to the CR for deletion (there is no API for deleting collections available
   * yet...).
   * </p>
   * 
   * @return The URL to the Collection Registry for deleting the collection
   * @throws IOException
   */
  protected String getCollectionRegistryURL() throws IOException {

    String result = "";

    if (this.collectionSourceURI == null || this.collectionSourceURI.toString().isEmpty()) {
      String message = "collection source uri must not be null, do we have a root collection?";
      log.warn(message);
    }

    String crid = DHRepUtils.getCRIDFromRedisDB(this.collectionSourceURI, redisClient, this.dryrun);
    if (crid == null || crid.isEmpty()) {
      crid = "[none available]";
    }

    // must be like: https://dfatest.de.dariah.eu/colreg-ui/collections/62fa09210ca5c044a24a0ab4

    result = this.crEndpoint + "/collections/" + crid;

    return result;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Check for service config files and get their properties.
   * </p>
   * 
   * @param theConfigFile The dhrep-delete config file.
   * @throws FileNotFoundException
   * @throws IOException
   */
  private void getConfigValues(final File theConfigFile) throws FileNotFoundException, IOException {

    // Get service configuration.
    Properties p = new Properties();
    p.load(new FileInputStream(theConfigFile));

    // Common settings.
    this.dhcrudEndpoint = p.getProperty(DHCRUD_ENDPOINT);
    this.esEndpoint = p.getProperty(ES_ENDPOINT);
    this.redisEndpoint = p.getProperty(REDIS_ENDPOINT);
    this.citationPublisher = p.getProperty(CITATION_PUBLISHER);
    this.citationDOIResolver = p.getProperty(CITATION_DOI_RESOLVER);
    this.doiMetadataPath = p.getProperty(DOI_METADATA_PATH);

    // HDL config.
    this.hdlConfigLoction = p.getProperty(HDL_CONFIG_LOCATION);
    File hdlConfigFile = new File(this.hdlConfigLoction);
    if (!hdlConfigFile.exists()) {
      String message = "hdl " + CONFIG_FILE_NOT_FOUND + hdlConfigFile.getAbsolutePath();
      throw new FileNotFoundException(message);
    }

    log.debug("hdl " + CONFIG_FILE_APPROVED);

    Properties hdlProp = new Properties();
    hdlProp.load(new FileInputStream(hdlConfigFile));
    this.hdlEndpoint = hdlProp.getProperty(p.getProperty(HDL_ENDPOINT));
    this.hdlPath = hdlProp.getProperty(p.getProperty(HDL_PATH));
    this.hdlPrefix = hdlProp.getProperty(p.getProperty(HDL_PREFIX));
    this.hdlUser = hdlProp.getProperty(p.getProperty(HDL_USER));
    this.hdlPasswd = hdlProp.getProperty(p.getProperty(HDL_PASSWD));
    if (this.hdlEndpoint == null || this.hdlEndpoint.isEmpty() ||
        this.hdlPath == null || this.hdlPath.isEmpty() ||
        this.hdlPrefix == null || this.hdlPrefix.isEmpty() ||
        this.hdlUser == null || this.hdlUser.isEmpty() ||
        this.hdlPasswd == null || this.hdlPasswd.isEmpty()) {
      String message = "hdl " + CONFIG_INCOMPLETE + hdlConfigFile.getAbsolutePath();
      throw new IOException(message);
    }

    log.debug("hdl " + CONFIG_APPROVED + this.hdlEndpoint + this.hdlPath + ", "
        + this.hdlPrefix + ", " + this.hdlUser + ":***");

    // DOI config.
    this.doiConfigLocation = p.getProperty(DOI_CONFIG_LOCATION);
    File doiConfigFile = new File(this.doiConfigLocation);
    if (!doiConfigFile.exists()) {
      String message = "doi " + CONFIG_FILE_NOT_FOUND + doiConfigFile.getAbsolutePath();
      throw new FileNotFoundException(message);
    }

    log.debug("doi " + CONFIG_FILE_APPROVED);

    Properties doiProp = new Properties();
    doiProp.load(new FileInputStream(doiConfigFile));
    this.doiEndpoint = doiProp.getProperty(p.getProperty(DOI_ENDPOINT));
    this.doiPrefix = doiProp.getProperty(p.getProperty(DOI_PREFIX));
    this.doiUser = doiProp.getProperty(p.getProperty(DOI_USER));
    this.doiPasswd = doiProp.getProperty(p.getProperty(DOI_PASSWD));
    if (this.doiEndpoint == null || this.doiEndpoint.isEmpty() ||
        this.doiPrefix == null || this.doiPrefix.isEmpty() ||
        this.doiUser == null || this.doiUser.isEmpty() ||
        this.doiPasswd == null || this.doiPasswd.isEmpty()) {
      String message = "doi " + CONFIG_INCOMPLETE + doiConfigFile.getAbsolutePath();
      throw new IOException(message);
    }

    log.debug("doi " + CONFIG_APPROVED + this.doiEndpoint + ", " + this.doiPrefix + ", "
        + this.doiUser + ":***");

    // IIIFMD config.
    this.iiifmdConfigLocation = p.getProperty(IIIFMD_CONFIG_LOCATION);
    File iiifmdConfigFile = new File(this.iiifmdConfigLocation);
    if (!iiifmdConfigFile.exists()) {
      String message = "iiifmd " + CONFIG_FILE_NOT_FOUND + iiifmdConfigFile.getAbsolutePath();
      throw new FileNotFoundException(message);
    }

    log.debug("iiifmd " + CONFIG_FILE_APPROVED);

    Properties iiifmdProp = new Properties();
    iiifmdProp.load(new FileInputStream(iiifmdConfigFile));
    this.iiifmdCacheDir = iiifmdProp.getProperty(p.getProperty(IIIFMD_CACHE_DIR));
    if (this.iiifmdCacheDir == null || this.iiifmdCacheDir.isEmpty()) {
      String message = "iiifmd " + CONFIG_INCOMPLETE + iiifmdConfigFile.getAbsolutePath();
      throw new IOException(message);
    }

    log.debug("iiifmd " + CONFIG_APPROVED + this.iiifmdCacheDir);

    // Digilib config.
    this.digilibConfigLocation = p.getProperty(DIGILIB_CONFIG_LOCATION);
    File digilibConfigFile = new File(this.digilibConfigLocation);
    if (!digilibConfigFile.exists()) {
      String message = "digilib " + CONFIG_FILE_NOT_FOUND + digilibConfigFile.getAbsolutePath();
      throw new FileNotFoundException(message);
    }

    log.debug("digilib " + CONFIG_FILE_APPROVED);

    Properties digilibProperties = new Properties();
    digilibProperties.load(new FileInputStream(digilibConfigFile));
    String digilibCacheDirValue = digilibProperties.getProperty(p.getProperty(DIGILIB_CACHE_DIRS));
    String blubbi[] = digilibCacheDirValue.split(":");
    for (String s : blubbi) {
      this.digilibCacheDirs.add(s);
    }
    if (digilibCacheDirValue.isEmpty() || this.digilibCacheDirs.isEmpty()) {
      String message = "digilib " + CONFIG_INCOMPLETE + digilibConfigFile.getAbsolutePath();
      throw new IOException(message);
    }

    log.debug("digilib " + CONFIG_APPROVED + this.digilibCacheDirs);

    // Redis config (read from XMl file).
    this.redisConfigLocation = p.getProperty(REDIS_CONFIG_LOCATION);
    File redisConfigFile = new File(this.redisConfigLocation);
    if (!redisConfigFile.exists()) {
      String message = "redis " + CONFIG_FILE_NOT_FOUND + redisConfigFile.getAbsolutePath();
      throw new FileNotFoundException(message);
    }

    log.debug("redis and cr " + CONFIG_FILE_APPROVED);

    String redisDBNoPath = p.getProperty(REDIS_DB_NO);
    String crEndpointPath = p.getProperty(CR_ENDPOINT);
    try {
      this.redisDBNo = DHRepUtils.getFromXML(redisDBNoPath, redisConfigFile);
      this.crEndpoint = DHRepUtils.getFromXML(crEndpointPath, redisConfigFile);
    } catch (XPathExpressionException | ParserConfigurationException | SAXException
        | IOException e) {
      String message = "xml parse error: " + e.getMessage() + "! please check " + REDIS_DB_NO
          + ", and " + CR_ENDPOINT;
      throw new IOException(message);
    }
    if (this.redisDBNo == null || this.redisDBNo.isEmpty() ||
        this.crEndpoint == null || this.crEndpoint.isEmpty()) {
      String message = "redis and cr" + CONFIG_INCOMPLETE + "please check " + REDIS_DB_NO + ", and "
          + CR_ENDPOINT;
      throw new IOException(message);
    }

    log.debug("redis and cr " + CONFIG_APPROVED + this.redisEndpoint + "[" + this.redisDBNo + "], "
        + this.crEndpoint);
  }

  /**
   * <p>
   * HDL syntax checking method, uses regexp and the HDL's prefix from config.
   * </p>
   * 
   * @param theURI The object's HDL URI to check
   * @return true if URI has the correct syntax, false if not
   */
  private boolean isCorrectHDLSyntax(final URI theURI) {

    boolean result = false;

    String hdlRegexp = "(hdl:)?" + this.hdlPrefix
        + "\\/[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{1}";

    result = theURI.toString().matches(hdlRegexp);

    return result;
  }

  /**
   * <p>
   * Initialize DH-crud client.
   * </p>
   */
  private void initDHCrudClient() {

    if (dhcrudClient == null) {

      log.debug("creating new dhcrud client: " + this.dhcrudEndpoint);

      dhcrudClient = JAXRSClientFactory.create(this.dhcrudEndpoint, DHCrudService.class);
    }
  }

  /**
   * <p>
   * Initialize HDL web client.
   * </p>
   */
  private void initHDLClient() {

    String hdlBaseURL = this.hdlEndpoint + this.hdlPath;

    if (hdlClient == null) {

      log.debug("creating new hdl client: " + hdlBaseURL);

      // Create web client with header and basic auth.
      hdlClient = WebClient.create(hdlBaseURL, this.hdlUser, this.hdlPasswd, null)
          .header(HttpHeaders.CONTENT_TYPE, DHRepUtils.MIMETYPE_JSON);
    }
  }

  /**
   * <p>
   * Initialize DOI web client.
   * </p>
   */
  private void initDOIClient() {

    String doiBaseURL = this.doiEndpoint + this.doiMetadataPath;

    if (doiClient == null) {

      log.debug("creating new doi client: " + doiBaseURL);

      // Create web client with basic auth.
      doiClient = WebClient.create(doiBaseURL, this.doiUser, this.doiPasswd, null);
    }
  }

  /**
   * <p>
   * Initialize ElsaticSearch web client.
   * </p>
   */
  private void initESClient() {

    String esBaseURL = this.esEndpoint;

    if (esClient == null) {

      log.debug("creating new elasticsearch client: " + esBaseURL);

      esClient = WebClient.create(esBaseURL);
    }
  }

  /**
   * <p>
   * Init Redis pool and client.
   * </p>
   * 
   * @throws IOException
   */
  private void initRedisClient() throws IOException {

    if (redisClient == null) {

      URI endpointURI = URI.create(this.redisEndpoint);
      String endpoint = endpointURI.getHost();
      int port = endpointURI.getPort();
      int dbno = Integer.parseInt(this.redisDBNo);

      log.debug("creating new redis pool and client: " + endpoint + ":" + port + "[" + dbno + "]");

      try {
        redisPool = new JedisPool(endpoint, port);
        if (redisPool != null) {
          redisClient = redisPool.getResource();
          redisClient.select(dbno);
        }
      } catch (JedisConnectionException e) {
        String message = "no connection to redis db: " + e.getMessage() + "!"
            + DHRepUtils.TRY_AGAIN_ON_SERVER;
        if (this.dryrun) {
          log.warn(DHRepUtils.DRYRUN + message);
        } else {
          throw new IOException(message);
        }
      }
    }
  }

  /**
   * <p>
   * Create the new DELETED metadata to add to the HDL metadata, such as: "[author1], [author2]
   * ([year]). [title1], [title2]. DARIAH-DE. https://doi.org/[DOI]", see example: "Fugu Fischinger,
   * Stefan E. Funk (2021). DELETED DRAGON ON DHREPWORKSHOP. DARIAH-DE.
   * https://doi.org/10.20375/0000-001b-4650-a"
   * </p>
   * 
   * @param theURI The URI to create the metadata for
   * @return The complete new JSON string with DELETED addition
   * @throws IOException
   * @throws ParseException
   */
  private String createHDLDeletedMetadata(final URI theURI, final String theJSON)
      throws IOException, ParseException {

    String result = "";

    // Get publication date from HDL metadata.
    String pubdate = DHRepUtils.checkForJSONEntries(theJSON, HDL_KEY_PUBDATE);
    if (pubdate == null) {
      log.error(
          "no PUBDATE entry existing in hdl metadata! please contact your repository administrators!");
    } else {
      pubdate = pubdate.substring(0, 4);
    }

    // Get descriptive metadata.
    String dmd = DHRepUtils.getDMDFromDHCrud(theURI, dhcrudClient);
    Model dmdModel = RDFUtils.readModel(dmd, RDFConstants.TURTLE);
    this.overallRDFModel.add(dmdModel);
    // Get creators and titles from DMD.
    List<String> creators =
        RDFUtils.findAllObjects(dmdModel, RDFConstants.DC_PREFIX, RDFConstants.ELEM_DC_CREATOR);
    List<String> titles =
        RDFUtils.findAllObjects(dmdModel, RDFConstants.DC_PREFIX, RDFConstants.ELEM_DC_TITLE);

    // Assemble citation.
    String citation = DHRepUtils.assembleCitation(creators, titles, pubdate,
        this.citationPublisher, this.doiPrefix, this.citationDOIResolver, theURI);

    log.info("creating citation: " + citation);

    // Assemble JSON.
    JsonObject deleted = new JsonObject();
    deleted.put("idx", "99");
    deleted.put("type", HDL_KEY_DELETED);
    // TODO Encode the citation in any way?
    deleted.put("parsed_data", citation);

    log.debug("adding json: " + deleted.toString());

    // Get JSON from string, and add deleted to JSON array and return new JSON string.
    JsonArray a = JSON.parseAny(theJSON).getAsArray();
    a.add(deleted);
    result = a.toString();

    // Store JSON to log file.
    File jsonFile =
        new File(this.logFileDir, DHRepUtils.omitDOIInstitution(theURI) + JSON_FILE_SUFFIX);
    IOUtils.transferTo(new ByteArrayInputStream(result.getBytes("UTF-8")), jsonFile);

    log.debug("storing json to file: " + jsonFile.getName());

    return result;
  }

  /**
   * <p>
   * Gathers all object URIs, adds subcollections and (sub)objects.
   * </p>
   * 
   * @param theCollection The collection URI
   * @throws IOException
   * @throws ParseException
   * @throws XMLStreamException
   */
  private void gatherObjects(final URI theURI)
      throws IOException, ParseException, XMLStreamException {

    // Get administrative object metadata from dhcrud.
    String admmd = DHRepUtils.getADMMDFromDHCrud(theURI, dhcrudClient);
    Model admmdModel = RDFUtils.readModel(admmd, RDFConstants.TURTLE);
    this.overallRDFModel.add(admmdModel);
    // If collection.
    if (RDFUtils.isCollection(admmdModel, theURI.toString())) {
      this.completeObjectList.add(theURI);

      log.info("adding collection [" + theURI + "]");

      // Get data from dhcrud, and loop over parts recursively.
      String data = DHRepUtils.getDataFromDHCrud(theURI, dhcrudClient);
      Model dataModel = RDFUtils.readModel(data, RDFConstants.TURTLE);
      this.overallRDFModel.add(dataModel);
      List<String> parts =
          RDFUtils.getProperties(dataModel, LTPUtils.resolveHandlePid(theURI.toString()),
              RDFConstants.DCTERMS_PREFIX, RDFConstants.ELEM_DCTERMS_HASPART);
      for (String p : parts) {
        gatherObjects(URI.create(LTPUtils.omitHdlNamespace(p)));
      }
    }

    // If object.
    else {
      // Error if root URI is not a collection.
      if (theURI.equals(this.rootCollectionURI) && this.deleteOrNotDelete) {
        String message = "root uri must be a collection!";
        throw new IOException(message);
      } else {
        this.completeObjectList.add(theURI);

        log.debug("adding object [" + theURI + "]");
      }
    }
  }

  /**
   * <p>
   * Store the BAG URL list to disk.
   * </p>
   * 
   * @return The file name of the BAG list file
   * @throws UnsupportedEncodingException
   * @throws IOException
   */
  private void storeBagList() throws UnsupportedEncodingException, IOException {

    // Store complete BAG URL list.
    File bagListFile = new File(this.logFileDir,
        DHRepUtils.TODO_FILE_PREFIX
            + DHRepUtils.omitDOIInstitution(this.rootCollectionURI)
            + DHRepUtils.BAG_LIST_FILE_SUFFIX);
    String bagString = "";
    for (URI u : this.completeBagList) {
      bagString += u.toASCIIString() + "\n";
    }
    IOUtils.transferTo(new ByteArrayInputStream(bagString.getBytes("UTF-8")), bagListFile);

    this.bagListFilename = bagListFile.getName();

    log.debug("bag uri list stored to : " + this.bagListFilename);
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @return The root collection URI
   */
  public URI getRootCollectionURI() {
    return this.rootCollectionURI;
  }

  /**
   * @return The complete object list of objects to be deleted
   */
  public List<URI> getCompleteObjectList() {
    return this.completeObjectList;
  }

  /**
   * @return The complete BAG URL list of storage objects to be deleted
   */
  public List<URI> getCompleteBagList() {
    return this.completeBagList;
  }

  /**
   * @return The BAG list filename
   */
  public String getBagListFilename() {
    return this.bagListFilename;
  }

}
