package info.textgrid.middleware;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.xcontent.XContentType;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;


@Command(
    name="sandboxer",
    header="Add sandbox flag to elasticsearch metadata entries.",
    description="After reindexing the repository into elasticsearch there are no sandbox "
        + "(nearlyPublished) flags in Elasticsearch. This tool adds them by querying sesame."
)
public class Sandboxer extends TgSearchIndexClient implements Callable<Integer> {

  private static Logger log = LogManager.getLogger("CLITOOLS");

  @Parameters(
      index = "0",
      description = "properties file location, e.g. /etc/dhrep/tgsearch/tgsearch-public.properties")
  private File propertiesFile;

  @Option(
      names = {"-h", "--howmany"},
      description = "how many metadata entries to process, defaults to 1")
  private int howmany = 1;

  @Option(
      names = {"-d", "--dry-run"},
      description = "do not write to elasticsearch, just query")
  private boolean dryRun;

  @Override
  public Integer call() throws Exception {
    setupFromPropertiesLoc(propertiesFile);
    compareSandboxEStoSesame();
    esclient.close();
    return 0;
  }

  private void compareSandboxEStoSesame() throws IOException {

    String json = "{\"nearlyPublished\" : true}";

    String queryString =
        "PREFIX tg:<http://textgrid.info/relation-ns#>" +
            "SELECT ?uri WHERE {\n" +
            "  ?uri tg:isNearlyPublished true\n" +
            "  FILTER NOT EXISTS { ?uri tg:isDeleted true }\n" +
            "} LIMIT " + howmany;

    List<String> allUris = timedSparqlQuery(queryString, false);

    for (String uri : allUris) {
      String id = uri.substring(9);
      try {
        if(!dryRun) {
          UpdateRequest updateRequest =
              new UpdateRequest().index(index).type(type).id(id).doc(json, XContentType.JSON);
          UpdateResponse response = esclient.update(updateRequest, RequestOptions.DEFAULT);
          log.info(id + " done");
        } else {
          log.info("dry run: not updating elasticsearch: " + id);
        }
      } catch (Exception e) {
        log.info("error updating "+id+", possibly doc missing in es?", e);
      }
    }

    log.info("all done");
  }

}
