package info.textgrid.middleware;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.apache.commons.io.IOUtils;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetRequestBuilder;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.document.DocumentField;
import org.elasticsearch.common.xcontent.ToXContent;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.json.JsonXContent;
import info.textgrid.utils.sesameclient.SesameClient;


public class OtherPossiblyUsefullCode {

  private final static String ES_INDEX = "textgrid-public";
  private final static String ES_TYPE = "metadata";
  private SesameClient scpub;
  private TransportClient esclient;

  public static void main(String[] args) throws IOException {

    OtherPossiblyUsefullCode sb = new OtherPossiblyUsefullCode();

    // sb.formatsToSesame();
    // sb.editionWorkRelToSesame();


  }

  public OtherPossiblyUsefullCode() throws MalformedURLException {
    scpub = new SesameClient("http://localhost:9091/openrdf-sesame/repositories/textgrid-public");
    /*
     * Settings tgESClusterSettings = ImmutableSettings.settingsBuilder().put("cluster.name",
     * "esx-1-test-instance").build(); esclient = new
     * TransportClient(tgESClusterSettings).addTransportAddress(new
     * InetSocketTransportAddress("localhost", 9302 ));
     */

  }

  public void editionWorkRelToSesame() throws IOException {
    String queryString =
        "PREFIX tg:<http://textgrid.info/relation-ns#>"
            + "PREFIX dc:<http://purl.org/dc/elements/1.1/>" +
            "SELECT ?uri WHERE {\n" +
            "?uri dc:format <textgrid:mime:text/tg.edition+tg.aggregation+xml> ." +
            "FILTER NOT EXISTS { ?uri tg:isDeleted true } .\n" +
            "FILTER NOT EXISTS { ?uri tg:isEditionOf ?egal } ." +
            // "FILTER NOT EXISTS { ?uri tg:notInElasticsearch true } ." +
            "} LIMIT 30000";

    InputStream sres = scpub.sparql(queryString);
    System.out.println("res there");

    HashSet<String> allUris = urisFromSparqlResponse(sres);

    System.out.println("size: " + allUris.size());

    for (String uri : allUris) {
      // System.out.println(uri);
      // getFormatAndSandboxFromES(uris);
    }
    getEditionOfFromES(allUris);

    String countString =
        "PREFIX tg:<http://textgrid.info/relation-ns#>"
            + "PREFIX dc:<http://purl.org/dc/elements/1.1/>" +
            "SELECT (count(?uri) as ?cnt) WHERE {\n" +
            "?uri dc:format <textgrid:mime:text/tg.edition+tg.aggregation+xml> ." +
            "FILTER NOT EXISTS { ?uri tg:isDeleted true } .\n" +
            "FILTER NOT EXISTS { ?uri tg:isEditionOf ?egal } ." +
            // "FILTER NOT EXISTS { ?uri tg:notInElasticsearch true } ." +
            "} ";

    InputStream cres = scpub.sparql(countString);
    printLiteralsFromSparqlResponse(cres);
  }

  public void formatsToSesame() throws IOException {

    // SesameClient scpub = new
    // SesameClient("http://10.160.254.106:9090/openrdf-sesame/repositories/textgrid-public");

    // Settings tgESClusterSettings = ImmutableSettings.settingsBuilder().put("cluster.name",
    // "tg-search-es-ws4").build();
    // esClient = new TransportClient(tgESClusterSettings).addTransportAddress(new
    // InetSocketTransportAddress(url, port));
    // SesameClient scnonpub = new
    // SesameClient("http://10.160.254.106:9090/openrdf-sesame/repositories/textgrid");

    String queryString =
        "PREFIX tg:<http://textgrid.info/relation-ns#>"
            + "PREFIX dc:<http://purl.org/dc/elements/1.1/>" +
            "SELECT ?uri WHERE {\n" +
            "?x tg:isBaseUriOf ?uri ." +
            "FILTER NOT EXISTS { ?uri tg:isDeleted true } .\n" +
            "FILTER NOT EXISTS { ?uri dc:format ?form } ." +
            "FILTER NOT EXISTS { ?uri tg:notInElasticsearch true } ." +
            "} LIMIT 100000";

    InputStream sres = scpub.sparql(queryString);
    System.out.println("res there");

    HashSet<String> allUris = urisFromSparqlResponse(sres);

    System.out.println("size: " + allUris.size());

    for (String uri : allUris) {
      // System.out.println(uri);
      // getFormatAndSandboxFromES(uris);
    }
    getFormatFromES(allUris);


    System.out.println("now counting");

    String countString =
        "PREFIX tg:<http://textgrid.info/relation-ns#>"
            + "PREFIX dc:<http://purl.org/dc/elements/1.1/>" +
            "SELECT (count(?uri) as ?cnt) WHERE {\n" +
            "?x tg:isBaseUriOf ?uri ." +
            "FILTER NOT EXISTS { ?uri tg:isDeleted true } .\n" +
            "FILTER NOT EXISTS { ?uri dc:format ?form } ." +
            "FILTER NOT EXISTS { ?uri tg:notInElasticsearch true } ." +
            // "FILTER EXISTS { ?uri dc:format ?form } ." +
            "}";

    InputStream cres = scpub.sparql(countString);
    printLiteralsFromSparqlResponse(cres);


  }

  public void getEditionOfFromES(HashSet<String> uris) {
    MultiGetRequestBuilder metadata4allUrisPrep = esclient.prepareMultiGet();
    for (String uri : uris) {
      String formatedId = uri.replaceAll("textgrid:", "");

      ArrayList<String> fields = new ArrayList<String>();
      fields.add("textgridUri");
      fields.add("edition.isEditionOf");

      // metadata4allUrisPrep.add(new MultiGetRequest.Item(ES_INDEX, ES_TYPE,
      // formatedId).fields(fields.toArray(new String[fields.size()])));

    }

    StringBuffer ttlbuffer = new StringBuffer();
    MultiGetResponse metadata4allUris = metadata4allUrisPrep.execute().actionGet();
    for (MultiGetItemResponse metadata4allUrisResp : metadata4allUris.getResponses()) {

      if (metadata4allUrisResp.getResponse().isExists()) {

        Map<String, DocumentField> metadata4allUrisContent =
            metadata4allUrisResp.getResponse().getFields();
        String hitUri = metadata4allUrisContent.get("textgridUri").getValue().toString();
        String workUri = metadata4allUrisContent.get("edition.isEditionOf").getValue().toString();

        ttlbuffer.append("<" + hitUri + "> <http://textgrid.info/relation-ns#isEditionOf> <"
            + workUri + "> .\n");

        // System.out.println(hitUri + " - " + workUri);
      }
    }

    System.out.println(ttlbuffer.toString());
    try {
      scpub.uploadTurtle(IOUtils.toInputStream(ttlbuffer.toString(), Charset.forName("UTF-8")));
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  public void getFormatFromES(HashSet<String> uris) {
    MultiGetRequestBuilder metadata4allUrisPrep = esclient.prepareMultiGet();
    for (String uri : uris) {
      String formatedId = uri.replaceAll("textgrid:", "");

      ArrayList<String> fields = new ArrayList<String>();
      fields.add("textgridUri");
      fields.add("format");

      // System.out.println(formatedId);
      /*
       * metadata4allUrisPrep.add(new MultiGetRequest.Item(ES_INDEX, ES_TYPE,
       * formatedId).fields(fields.toArray(new String[fields.size()])));
       */

    }

    MultiGetResponse metadata4allUris = metadata4allUrisPrep.execute().actionGet();

    System.out.println(metadata4allUris.getResponses().length);

    StringBuffer ttlbuffer = new StringBuffer();

    for (MultiGetItemResponse metadata4allUrisResp : metadata4allUris.getResponses()) {
      // System.out.println("hu");
      // System.out.println(metadata4allUrisResp.getResponse());
      if (metadata4allUrisResp.getResponse().isExists()) {

        Map<String, DocumentField> metadata4allUrisContent =
            metadata4allUrisResp.getResponse().getFields();

        String hitUri = metadata4allUrisContent.get("textgridUri").getValue().toString();
        String format = metadata4allUrisContent.get("format").getValue().toString();
        // System.out.println(hitUri +" - " + format);
        ttlbuffer.append("<" + hitUri + "> <http://purl.org/dc/elements/1.1/format> <textgrid:mime:"
            + format + "> .\n");
      } else {
        // System.out.println("not there: " + metadata4allUrisResp.getResponse().getId());
        ttlbuffer.append("<textgrid:" + metadata4allUrisResp.getResponse().getId()
            + "> <http://textgrid.info/relation-ns#notInElasticsearch> \"true\"^^<http://www.w3.org/2001/XMLSchema#boolean> .\n");
      }
    }

    try {
      scpub.uploadTurtle(IOUtils.toInputStream(ttlbuffer.toString(), Charset.forName("UTF-8")));
    } catch (IOException e) {
      e.printStackTrace();
    }

    System.out.println(ttlbuffer.toString());

  }

  /*
   * public void markNotFoundInSesame(HashSet<String> ids) {
   *
   * StringBuffer sb = new StringBuffer();
   * //sb.append("@prefix tg: <http://textgrid.info/relation-ns#>.\n\n"); for(String id : ids) {
   * //sb.append("<textgrid:"+id+"> tg:notInElasticsearch true.\n"); sb.append("<textgrid:" + id
   * +"> <http://textgrid.info/relation-ns#notInElasticsearch> \"true\"^^<http://www.w3.org/2001/XMLSchema#boolean> .\n"
   * ); }
   *
   * //String ttl = "<textgrid:" + id
   * +"> <http://textgrid.info/relation-ns#notInElasticsearch> \"true\"^^<http://www.w3.org/2001/XMLSchema#boolean> ."
   * ;
   *
   *
   * try { scpub.uploadTurtle(IOUtils.toInputStream(sb.toString(), Charset.forName("UTF-8"))); }
   * catch (IOException e) { e.printStackTrace(); }
   *
   * System.out.println(sb.toString()); }
   */

  public static void printLiteralsFromSparqlResponse(InputStream in) {

    // HashSet<String> urilist = new HashSet<String>();

    try {
      XMLStreamReader r = XMLInputFactory.newInstance()
          .createXMLStreamReader(in);
      while (r.hasNext()) {

        if (r.next() == XMLStreamConstants.START_ELEMENT) {
          if (r.getLocalName().equals("literal")) {
            String lit = r.getElementText();
            // log.debug("uri: " + uri);
            // urilist.add(uri);
            System.out.println(lit);
          }
        }
      }
    } catch (XMLStreamException e) {
      System.out.println("error parsing sparql-result-stream");
      e.printStackTrace();
    }

    // return urilist;
  }

  public static HashSet<String> urisFromSparqlResponse(InputStream in) {

    HashSet<String> urilist = new HashSet<String>();

    try {
      XMLStreamReader r = XMLInputFactory.newInstance()
          .createXMLStreamReader(in);
      while (r.hasNext()) {

        if (r.next() == XMLStreamConstants.START_ELEMENT) {
          if (r.getLocalName().equals("uri")) {
            String uri = r.getElementText();
            // log.debug("uri: " + uri);
            urilist.add(uri);
          }
        }
      }
    } catch (XMLStreamException e) {
      System.out.println("error parsing sparql-result-stream");
      e.printStackTrace();
    }

    return urilist;
  }



  public void moveNonpubRdfTopub() throws IOException {
    SesameClient scpub =
        new SesameClient("http://10.160.254.105:9090/openrdf-sesame/repositories/textgrid-public");
    SesameClient scnonpub =
        new SesameClient("http://10.160.254.105:9090/openrdf-sesame/repositories/textgrid");
    // TransportClient esclient = new TransportClient().addTransportAddress(new
    // InetSocketTransportAddress("10.160.254.105", 9300 ));

    String type = "metadata";
    String index = "textgrid-public";

    String queryString = "PREFIX tg:<http://textgrid.info/relation-ns#>"
        + "SELECT ?uri WHERE {\n" +
        "?x tg:isBaseUriOf ?uri ."
        + "FILTER NOT EXISTS { ?uri tg:isNearlyPublished ?inp } ." +
        "}";

    InputStream sres = scpub.sparql(queryString);
    System.out.println("res there");

    HashSet<String> allUris = urisFromSparqlResponse(sres);
    System.out.println("found uris in sesame: " + allUris.size());
    /*
     * SearchRequestBuilder query = esclient.prepareSearch(index) .setTypes(type)
     * .addFields("textgridUri", "format")
     * .setPostFilter(FilterBuilders.missingFilter("sandbox")).setSize(400000);
     *
     * System.out.println(query);
     *
     * SearchResponse esres = query.execute().actionGet();
     *
     * System.out.println("found in es: " + esres.getHits().totalHits());
     * System.out.println("retrieved from es: " + esres.getHits().getHits().length);
     *
     * //HashSet<String> esUris = new HashSet<String>(); for(SearchHit hit :
     * esres.getHits().getHits()) { String uri =
     * hit.getFields().get("textgridUri").value().toString(); String format =
     * hit.getFields().get("format").value().toString() ; //esUris.add(uri);
     * //System.out.println(uri); if(!allUris.contains(uri)&! format.equals("text/plain")) {
     * System.out.println("add: " + uri +" - " + format); //esUris.add(uri); String cquery =
     * "PREFIX tgl:<http://textgrid.info/namespaces/metadata/language/2010>\n" +
     * "PREFIX tns:<http://textgrid.info/namespaces/metadata/core/2010>\n" +
     * "PREFIX tgr:<http://textgrid.info/namespaces/metadata/agent/2010>\n" +
     * "PREFIX tg:<http://textgrid.info/relation-ns#>\n" +
     * "PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
     * "PREFIX ore:<http://www.openarchives.org/ore/terms/>\n" +
     * "PREFIX tgs:<http://textgrid.info/namespaces/metadata/script/2010>\n" +
     * "PREFIX owl:<http://www.w3.org/2002/07/owl#>" +
     * "PREFIX xsi:<http://www.w3.org/2001/XMLSchema-instance>" + "CONSTRUCT " +
     * "{<"+uri+"> ?1p ?1o. " + "?2s ?2p <"+uri+"> }" + " WHERE " + "{<"+uri+"> ?1p ?1o. " +
     * "?2s ?2p <"+uri+"> }";
     *
     * //System.out.println(cquery); InputStream res = scnonpub.constructTurtle(cquery);
     * //IOUtils.copy(res, new FileOutpuStream()); scpub.uploadTurtle(res); } }
     *
     * System.out.println("found in es: " + esres.getHits().totalHits());
     * //System.out.println("not in sesame uris: " + esUris.size());
     * System.out.println("found uris in sesame: " + allUris.size());
     */
    System.out.println("find notset");
  }


  public static void toJSON(ToXContent queryBuilder) {
    try {
      XContentBuilder builder =
          queryBuilder.toXContent(JsonXContent.contentBuilder().prettyPrint(),
              ToXContent.EMPTY_PARAMS);
      System.out.println(Strings.toString(builder));
      System.out.println("???");
    } catch (IOException e) {
      System.out.println("error");
    }
  }

}
