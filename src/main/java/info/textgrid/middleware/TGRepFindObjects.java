package info.textgrid.middleware;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 * <p>
 * Query index databases (such as ElasticSearch) for finding objects with a certain (i) minetype,
 * (ii) modification date, and later maybe more... :-)
 * </p>
 */
@Command(
    name = "find-objects",
    header = "Find repository objects.",
    description = "Find objects in the TextGrid repository index databases using filters.")
public class TGRepFindObjects implements Callable<Integer> {

  // **
  // FINALS
  // **

  private static final String WE_GOT_AN_ERROR = "WE GOT AN ERROR! ";
  private static final String LAST_MODIFIED_FIELD = "lastModified";
  private static final long WARN_LIMIT = 10000;

  // **
  // STATICS
  // **

  private static Logger log = LogManager.getLogger("CLITOOLS");

  // **
  // CLASS
  // **

  @Option(
      names = {"-c", "--config"},
      description = "properties file location, default is /etc/dhrep/dhrep-cli/dhrep-cli.properties",
      defaultValue = "/etc/dhrep/dhrep-cli/dhrep-cli.properties")
  private File propertiesFile;

  @Option(
      names = {"-m", "--mimetype"},
      description = "mimetype filter.")
  private String format;

  @Option(
      names = {"-f", "--from"},
      description = "start date, in format yyyy-MM-dd, default: 2010-01-01")
  private String dateFrom = "2010-01-01";

  @Option(
      names = {"-t", "--to"},
      description = "end date, in format yyyy-MM-dd, default: now")
  private String dateTo;

  protected String index;
  protected RestHighLevelClient esclient;
  protected int batchSize = 1000; // max. is 10000

  /**
   *
   */
  @Override
  public Integer call() {

    try {
      setupFromPropertiesLoc(this.propertiesFile);

      SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd");
      SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

      String startDate = "";
      try {
        startDate = formatter.format(parser.parse(this.dateFrom));
      } catch (ParseException e) {
        log.error("start date needs to be in format yyyy-MM-dd but was: " + this.dateFrom);
        return 111;
      }

      String endDate = "";
      if (this.dateTo != null) {
        try {
          // get timestamp from 1 minutes ago (to make sure, all crud operations finished)
          endDate = formatter.format(parser.parse(this.dateTo));
        } catch (ParseException e) {
          log.error("start date needs to be in format yyyy-MM-dd but was: " + this.dateTo);
          return 222;
        }
      } else {
        endDate = formatter.format(new Date(System.currentTimeMillis() - (1000 * 60)));
        log.warn("no end date given, default to now: " + endDate);
      }

      List<String> result = find(startDate, endDate, this.format);

      for (String uri : result) {
        System.out.println(uri);
      }

    } catch (IOException e) {
      log.error(WE_GOT_AN_ERROR + e.getClass().getName() + "! " + e.getMessage());
      return 333;
    } finally {
      try {
        this.esclient.close();
      } catch (IOException e) {
        // no no
      }
    }

    return 0;
  }

  /**
   * @param propertiesFile
   * @throws IOException
   */
  protected void setupFromPropertiesLoc(final File propertiesFile) throws IOException {

    log.debug("using properties file: " + this.propertiesFile.getCanonicalPath());

    Properties prop = new Properties();
    InputStream input = new FileInputStream(propertiesFile);
    prop.load(input);

    String esUrl = prop.getProperty("elasticSearch.url");
    String[] esPorts = prop.getProperty("elasticSearch.ports").split(",");
    this.index = prop.getProperty("elasticSearch.index");
    this.batchSize = Integer.valueOf(prop.getProperty("elasticSearch.itemLimit"));

    log.debug("=== settings ===");
    log.debug("es host: " + esUrl);
    log.debug("es ports:");
    Arrays.stream(esPorts).forEach(log::debug);
    log.debug("es index: " + this.index);
    log.debug("es item limit: " + this.batchSize);
    log.debug("=== /settings ===");

    List<HttpHost> hosts = new ArrayList<HttpHost>();
    for (String port : esPorts) {
      hosts.add(new HttpHost(esUrl, Integer.parseInt(port), "http"));
    }

    this.esclient =
        new RestHighLevelClient(RestClient.builder(hosts.toArray(new HttpHost[hosts.size()])));
  }

  /**
   * @param from
   * @param to
   * @param theFormat
   * @return
   * @throws IOException
   */
  public List<String> find(final String from, final String to, final String theFormat)
      throws IOException {

    List<String> result = new ArrayList<String>();

    final Scroll scroll = new Scroll(TimeValue.timeValueSeconds(333));

    SearchRequest getRecordList = new SearchRequest(this.index);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

    QueryBuilder fromToBuilder = QueryBuilders.boolQuery()
        .must(QueryBuilders
            .rangeQuery(LAST_MODIFIED_FIELD)
            .from(from).to(to));
    BoolQueryBuilder query = QueryBuilders.boolQuery()
        .must(fromToBuilder);

    if (theFormat != null && !theFormat.isEmpty()) {
      QueryBuilder formatBuilder = QueryBuilders.boolQuery()
          .must(QueryBuilders.termQuery("format.untouched", theFormat));
      query.must(formatBuilder);
    }

    String[] includeFields = new String[] {"textgridUri"};
    String[] excludeFields = Strings.EMPTY_ARRAY;

    searchSourceBuilder
        .query(query)
        .fetchSource(includeFields, excludeFields)
        .sort(new FieldSortBuilder(LAST_MODIFIED_FIELD).order(SortOrder.ASC))
        .size(this.batchSize);

    getRecordList
        .source(searchSourceBuilder)
        .scroll(scroll);

    TgSearchIndexClient.printQuery(searchSourceBuilder);

    SearchResponse getRecordListItems = this.esclient.search(getRecordList, RequestOptions.DEFAULT);

    SearchHits hits = getRecordListItems.getHits();
    String scrollId = getRecordListItems.getScrollId();

    log.debug("found " + hits.getTotalHits() + " in " + this.index);

    if (hits.getTotalHits().value > WARN_LIMIT) {
      log.warn("got more than " + WARN_LIMIT + " hits! please feel free to CTRL-C! :-)");
    }

    int count = 0;
    // Scroll until no hits are returned.
    while (hits.getHits() != null && hits.getHits().length > 0) {

      log.debug("processing hits " + (count + 1) + "-" + (count + hits.getHits().length));

      count += hits.getHits().length;
      for (SearchHit hit : hits) {
        String textgridUri = hit.getSourceAsMap().get("textgridUri").toString();
        result.add(textgridUri);
      }

      SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId).scroll(scroll);
      SearchResponse searchScrollResponse =
          this.esclient.scroll(scrollRequest, RequestOptions.DEFAULT);
      scrollId = searchScrollResponse.getScrollId();
      hits = searchScrollResponse.getHits();
    }

    // Clear scroll after completion.
    ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
    clearScrollRequest.addScrollId(scrollId);
    ClearScrollResponse clearScrollResponse =
        this.esclient.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
    boolean succeeded = clearScrollResponse.isSucceeded();

    if (succeeded) {
      log.debug("scroll cleared");
    } else {
      log.error("error clearing scroll");
    }

    return result;
  }

}
