package info.textgrid.middleware;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.Callable;
import javax.xml.stream.XMLStreamException;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

/**
 *
 */
@Command(
    name = "dhrep-delete",
    header = "Delete objects from the DARIAH-DE Repository.",
    description = "PLEASE USE ONLY IF THE DARIAH-DE COORDINATION OFFICE (DCO) APPROVES! "
        + "Deletion of objects is ONLY ALLOWED under certain circumstances! "
        + "The root collection and all its subcollections/objects will be deleted recursively! "
        + "Please use the --NO-DRY-RUN-REALLY flag ONLY IF EVERYTHING IS APPROVED! "
        + "BTW. dry-run is the default setting! "
        + "Example: /opt/dhrep/cli-tools dhrep-delete 21.T11991/0000-001C-311D-B")
public class DHRepDelete implements Callable<Integer> {

  // **
  // FINALS
  // **

  private static final boolean FOR_DELETION = true;
  private static final String PREFLIGHT = "STAGE I: PREFLIGHT";
  private static final String STAGE2 = "STAGE II: PROCESS HDL METADATA MODIFICATIONS";
  private static final String STAGE3 = "STAGE III: PROCESS DOI METADATA DELETION";
  private static final String STAGE4 = "STAGE IV: DELETING FROM PUBLIC STORAGE";
  private static final String STAGE5 = "STAGE V: DELETING ELASTICSEARCH INDEX DATA";
  private static final String STAGE6 = "STAGE VI: DELETING COLLECTIONS FROM IIIF MANIFEST CACHE";
  private static final String STAGE7 = "STAGE VII: DELETING IMAGES FROM DIGILIB CACHE";
  private static final String STAGE8 = "STAGE VIII: DELETING FROM DHREP COLLECTION REGISTRY";
  private static final String STAGE9 = "STAGE IX: STILL TO DO";

  // **
  // STATICS
  // **

  private static Logger log = LogManager.getLogger("CLITOOLS");

  // **
  // CLASS
  // **

  @Parameters(
      index = "0",
      description = "The root collection HDL to be deleted (recursively!), such as "
          + "\"21.11113/0000-000D-FEDF-B\")")
  private URI rootCollectionURI;

  @Option(
      names = {"-c", "--config"},
      description = "The path to the dhrep-cli config file, default is /etc/dhrep/dhrep-cli/dhrep.properties.",
      defaultValue = "/etc/dhrep/dhrep-cli/dhrep.properties")
  private File dhrepConfiguration;

  @Option(
      names = {"--NO-DRY-RUN-REALLY"},
      description = "Delete the root collection and all sub objects of the given HDL "
          + "recursively and finally? [yes/NO]",
      interactive = true)
  private String noDryrunReally;

  @Option(
      names = {"-p", "--preflight"},
      description = "Just run the preflight stage! Just try it!")
  private boolean preflightOnly;

  // Dry run is default.
  private boolean dryrun = true;

  /**
   * 
   */
  @Override
  public Integer call() {

    // Check for "--no-dry-run-really", set dryrun to falser, if user provided a "yes"!
    if (this.noDryrunReally != null && this.noDryrunReally.equals("yes")) {
      this.dryrun = false;
    }

    // NOTE Make it absolutely clear that we REALLY are DELETING here!!
    if (this.dryrun) {
      log.info("starting dhrep-delete in [DRYRUN] mode");
    } else {
      log.warn("starting dhrep-delete in");
      log.warn(" ");
      log.warn("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      log.warn("!!                                                         !!");
      log.warn("!!   DDDD   EEEEE  L      EEEEE   TTTTT  I   OOO   N   N   !!");
      log.warn("!!   D   D  E      L      E         T    I  O   O  NN  N   !!");
      log.warn("!!   D   D  EEE    L      EEE       T    I  O   O  N N N   !!");
      log.warn("!!   D   D  E      L      E         T    I  O   O  N  NN   !!");
      log.warn("!!   DDDD   EEEEE  LLLLL  EEEEE     T    I   OOO   N   N   !!");
      log.warn("!!                                                         !!");
      log.warn("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      log.warn(" ");
      log.warn("mode!");
    }

    // **
    // I. PREFLIGHT
    // **

    DHRepUtils.logStageStart(PREFLIGHT);

    DHRepConfig config = new DHRepConfig(this.rootCollectionURI, this.dryrun, FOR_DELETION);
    try {
      config.readConfiguration(this.dhrepConfiguration);
      config.checkURISyntax();
      config.initServices();
      config.checkServiceAvailability(this.rootCollectionURI);
      config.generateObjectList();

      int objectsToDelete = config.getCompleteObjectList().size();
      log.info("found " + objectsToDelete + " object" + (objectsToDelete != 1 ? "s" : "")
          + " to delete " + config.getCompleteObjectList());

      config.storeOverallRDFModel();

    } catch (IOException | ParseException | XMLStreamException e) {
      log.error(
          PREFLIGHT + DHRepUtils.STAGE_FAILED + e.getClass().getName() + "! " + e.getMessage());
      return 1;
    }

    // Return of preflight param is set.
    if (this.preflightOnly) {
      return 0;
    }

    // **
    // II. HANDLE METADATA MODIFICATIONS
    // **

    DHRepUtils.logStageStart(STAGE2);

    try {
      config.createAndAddDeletedHDLMetadata();
      config.storeOverallRDFModel();

    } catch (IOException | ParseException e) {
      log.error(STAGE2 + DHRepUtils.STAGE_FAILED + e.getClass().getName() + "! " + e.getMessage());
      return 2;
    }

    // **
    // III. DOI METADATA DELETION
    // **

    DHRepUtils.logStageStart(STAGE3);

    try {
      config.deleteDOIMetadata();
    } catch (IOException e) {
      log.error(STAGE3 + DHRepUtils.STAGE_FAILED + e.getClass().getName() + "! " + e.getMessage());
      return 3;
    }

    // **
    // IV. DELETE FROM PUBLIC STORAGE
    // **

    DHRepUtils.logStageStart(STAGE4);

    String messageTodoStage4 = "Create a GWDG support ticket at: "
        + "<mailto:support@gwdg.de@subject=CDSTAR+DARIAH-DE+Public+Storage+Object+Deletion> "
        + "and ask for deletion of: " + config.getCompleteBagList() + ". "
        + "A complete list of objects to delete is also provided in the log files with name \""
        + config.getBagListFilename() + "\".";
    log.info(DHRepUtils.TODO + messageTodoStage4);

    // **
    // V. DELETE ELASTICSEARCH INDEX DATA
    // **

    DHRepUtils.logStageStart(STAGE5);

    try {
      config.deleteFromElasticSearch();
    } catch (IOException e) {
      log.error(STAGE5 + DHRepUtils.STAGE_FAILED + e.getClass().getName() + "! " + e.getMessage());
      return 5;
    }

    // **
    // VI. DELETING COLLECTIONS FROM IIIF MANIFEST CACHE
    // **

    DHRepUtils.logStageStart(STAGE6);

    try {
      config.deleteIIIFMainfests();
    } catch (IOException e) {
      log.error(STAGE6 + DHRepUtils.STAGE_FAILED + e.getClass().getName() + "! " + e.getMessage());
      return 6;
    }

    // **
    // VII. DELETING IMAGES FROM DIGILIB CACHE
    // **

    DHRepUtils.logStageStart(STAGE7);

    try {
      config.deleteDigilibCache();
    } catch (IOException e) {
      log.error(STAGE7 + DHRepUtils.STAGE_FAILED + e.getClass().getName() + "! " + e.getMessage());
      return 7;
    }

    // **
    // VIII. DELETE FROM DHREP COLLECTION REGISTRY
    // **

    DHRepUtils.logStageStart(STAGE8);

    // Get source URI for Redis DB query.
    String messageTodoStage8 = "";
    try {
      String crURL = config.getCollectionRegistryURL();

      messageTodoStage8 = "Go to <" + crURL
          + "> and delete collection in the DH-rep Collection Registry! API call is coming soon... :-)";
      log.info(DHRepUtils.TODO + messageTodoStage8);

    } catch (IOException e) {
      log.error(STAGE8 + DHRepUtils.STAGE_FAILED + e.getClass().getName() + "! " + e.getMessage());
      return 8;
    }

    // **
    // IX. STILL TO DO
    // **

    DHRepUtils.logStageStart(STAGE9);

    String dryRunPrefix = "";
    if (this.dryrun) {
      dryRunPrefix = DHRepUtils.DRYRUN;
    }
    log.warn(dryRunPrefix + "You have got some TODOs left to complete this deletion:");
    log.warn(dryRunPrefix + "I.  " + messageTodoStage4);
    log.warn(dryRunPrefix + "II. " + messageTodoStage8);
    log.warn(dryRunPrefix + DHRepUtils.INCONVENIENCE);

    // **
    // ALL DONE!
    // **

    return 0;
  }

}
