package info.textgrid.middleware;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.xcontent.ToXContent;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.json.JsonXContent;
import info.textgrid.utils.sesameclient.SesameClient;
import info.textgrid.utils.sesameclient.TGSparqlUtils;

public class TgSearchIndexClient {

  private static Logger log = LogManager.getLogger("CLITOOLS");

  protected String index;
  protected String type;
  protected SesameClient sc;
  protected RestHighLevelClient esclient;

  protected void setupFromPropertiesLoc(File propertiesFile) throws MalformedURLException {

    Properties prop = new Properties();
    InputStream input = null;

    try {
      input = new FileInputStream(propertiesFile);
      prop.load(input);
    } catch (FileNotFoundException e) {
      System.out.println("file not found: " + propertiesFile.getAbsolutePath());
      return;
    } catch (IOException e) {
      System.out.println("something wrong with file access: " + propertiesFile.getAbsolutePath());
      return;
    }

    String triplestoreLoc = prop.getProperty("sesame.endpoint");
    String esUrl = prop.getProperty("elasticSearch.url");
    String[] esPorts = prop.getProperty("elasticSearch.ports").split(",");
    index = prop.getProperty("elasticSearch.index");
    type = prop.getProperty("elasticSearch.type");

    log.info("=== settings ===");
    log.info("triplestore: " + triplestoreLoc);
    log.info("es host: " + esUrl);
    log.info("es ports: ");
    Arrays.stream(esPorts).forEach(log::info);
    log.info("es index: " + index);
    log.info("es type: " + type);
    log.info("=== /settings ===");

    // System.out.println("\npress enter to proceed or ^C to stop");
    // System.in.read();

    sc = new SesameClient(triplestoreLoc);

    List<HttpHost> hosts = new ArrayList<HttpHost>();
    for (String port : esPorts) {
      hosts.add(new HttpHost(esUrl, Integer.parseInt(port), "http"));
    }

    esclient = new RestHighLevelClient(
        RestClient.builder(
            hosts.toArray(new HttpHost[hosts.size()])));

  }

  protected List<String> timedSparqlQuery (String query, boolean showUris) throws IOException {
    log.debug("query: \n"+query);

    Instant start = Instant.now();
    InputStream istream = sc.sparql(query);
    Instant finish = Instant.now();
    float sparqlQueryTime = (float)Duration.between(start, finish).toMillis() / 1000;

    List<String> uriList = TGSparqlUtils.urisFromSparqlResponse(istream);
    if(showUris) {
      log.info("SPARQLtime: "+String.format("%.4f", sparqlQueryTime)+"s | found: " + uriList);
    } else {
      log.info("SPARQLtime: "+String.format("%.4f", sparqlQueryTime)+"s | found: " + uriList.size());
    }
    return uriList;
  }

  public static void printQuery(ToXContent queryBuilder) {
    try {
      XContentBuilder builder =
          queryBuilder.toXContent(JsonXContent.contentBuilder().prettyPrint(),
              ToXContent.EMPTY_PARAMS);
      log.debug(Strings.toString(builder));
    } catch (IOException e) {
      log.error("error printing json");
    }
  }

}
