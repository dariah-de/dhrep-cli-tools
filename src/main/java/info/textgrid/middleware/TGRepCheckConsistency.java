package info.textgrid.middleware;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;
import org.apache.cxf.helpers.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.middleware.common.JPairtree;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.xml.bind.JAXB;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 *
 */
@Command(
    name = "check-consistency",
    header = "Check TextGrid object consistency.",
    description = "Checks consistency of Textgrid objects on object metadata, Handle metadata, and storage level.")
public class TGRepCheckConsistency implements Callable<Integer> {

  // **
  // FINALS
  // **

  private static final int INVALID_FILESIZE_FROM_HDL_SERVICE = -1;
  private static final int TGSEARCH_LIMIT = 1000;

  private static final String STAGE_I = "STAGE I: GET URIS";
  private static final String STAGE_I_COMPLETE = "uri list generation complete";
  private static final String STAGE_II = "STAGE II: CHECK CONSISTENCY";
  private static final String STAGE_II_COMPLETE = "consistency check complete";
  private static final String STAGE_III = "STAGE III: GET STATISTICS";
  private static final String STAGE_III_COMPLETE = "statistic generation complete";

  // **
  // STATICS
  // **

  private static Logger log = LogManager.getLogger("CLITOOLS");

  private static File consistencyLog;
  private static HashMap<String, Integer> cases = new HashMap<String, Integer>();
  private static HashSet<String> mimetypes = new HashSet<String>();
  private static HashSet<String> createdDates = new HashSet<String>();
  private static File metadataDownloadFolder = new File("metadata-download");

  // **
  // CLASS
  // **

  @Option(
      names = {"-c", "--config"},
      description = "The path to the config file, default is /etc/dhrep/dhrep-cli/dhrep-cli.properties.",
      defaultValue = "/etc/dhrep/dhrep-cli/dhrep-cli.properties")
  private File dhrepConfiguration;

  @Option(
      names = {"-p", "--project"},
      description = "Project id filter. usage: '-p TGPR-9b18459d-3a6b-004f-b6aa-4fba2b9e1d3e'")
  private String projectID;

  @Option(
      names = {"-n", "--number"},
      description = "Check only this much objects. usage: '-n 42'")
  private int number;

  @Option(
      names = {"-m", "--mimetype"},
      description = "Mimetype filter. usage: '-m image/tiff'")
  private String format;

  @Option(
      names = {"-f", "--filter"},
      description = "More and custom filters. usage: '-f work.vocab.eltec.authorGender:male -f edition.language:srp'")
  private List<String> filters;

  @Option(
      names = {"-q", "--query"},
      description = "The tgsearch query. default is '*'. usage: -q 'adelheid'",
      defaultValue = "*")
  private String query;

  @Option(
      names = {"-s", "--cases"},
      description = "Outputs all known cases of invalidity.")
  private boolean allTheCases;

  @Option(
      names = {"-o", "--order"},
      description = "Orders the URI list alphabetically.",
      defaultValue = "false")
  private boolean order;

  @Option(
      names = {"-d", "--download-tg-metadata"},
      description = "Downloads inconsistent TextGrid metadata from storage to pairtree path, used for testing and backups.")
  private boolean downloadMetadata;

  @Option(
      names = {"-u", "--store-uri-list"},
      description = "Stores the URI list to file with suffix '" + TGRepConfig.URILIST_SUFFIX + "'.")
  private boolean storeUriList;

  /**
   * @throws IOException
   */
  @Override
  public Integer call() throws IOException {

    log.info("starting tgrep-consistency check");

    // Serve -s flag (--cases).
    if (this.allTheCases) {
      String casesMessage =
          """
              Cases of invalidity:
                0. UNKNOWN --> Please call fugu! :)

                1. OK --> Nothing to do!

                2. NO PID --> No handle PID found in TextGrid metadata at all!

                3. No handle filesize, No handle checksum.
                   Example: [fs:2571 2571 NOT_FOUND]
                            [cs:md5:29551f6508c740d926c21f1eab330120
                                md5:29551f6508c740d926c21f1eab330120
                                --]

                4. No handle filesize, no metadata checksum, no handle checksum.
                   Example: [fs:413992 413992 NOT_FOUND]
                            [cs:md5:52b5cd0ca3ab3e377147fc05e603a1d9
                                --
                                --]

                5. Different handle filesize, different handle checksum.
                   Example: [fs:11 11 11674866]
                            [cs:md5:c520c311494e61ef13c4961a3238845c
                                md5:c520c311494e61ef13c4961a3238845c
                                md5:cc11f6ed06ba8ede50b97b0a284fbfb6]

                6. Two filesizes equal, handle filesize is -1, only storage checksum.
                   Example: [fs:582 582 -1]
                            [cs:md5:ca86416c0db5b33b0472730efc317fa6
                                --
                                --]

                7. Storage and metadata filesizes differ, handle filesize is -1, only storage
                   checksum.
                   Example: [fs:13056 13038 -1]
                            [cs:md5:cc8334fe9a28e950702af8090f2c3cf1
                                --
                                --]

                8. Handle filesizes OK, handle checksum differs.
                   Example: [fs:517 517 517]
                            [cs:md5:3f3fbecb584152c3e3c3814eb3734d0e
                                md5:3f3fbecb584152c3e3c3814eb3734d0e
                                md5:6c293a866e9cb096528eed011e1a378b]

                9. No handle filesize, checksums OK.
                   Example: [fs:11 11 NOT_FOUND]
                            [cs:md5:c520c311494e61ef13c4961a3238845c
                                md5:c520c311494e61ef13c4961a3238845c
                                md5:c520c311494e61ef13c4961a3238845c]
              """;
      System.out.println(casesMessage);
      System.exit(0);
    }

    // Serve -d flag (--download-tg-metadata).
    try {
      if (this.downloadMetadata) {
        metadataDownloadFolder.mkdir();

        log.info("inconsistent metadata will be downloaded to "
            + metadataDownloadFolder.getCanonicalPath());
      }
    } catch (IOException e) {
      log.error(e.getClass().getName() + ": " + e.getMessage());
    }

    long startTime = System.currentTimeMillis();
    String logFilename = String.valueOf(startTime);

    // **
    // PREFLIGHT
    // **

    DHRepUtils.logStageStart(TGRepConfig.PREFLIGHT);

    try {

      doPreflight();

    } catch (IOException | CrudClientException e) {
      log.error(TGRepConfig.PREFLIGHT_FAILED + e.getClass().getName() + ": " + e.getMessage());
      return 1;
    }

    log.info(TGRepConfig.PREFLIGHT_COMPLETE);

    // **
    // STAGE I: GET URIS
    // **

    DHRepUtils.logStageStart(STAGE_I);

    List<String> uris = doStageI();

    // Store URI list, if configured so.
    if (this.storeUriList) {
      File uriListFile = new File(logFilename + TGRepConfig.URILIST_SUFFIX);
      try (FileWriter uriWriter = new FileWriter(uriListFile)) {
        for (String uri : uris) {
          uriWriter.append(uri + "\n");
        }
      }

      log.info("uri list written to: " + uriListFile.getName());
    }

    log.info(STAGE_I_COMPLETE);

    // **
    // STAGE II: CHECK CONSISTENCY
    // **

    DHRepUtils.logStageStart(STAGE_II);

    try {

      doStageII(uris, this.downloadMetadata, metadataDownloadFolder, this.number, logFilename);

    } catch (CrudClientException | IOException | JSONException e) {
      log.error(TGRepConfig.STAGE_II_FAILED + e.getClass().getName() + ": " + e.getMessage());
      return 1;
    }

    log.info(STAGE_II_COMPLETE);

    // **
    // STAGE III: GET STATISTICS
    // **

    DHRepUtils.logStageStart(STAGE_III);

    doStageIII();

    log.info(STAGE_III_COMPLETE);

    // **
    // ALL DONE!
    // **

    long finishTime = System.currentTimeMillis();
    float totalTime = finishTime - startTime;

    log.info("DURATION: " + String.format("%.1f", (totalTime / 1000)) + " seconds" + " or "
        + String.format("%.1f", (totalTime / 1000 / 60)) + " mimutes" + " or "
        + String.format("%.2f", (totalTime / 1000 / 60 / 60)) + " hours");
    log.info("consistency log written to: " + consistencyLog.getName());

    return 0;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Generate JSON output string per URI, examine outcome and identify cases. Cases see -s option!
   * </p>
   * 
   * @param theUri
   * @param theJsonString
   * @param theWriter
   * @return
   * @throws JSONException
   * @throws IOException
   */
  private static String getJsonOutcome(final String theUri, final String theJsonString,
      final FileWriter theWriter) throws JSONException, IOException {

    String result = "";

    String outLine = theUri;

    long filesizeStorage = 0;
    long filesizeTgMeta = 0;
    long filesizeHdlMeta = 0;
    String checksumStorage = "";
    String checksumTgMeta = "";
    String checksumHdlMeta = "";

    // Parse JSON.
    JSONObject json = new JSONObject(theJsonString);

    // Get PID, mimetype, and created date.
    String hdl = json.getString(TGRepConfig.HDL);
    String mimetype = json.getString(TGRepConfig.MIMETYPE);
    String created = json.getString(TGRepConfig.CREATED);

    // Get filesize values.
    JSONObject filesize = json.getJSONObject(TGRepConfig.FILESIZE);

    filesizeStorage = filesize.getLong(TGRepConfig.STORAGE);
    filesizeTgMeta = filesize.getLong(TGRepConfig.METADATA);
    filesizeHdlMeta = filesize.getLong(TGRepConfig.HANDLE);

    result += "[fs:" + TGRepConfig.getCommaSeparatedFilesizeBlock(filesizeStorage, filesizeTgMeta,
        filesizeHdlMeta) + "] ";
    outLine += "," + hdl + "," + TGRepConfig.getCommaSeparatedFilesizeBlock(filesizeStorage,
        filesizeTgMeta, filesizeHdlMeta);

    // Get checksums.
    JSONObject checksum = json.getJSONObject(TGRepConfig.CHECKSUM);

    checksumStorage = TGRepConfig.normalise(checksum.getString(TGRepConfig.STORAGE));
    checksumTgMeta = TGRepConfig.normalise(checksum.getString(TGRepConfig.METADATA));
    checksumHdlMeta = TGRepConfig.normalise(checksum.getString(TGRepConfig.HANDLE));

    result += "[cs:" + TGRepConfig.getCommaSeparatedChecksumBlock(checksumStorage, checksumTgMeta,
        checksumHdlMeta) + "] ";
    outLine += "," + TGRepConfig.getCommaSeparatedChecksumBlock(checksumStorage, checksumTgMeta,
        checksumHdlMeta);

    // Case 9 (no pid in metadata found).
    if (hdl.equals(TGRepConfig.NOT_IN_METADATA)) {
      cases.put(theUri, 2);
    }
    // Case 3.
    else if (filesizeStorage >= 0
        && filesizeStorage != TGRepConfig.FILESIZE_EMPTY_OR_NOT_FOUND
        && filesizeStorage == filesizeTgMeta
        && filesizeHdlMeta == TGRepConfig.FILESIZE_EMPTY_OR_NOT_FOUND
        && !checksumStorage.isEmpty()
        && checksumStorage.equals(checksumTgMeta)
        && checksumHdlMeta.isEmpty()) {
      cases.put(theUri, 3);
    }
    // Case 4.
    else if (filesizeStorage >= 0
        && filesizeStorage != TGRepConfig.FILESIZE_EMPTY_OR_NOT_FOUND
        && filesizeStorage == filesizeTgMeta
        && filesizeHdlMeta == TGRepConfig.FILESIZE_EMPTY_OR_NOT_FOUND
        && !checksumStorage.isEmpty()
        && checksumTgMeta.isEmpty()
        && checksumHdlMeta.isEmpty()) {
      cases.put(theUri, 4);
    }
    // Case 5.
    else if (filesizeStorage >= 0
        && filesizeStorage != TGRepConfig.FILESIZE_EMPTY_OR_NOT_FOUND
        && filesizeStorage == filesizeTgMeta
        && filesizeHdlMeta >= 0
        && filesizeHdlMeta != TGRepConfig.FILESIZE_EMPTY_OR_NOT_FOUND
        && filesizeHdlMeta != filesizeStorage
        && !checksumStorage.isEmpty()
        && !checksumTgMeta.isEmpty()
        && !checksumHdlMeta.isEmpty()
        && checksumStorage.equals(checksumTgMeta)
        && !checksumTgMeta.equals(checksumHdlMeta)) {
      cases.put(theUri, 5);
    }
    // Case 6.
    else if (filesizeStorage >= 0
        && filesizeTgMeta >= 0
        && filesizeStorage == filesizeTgMeta
        && filesizeHdlMeta == INVALID_FILESIZE_FROM_HDL_SERVICE
        && !checksumStorage.isEmpty()
        && checksumTgMeta.isEmpty()
        && checksumHdlMeta.isEmpty()) {
      cases.put(theUri, 6);
    }
    // Case 7.
    else if (filesizeStorage >= 0
        && filesizeTgMeta >= 0
        && filesizeStorage != filesizeTgMeta
        && filesizeHdlMeta == INVALID_FILESIZE_FROM_HDL_SERVICE
        && !checksumStorage.isEmpty()
        && checksumTgMeta.isEmpty()
        && checksumHdlMeta.isEmpty()) {
      cases.put(theUri, 7);
    }
    // Case 8.
    else if (json.getString(TGRepConfig.FILESIZE_STATUS).equals(TGRepConfig.OK)
        && !checksumStorage.isEmpty()
        && !checksumTgMeta.isEmpty()
        && !checksumHdlMeta.isEmpty()
        && checksumStorage.equals(checksumTgMeta)
        && !checksumTgMeta.equals(checksumHdlMeta)) {
      cases.put(theUri, 8);
    }
    // Case 9.
    else if (json.getString(TGRepConfig.CHECKSUM_STATUS).equals(TGRepConfig.OK)
        && filesizeStorage >= 0
        && filesizeTgMeta >= 0
        && filesizeStorage == filesizeTgMeta
        && filesizeHdlMeta == TGRepConfig.FILESIZE_EMPTY_OR_NOT_FOUND) {
      cases.put(theUri, 9);
    }
    // Case 0: a new one!
    else {
      cases.put(theUri, 0);
    }

    if (cases.get(theUri) != null) {

      result += "[case:" + cases.get(theUri) + "]";
      outLine += "," + cases.get(theUri);

      // Set mimetypes and created dates.
      if (mimetype != null) {
        mimetypes.add(mimetype);
        outLine += "," + mimetype;
      }
      if (created != null) {
        createdDates.add(created);
        outLine += "," + created;
      }
    }

    theWriter.write(outLine + "\n");
    theWriter.flush();

    return result;
  }

  /**
   * @param theUri
   * @param theResponse
   * @param theWriter
   * @param theCount
   * @param theHits
   * @throws JSONException
   * @throws IOException
   */
  private static boolean check(String theUri, jakarta.ws.rs.core.Response theResponse,
      FileWriter theWriter, int theCount, int theHits) throws JSONException, IOException {

    boolean result = false;

    String outcome = "<" + theCount + "/" + theHits + "> " + theUri + " -> ";

    int status = theResponse.getStatus();

    // 204 NO CONTENT --> all OK! No HTTP body!
    if (status == Status.NO_CONTENT.getStatusCode()) {

      log.debug(outcome + "[ok]");

      cases.put(theUri, 1);
      result = true;
    }

    // 200 OK --> JSON response containing errors!
    else if (status == Status.OK.getStatusCode()) {
      String jsonString = IOUtils.readStringFromStream(theResponse.readEntity(InputStream.class));

      log.warn(outcome + getJsonOutcome(theUri, jsonString, theWriter));
    }

    return result;
  }

  /**
   * <p>
   * PREFLIGHT
   * </p>
   * 
   * @throws IOException
   * @throws CrudClientException
   */
  private void doPreflight() throws IOException, CrudClientException {

    // Check configuration.
    if (!this.dhrepConfiguration.exists()) {
      String message =
          TGRepConfig.CONFIG_FILE_NOT_FOUND + this.dhrepConfiguration.getCanonicalPath();
      throw new FileNotFoundException(message);
    }

    // Get service configuration.
    Properties p = new Properties();
    p.load(new FileInputStream(this.dhrepConfiguration));

    // Common settings.
    String tgcrudPublicUrl = p.getProperty(TGRepConfig.TGCRUD_ENDPOINT);
    String tgsearchPublicUrl = p.getProperty(TGRepConfig.TGSEARCH_ENDPOINT);

    log.info(TGRepConfig.CONFIG_FILE_APPROVED);

    // Get clients.
    TGRepConfig.initTgcrud(tgcrudPublicUrl);

    String version = TGRepConfig.tgcrudPublic.getVersion().execute();

    log.debug("tgcrud pubic " + version + " initialisation complete [" + tgcrudPublicUrl
        + "]");

    TGRepConfig.initTgsearch(tgsearchPublicUrl);

    log.debug("tgsearch pubic initialisation complete [" + tgsearchPublicUrl + "]");
  }

  /**
   * <p>
   * STAGE I: GET URIS
   * </p>
   * 
   * @return
   */
  private List<String> doStageI() {

    List<String> result = new ArrayList<String>();

    // Set filters, if any.
    List<String> allFilters = new ArrayList<String>();
    if (this.projectID != null && !this.projectID.isEmpty()) {
      allFilters.add("project.id:" + this.projectID);
    }
    if (this.format != null && !this.format.isEmpty()) {
      allFilters.add("format:" + this.format);
    }
    if (this.filters != null && !this.filters.isEmpty()) {
      for (String f : this.filters) {
        allFilters.add(f);
      }
    }

    // First call for amount of hits!
    log.debug(
        "search params (hits): query=" + this.query + ", limit=1" + ", filters=" + allFilters);

    Response response = TGRepConfig.tgsearchPublic.searchQuery()
        .setQuery(this.query)
        .setLimit(1)
        .setFilterList(allFilters)
        .execute();

    int hits = Integer.parseInt(response.getHits());

    log.info("found " + hits + " uri" + (hits != 1 ? "s" : ""));

    // Loop until hits is reached.
    log.debug("search params (uris): query=" + this.query + ", limit=" + TGSEARCH_LIMIT
        + ", filters=" + allFilters);

    for (int start = 0; start < hits; start += TGSEARCH_LIMIT) {

      // Check for number param.
      if (this.number > 0 && start > this.number) {
        log.info("stopping at " + this.number);
        break;
      }

      response = TGRepConfig.tgsearchPublic.searchQuery()
          .setQuery(this.query)
          .setStart(start)
          .setLimit(TGSEARCH_LIMIT)
          .setFilterList(allFilters)
          .execute();

      int end = start + TGSEARCH_LIMIT;
      if (end > hits) {
        end = hits;
      }

      log.debug("fetching uris " + (start + 1) + "-" + end + " of " + hits);

      // Lambda function for getting TextGrid URIs from TG-search response only.
      List<String> partUris = response
          .getResult()
          .stream()
          .limit(TGSEARCH_LIMIT)
          .map(e -> e.getObject().getGeneric().getGenerated().getTextgridUri().getValue())
          .collect(Collectors.toList());

      // Add URIs from this run.
      result.addAll(partUris);
    }

    // Sort URI list.
    String order = "unsorted";
    if (this.order) {
      Collections.sort(result);
      order = "alphabetically";
    }
    log.info("order of uri list: " + order);

    return result;
  }

  /**
   * <p>
   * STAGE II: CHECK CONSISTENCY
   * </p>
   * 
   * @param theUris
   * @param downloadMetadata
   * @param theMetadataRoot
   * @param theNumber
   * @param theLogFilename
   * @throws JSONException
   * @throws IOException
   * @throws CrudClientException
   */
  private static void doStageII(List<String> theUris, boolean downloadMetadata,
      File theMetadataRoot, int theNumber, String theLogFilename)
      throws JSONException, IOException, CrudClientException {

    consistencyLog = new File(String.valueOf(theLogFilename + TGRepConfig.CC_LOG_SUFFIX));
    int hits = theUris.size();

    try {
      consistencyLog.createNewFile();
    } catch (IOException e) {
      String message =
          TGRepConfig.PREFLIGHT_FAILED + e.getClass().getName() + "! " + e.getMessage();
      log.error(message);
      System.exit(7);
    }

    try (FileWriter fw = new FileWriter(consistencyLog)) {
      fw.write(TGRepConfig.CSV_HEADER);

      log.info("writing output to: " + consistencyLog.getAbsolutePath());

      // Loop URIs for consistency check.
      int count = 0;
      for (String u : theUris) {

        // Check for number param.
        if (theNumber > 0 && count == theNumber) {
          log.debug("stopping at " + theNumber);
          break;
        }
        count++;

        boolean isConsistent =
            check(u, TGRepConfig.tgcrudPublic.checkConsistency().setTextgridUri(u).execute(), fw,
                count, (theNumber > 0 ? theNumber : hits));

        // Download metadata from TG-crud (if so configured and if inconsistent).
        if (downloadMetadata && !isConsistent) {

          MetadataContainerType metadata =
              TGRepConfig.tgcrudPublic.readMetadata().setTextgridUri(u).execute();

          JPairtree pairtreeForUri = new JPairtree(u);
          File metadataFile = new File(pairtreeForUri.createPPathFolders(theMetadataRoot),
              pairtreeForUri.getPtreeEncodedId() + ".meta");

          try (FileWriter writeTheMetadataFile = new FileWriter(metadataFile)) {
            JAXB.marshal(metadata, writeTheMetadataFile);
            writeTheMetadataFile.close();

            log.debug("metadata downloaded to " + metadataFile.getCanonicalPath());
          }
        }
      }

      fw.close();
    }
  }

  /**
   * <p>
   * STAGE III: GET STATISTICS
   * </p>
   */
  private static void doStageIII() {

    int case0 = 0;
    int case1 = 0;
    int case2 = 0;
    int case3 = 0;
    int case4 = 0;
    int case5 = 0;
    int case6 = 0;
    int case7 = 0;
    int case8 = 0;
    int case9 = 0;

    for (String u : cases.keySet()) {
      int c = cases.get(u);
      switch (c) {
        case 0:
          case0++;
          break;
        case 1:
          case1++;
          break;
        case 2:
          case2++;
          break;
        case 3:
          case3++;
          break;
        case 4:
          case4++;
          break;
        case 5:
          case5++;
          break;
        case 6:
          case6++;
          break;
        case 7:
          case7++;
          break;
        case 8:
          case8++;
          break;
        case 9:
          case8++;
          break;
      }
    }

    log.info("              VALID -> " + case1);
    log.info("  -----------------");
    log.info("            INVALID -> "
        + (case0 + case2 + case3 + case4 + case5 + case6 + case7 + case8 + case9));
    log.info("            case 2: " + case2 + (case2 > 0 ? " <- NO PID" : ""));
    log.info("            case 3: " + case3);
    log.info("            case 4: " + case4);
    log.info("            case 5: " + case5);
    log.info("            case 6: " + case6);
    log.info("            case 7: " + case7);
    log.info("            case 8: " + case8);
    log.info("            case 9: " + case9);
    log.info("  -----------------");
    log.info(
        "            case 0: " + case0 + (case0 > 0 ? " <- UNKNOWN -> please call fu! :)" : ""));
  }

}
