package info.textgrid.middleware;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@Command(
    name="prescale",
    header="Fill or update the tgrep digilib prescale directory.",
    description="Use this tool to fill or update the digilib prescale directory. "
        + "It gets a list of textgrid objects where format starts with \"image\" "
        + "and asks tg-crud to send an update notification to the prescaler-messagebean "
        + "calling the recache-operation."
)
public class TriggerPrescaleJob extends TgSearchIndexClient implements Callable<Integer>{

  @Parameters(
      index = "0",
      description = "properties file location, e.g. /etc/dhrep/tgsearch/tgsearch-public.properties")
  private File propertiesFile;

  @Option(
      names = {"-f", "--from"},
      description = "start date, in format yyyy-MM-dd, default: 2010-01-01")
  private String date_from;

  @Option(
      names = {"-t", "--to"},
      description = "end date, in format yyyy-MM-dd, default: now")
  private String date_to;

  @Option(
      names = {"-h", "--howmany"},
      description = "size of an elasticsearch result, default: 10000")
  private int howmany = 10000;

  private SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" );

  @Override
  public Integer call() throws Exception {
    setupFromPropertiesLoc(propertiesFile);

    Date from = dateFormat.parse("2010-01-01");
    Date to = new Date();

    if (date_from != null) {
      from = dateFormat.parse(date_from);
    } else {
      System.out.println("No start date given, default to: " + dateFormat.format(from));
    }

    if (date_to != null) {
      to = dateFormat.parse(date_to);
    } else {
      System.out.println("No end date given, default to: " + dateFormat.format(to));
    }

    listImages(from, to);
    esclient.close();
    return 0;
  }

  public void listImages(Date from, Date to) throws Exception {

    final Scroll scroll = new Scroll(TimeValue.timeValueHours(24L));

    SearchRequest getImageList = new SearchRequest(index);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

    RangeQueryBuilder range = QueryBuilders.rangeQuery("lastModified").from(from).to(to);

    BoolQueryBuilder imageFilter =
      QueryBuilders.boolQuery()
        .must(QueryBuilders.prefixQuery("format", "image"))
        .must(range);

    String[] includeFields = new String[] {"textgridUri", "lastModified", "format"};
    String[] excludeFields = Strings.EMPTY_ARRAY;

    searchSourceBuilder
        .query(imageFilter)
        .fetchSource(includeFields, excludeFields)
        .sort(new FieldSortBuilder("lastModified").order(SortOrder.ASC))
        .size(howmany);

    getImageList
      .source(searchSourceBuilder)
      .scroll(scroll);

    //printQuery(searchSourceBuilder);

    SearchResponse searchResponse = esclient.search(getImageList, RequestOptions.DEFAULT);
    String scrollId = searchResponse.getScrollId();

    SearchHits hits = searchResponse.getHits();

    System.out.println("Found " + hits.getTotalHits() + " images in the timerange from "
        + dateFormat.format(from) + " to " + dateFormat.format(to) + " in " + index);

    //System.out.println("\npress enter to proceed or ^C to stop");
    //System.in.read();

    System.out.println("\nhits: " + hits.getHits().length + " - scrollId: " + scrollId);

    //Scroll until no hits are returned
    while(hits.getHits() != null && hits.getHits().length > 0) {

      processHits(hits);

      SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId).scroll(scroll);
      SearchResponse searchScrollResponse = esclient.scroll(scrollRequest, RequestOptions.DEFAULT);
      scrollId = searchScrollResponse.getScrollId();
      hits = searchScrollResponse.getHits();
      System.out.println("\nhits: " + hits.getHits().length + " - scrollId: " + scrollId);
    }

    // clear scroll after completion
    ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
    clearScrollRequest.addScrollId(scrollId);
    ClearScrollResponse clearScrollResponse = esclient.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
    boolean succeeded = clearScrollResponse.isSucceeded();

    if(succeeded) {
      System.out.println("scroll cleared");
    } else {
      System.out.println("error clearing scroll");
    }

  }

  private void processHits(SearchHits hits) throws Exception {

    for (SearchHit hit : hits) {
      String uri = hit.getSourceAsMap().get("textgridUri").toString();
      String format = hit.getSourceAsMap().get("format").toString();
      Date lastModified = dateFormat.parse(hit.getSourceAsMap().get("lastModified").toString());

      URL url = new URL("http://localhost:80/1.0/tgcrud/rest/"+uri+"/recache");

      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setRequestMethod("GET");
      int responseCode = con.getResponseCode();

      if (responseCode == HttpURLConnection.HTTP_OK) { // success
        System.out.println(uri + " ("+format + ") / last modified: " + dateFormat.format(lastModified) + " - tgcrud recache: OK");
      } else {
        System.out.println("ERROR with tgcrud recache: " + uri);
      }

    }

  }

}
