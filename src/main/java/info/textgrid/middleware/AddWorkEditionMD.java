package info.textgrid.middleware;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@Command(
    name="ewmd",
    header="Add work-editon metadata to xml objects in elasticsearch index.",
    description="Elasticsearch will have no edition and work metadata available for single textgrid items. "
        + "Instead of trying complicated joins we decided to duplicate the metadata to make facets "
        + "on authors or other edition/work fields possible on TEI-fulltext searches in the public "
        + "repository (on https://textgridrep.org). ewmd takes care about this."
)
public class AddWorkEditionMD extends TgSearchIndexClient implements Callable<Integer> {

  private static Logger log = LogManager.getLogger("CLITOOLS");

  private int batch_size = 1000; // max is 10000

  @Parameters(
      index = "0",
      description = "properties file location, e.g. /etc/dhrep/tgsearch/tgsearch-public.properties")
  private File propertiesFile;

  @Option(
      names = {"-f", "--from"},
      description = "start date, in format yyyy-MM-dd, default: 2010-01-01")
  private String date_from = "2010-01-01";

  @Option(
      names = {"-t", "--to"},
      description = "end date, in format yyyy-MM-dd, default: now")
  private String date_to;

  @Option(
      names = {"-d", "--dry-run"},
      description = "do not write to elasticsearch, just query")
  private boolean dryRun;

  @Override
  public Integer call() throws IOException {

    // setup es6 and sesame
    setupFromPropertiesLoc(propertiesFile);

    SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat formater = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss" );

    String startDate = "";
    try {
      startDate = formater.format(parser.parse(date_from));
    } catch (ParseException e) {
      log.error("start date needs to be in format yyyy-MM-dd but was: " + date_from);
      System.exit(1);
    }

    String endDate="";
    if (date_to != null) {
      try {
        // get timestamp from 1 minutes ago (to make sure, all crud operations finished)
        endDate = formater.format(parser.parse(date_to));
      } catch (ParseException e) {
        log.error("start date needs to be in format yyyy-MM-dd but was: " + date_to);
        System.exit(1);
      }
    } else {
      endDate = formater.format(new Date(System.currentTimeMillis() - (1000*60)));
      log.info("No end date given, default to now: " + endDate);
    }

    attachEditionWorkMeta(startDate, endDate);
    esclient.close();
    return 0;

  }

  public void attachEditionWorkMeta(String from, String to) throws IOException {

    /**
     * Get all text/xml objects which missing the edition and work field
     */

    final Scroll scroll = new Scroll(TimeValue.timeValueHours(24L));

    SearchRequest getRecordList = new SearchRequest(index);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

    BoolQueryBuilder xmlWithoutEditionFilter =
        QueryBuilders.boolQuery()
            .mustNot(QueryBuilders.existsQuery("edition"))
            .must(QueryBuilders.termQuery("format.untouched", "text/xml"))
            .must(QueryBuilders.rangeQuery("lastModified").from(from).to(to));

    String[] includeFields = new String[] {"textgridUri"};
    String[] excludeFields = Strings.EMPTY_ARRAY;

    searchSourceBuilder
        .query(xmlWithoutEditionFilter)
        .fetchSource(includeFields, excludeFields)
        .sort(new FieldSortBuilder("lastModified").order(SortOrder.ASC))
        .size(batch_size);

    getRecordList
      .source(searchSourceBuilder)
      .scroll(scroll);

    printQuery(searchSourceBuilder);

    SearchResponse getRecordListItems = esclient.search(getRecordList, RequestOptions.DEFAULT);

    SearchHits hits = getRecordListItems.getHits();
    String scrollId = getRecordListItems.getScrollId();

    log.info("Found " + hits.getTotalHits() + " in " + index);
    log.info("hits: " + hits.getHits().length + " - scrollId: " + scrollId);

    //Scroll until no hits are returned
    while(hits.getHits() != null && hits.getHits().length > 0) {
      processHits(hits);

      SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId).scroll(scroll);
      SearchResponse searchScrollResponse = esclient.scroll(scrollRequest, RequestOptions.DEFAULT);
      scrollId = searchScrollResponse.getScrollId();
      hits = searchScrollResponse.getHits();
      log.info("hits: " + hits.getHits().length + " - scrollId: " + scrollId);
    }

    // clear scroll after completion
    ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
    clearScrollRequest.addScrollId(scrollId);
    ClearScrollResponse clearScrollResponse = esclient.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
    boolean succeeded = clearScrollResponse.isSucceeded();

    if(succeeded) {
      log.info("scroll cleared");
    } else {
      log.info("error clearing scroll");
    }

  }


  private void processHits(SearchHits hits) throws IOException {

    for (SearchHit hit : hits) {
      String textgridUri = hit.getSourceAsMap().get("textgridUri").toString();
      log.info("get edworkmeta for " + textgridUri + " ==> ");

      Instant start = Instant.now();
      String parentEditionUri = findClosestParentEdition(textgridUri);
      Instant finish = Instant.now();
      float sparqlQueryTime = (float)Duration.between(start, finish).toMillis() / 1000;
      log.info("ParentEditionRecursiveQuery took: "+String.format("%.4f", sparqlQueryTime)+"s | found: " + parentEditionUri);

      /**
       * If the parentUri is not empty fetch the edition field of the edition and the work field of the
       * responding work element
       */
      if (!parentEditionUri.equals("")) {
        String uri = parentEditionUri.substring(9);

        String[] includes = new String[] {"edition"};
        String[] excludes = Strings.EMPTY_ARRAY;
        FetchSourceContext fetchSourceContext =
            new FetchSourceContext(true, includes, excludes);
        GetRequest getEdition = new GetRequest(index, type, uri)
            .storedFields("edition.isEditionOf")
            .fetchSourceContext(fetchSourceContext);

        GetResponse editionObject = esclient.get(getEdition, RequestOptions.DEFAULT);

        if (editionObject != null) {
          if (editionObject.isExists()) {
            HashMap editionMap = (HashMap) editionObject.getSourceAsMap().get("edition");
            String workUri = editionMap.get("isEditionOf").toString();
            workUri = workUri.substring(9);

            Map<String, Object> workField = getWorkElement(workUri);
            Map<String, Object> editionField = editionObject.getSource();

            textgridUri = textgridUri.substring(9);
            if(!dryRun) {
              updateElement(textgridUri, editionField, workField);
              log.info("added edition field in elasticsearch to: " + textgridUri);
            } else {
              log.info("DRY RUN: not updating elasticsearch: " + textgridUri);
              log.debug(editionField);
              log.debug(workField);
            }
          } else {
            log.info("edition not found: " + uri);
          }

        } else {
          log.info("no edition found for: " + uri);
        }

      } else {
        log.info("no matching ewmd found");
      }
    }

  }

  // Jackson for dealing with sparql json results
  private JsonPointer parentPointer = JsonPointer.compile("/results/bindings/0/parent/value");
  private JsonPointer formatPointer = JsonPointer.compile("/results/bindings/0/format/value");
  private ObjectMapper mapper = new ObjectMapper();

  private static final String EDITION_MIME = "textgrid:mime:text/tg.edition+tg.aggregation+xml";
/*
  private final static String parentFormatQuery = """
      PREFIX ore:<http://www.openarchives.org/ore/terms/>
      PREFIX dc:<http://purl.org/dc/elements/1.1/>
      PREFIX tg:<http://textgrid.info/relation-ns#>

      SELECT * WHERE {
        ?parent ore:aggregates <%s> .
        ?parent dc:format ?format .
      }
      """;
*/
  private String findClosestParentEdition(String textgridUri) throws IOException {
    // JDK 17
    //String query = parentFormatQuery.formatted(textgridUri);

    // JDK8
    String query = "PREFIX ore:<http://www.openarchives.org/ore/terms/>\n"
                 + "PREFIX dc:<http://purl.org/dc/elements/1.1/>\n"
                 + "PREFIX tg:<http://textgrid.info/relation-ns#>\n"
                 + "\n"
                 + "SELECT * WHERE {\n"
                 + "  ?parent ore:aggregates <"+textgridUri+"> .\n"
                 + "  ?parent dc:format ?format .\n"
                 + "}";

    log.debug("query: \n"+query);

    InputStream istream = sc.sparqlJson(query);
    JsonNode node = mapper.readTree(istream);

    String parent = node.at(parentPointer).asText();
    String format = node.at(formatPointer).asText();

    if(parent.equals("")) {
    // no parent found, break
      return "";
    } else if(format.equals(EDITION_MIME)) {
      // parent is edition return
      return parent;
    } else {
      // parrent is no edition, recurse
      return findClosestParentEdition(parent);
    }
  }

  public Map<String, Object> getWorkElement(String id) throws IOException {

    String[] includes = new String[] {"work"};
    String[] excludes = Strings.EMPTY_ARRAY;
    FetchSourceContext fetchSourceContext =
        new FetchSourceContext(true, includes, excludes);
    GetRequest recordById = new GetRequest(index, type, id)
        .fetchSourceContext(fetchSourceContext);

    GetResponse tgObject = esclient.get(recordById, RequestOptions.DEFAULT);

    Map<String, Object> workField = tgObject.getSource();

    return workField;
  }

  public void updateElement(String id, Map<String, Object> editionField,
      Map<String, Object> workField) throws IOException {

    // System.out.println("update document with id: " + id + " updated part is: " + editionField);
    // System.out.println("update document with id: " + id + " updated part is: " + workField);
    if (editionField != null) {
      UpdateRequest updateRequest =
          new UpdateRequest().index(index).type(type).id(id).doc(editionField, XContentType.JSON);
      UpdateResponse response = esclient.update(updateRequest, RequestOptions.DEFAULT);

    } else {
      log.info("edition md NULL");
    }
    if (workField != null) {
      UpdateRequest updateRequest =
          new UpdateRequest().index(index).type(type).id(id).doc(workField, XContentType.JSON);
      UpdateResponse response = esclient.update(updateRequest, RequestOptions.DEFAULT);
    } else {
      log.info("work md NULL");
    }

  }

}
