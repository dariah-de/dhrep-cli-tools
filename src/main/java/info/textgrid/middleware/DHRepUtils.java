package info.textgrid.middleware;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.http.HttpStatus;
import org.apache.jena.atlas.json.JSON;
import org.apache.jena.atlas.json.JsonArray;
import org.apache.jena.atlas.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.DHCrudService;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;
import redis.clients.jedis.Jedis;

/**
 *
 */
public class DHRepUtils {

  // **
  // FINALS
  // **

  private static final String DHREP_DELETE_LOGID = "dhrep-delete#";
  private static final String ERROR_GETTING_METADATA = "error getting metadata from dhcrud: ";
  private static final String ERROR_GETTING_DATA = "error getting data from dhcrud: ";
  private static final String ERROR_GETTING_HDL_METADATA =
      "error getting handle metadata from hdl service: ";
  private static final String ERROR_GETTING_DOI_METADATA =
      "error getting metadata from doi service: ";
  private static final String ERROR_DELETING_DOI_METADATA =
      "error deleting metadata from doi service: ";
  private static final String ERROR_DELETING_ES_DATA = "error deleting from elasticsearch index: ";
  private static final String ERROR_UPDATING_HDL_METADATA =
      "error updating handle metadata to hdl service: ";
  private static final String REDIS_CRID_PREFIX = "crid_";
  private static final String DARIAH_STORAGE_PREFIX = "https://de.dariah.eu/storage/";
  protected static final String ERROR_GETTING_DATA_FROM_ES =
      "error getting data from elasticsearch: ";
  protected static final String OK = "http ok ";
  protected static final String IGNORING_NOT_FOUND = "ignoring http not found ";
  protected static final String MIMETYPE_JSON = "application/json";
  protected static final String DRYRUN = "[DRYRUN] ";
  protected static final String TODO_FILE_PREFIX = "TODO_DELETE_FROM_STORAGE_";
  protected static final String BAG_LIST_FILE_SUFFIX = ".bags";
  protected static final String CACHE_DIR_NOT_EXISTING = "cache folder not existing: ";
  protected static final String TRY_AGAIN_ON_SERVER = " maybe try again directly on dhrep server?";
  protected static final String TRY_AGAIN_LATER = "service unavailable! please try again later!";

  protected static final String STAGE_FAILED = " FAILED due to a ";
  protected static final String LOG_START_CHARS = "--------========########  ";
  protected static final String LOG_END_CHARS = "  ########========--------";
  protected static final String TODO = "TODO! ";
  protected static final String INCONVENIENCE =
      "We apoligise for the inconvenience! We do! Really! Indeed! :-D";

  // **
  // STATICS
  // **

  private static Logger log = LogManager.getLogger("CLITOOLS");

  /**
   * <p>
   * Get the data from DH-crud.
   * </p>
   * 
   * @param theURI The object's DOI to read data from
   * @param theClient The DH-crud client
   * @return The data string
   * @throws IOException
   */
  public static String getDataFromDHCrud(final URI theURI, final DHCrudService theClient)
      throws IOException {

    String result = "";

    Response r = theClient.read(theURI, 0, DHREP_DELETE_LOGID + System.currentTimeMillis());
    log.debug("fetching data for [" + theURI + "]");

    int statusCode = r.getStatus();
    if (statusCode != HttpStatus.SC_OK) {
      String message = ERROR_GETTING_DATA + statusCode + " " + r.getStatusInfo().getReasonPhrase();
      log.error(message);
      throw new IOException(message);
    }
    result = r.readEntity(String.class);

    return result;
  }

  /**
   * @param theURI The object's HDL URI to read descriptive metadata from
   * @param theClient The DH-crud client
   * @return The data string
   * @throws IOException
   */
  public static String getDMDFromDHCrud(final URI theURI, final DHCrudService theClient)
      throws IOException {

    String result = "";

    Response r = theClient.readMetadataTTL(theURI, DHREP_DELETE_LOGID + System.currentTimeMillis());
    log.debug("fetching descriptive metadata for [" + theURI + "]");

    int statusCode = r.getStatus();
    if (statusCode != HttpStatus.SC_OK) {
      String message =
          ERROR_GETTING_METADATA + statusCode + " " + r.getStatusInfo().getReasonPhrase();
      log.error(message);
      throw new IOException(message);
    }
    result = r.readEntity(String.class);

    return result;
  }

  /**
   * @param theURI The object's HDL URI to read administrative metadata from
   * @param theClient The DH-crud client
   * @return The data string
   * @throws IOException
   */
  public static String getADMMDFromDHCrud(final URI theURI, final DHCrudService theClient)
      throws IOException {

    String result = "";

    Response r = theClient.readAdmMD(theURI, DHREP_DELETE_LOGID + System.currentTimeMillis());
    log.debug("fetching administrative metadata for [" + theURI + "]");

    int statusCode = r.getStatus();
    if (statusCode != HttpStatus.SC_OK) {
      String message =
          ERROR_GETTING_METADATA + statusCode + " " + r.getStatusInfo().getReasonPhrase();
      log.error(message);
      throw new IOException(message);
    }
    result = r.readEntity(String.class);

    return result;
  }

  /**
   * <p>
   * Get the Handle metadata for an object from the HDL service.
   * </p>
   * 
   * @param theURI The URI to get the metadata for
   * @return The Handle Metadata JSON for the given URI
   * @throws IOException
   */
  public static String getHDLMetadata(final URI theURI, final WebClient theClient)
      throws IOException {

    String result = "";

    Response r = theClient.replacePath("/" + theURI.toString()).get();
    log.debug("fetching handle json metadata: " + theClient.getCurrentURI());

    int statusCode = r.getStatus();
    if (statusCode != HttpStatus.SC_OK) {
      String message =
          ERROR_GETTING_HDL_METADATA + statusCode + " " + r.getStatusInfo().getReasonPhrase();
      log.error(message);
      throw new IOException(message);
    }
    result = r.readEntity(String.class);

    return result;
  }

  /**
   * <p>
   * Get the DOI metadata for an object from the DOI service.
   * </p>
   * 
   * @param theURI The URI to get the metadata for
   * @return The DOI Metadata XML for the given URI
   * @throws IOException
   */
  public static String getDOIMetadata(final URI theURI, final String theDOIPrefix,
      final WebClient theClient) throws IOException {

    // curl -H "Content-Type: application/xml" --user USERNAME:PASSWORD
    // https://mds.test.datacite.org/metadata/10.5438/0000-03VC

    String result = "";

    Response r = theClient.replacePath("/" + theDOIPrefix + "/" + omitDOIInstitution(theURI)).get();
    log.debug("fetching doi metadata: " + theClient.getCurrentURI());

    int statusCode = r.getStatus();
    if (statusCode == HttpStatus.SC_OK) {

      log.info(OK + "[" + theURI + "]");

    } else if (statusCode == HttpStatus.SC_NOT_FOUND) {

      log.warn(IGNORING_NOT_FOUND + "[" + theURI + "]");

    } else {
      String message =
          ERROR_GETTING_DOI_METADATA + statusCode + " " + r.getStatusInfo().getReasonPhrase();
      log.error(message);
      throw new IOException(message);
    }
    result = r.readEntity(String.class);

    return result;
  }

  /**
   * <p>
   * Delete the DOI metadata for an object from the DOI service.
   * </p>
   * 
   * @param theURI The URI to delete metadata for
   * @param dryrun DRY RUN or NO DRY RUN
   * @throws IOException
   */
  public static void deleteDOIMetadata(final URI theURI, final String theDOIPrefix,
      final WebClient theClient, boolean dryrun) throws IOException {

    // curl -X DELETE --user USERNAME:PASSWORD
    // https://mds.test.datacite.org/metadata/10.5072/0000-03VC

    if (!dryrun) {

      log.info("deleting doi metadata for [" + theURI + "]");

      Response r =
          theClient.replacePath("/" + theDOIPrefix + "/" + omitDOIInstitution(theURI)).delete();
      log.debug("deleting doi metadata: " + theClient.getCurrentURI());

      int statusCode = r.getStatus();
      if (statusCode == HttpStatus.SC_CREATED || statusCode == HttpStatus.SC_OK) {

        log.info(OK + " [" + theURI + "]");

      } else if (statusCode == HttpStatus.SC_NOT_FOUND) {

        log.warn(IGNORING_NOT_FOUND + "[" + theURI + "]");

      } else {
        String message =
            ERROR_DELETING_DOI_METADATA + statusCode + " " + r.getStatusInfo().getReasonPhrase();
        log.error(message);
        throw new IOException(message);
      }
    } else {
      log.info(DRYRUN + "skipping doi metadata deletion for [" + theURI + "]");
    }
  }

  /**
   * <p>
   * Get the Handle metadata for an object from the HDL service.
   * </p>
   * 
   * @param theURI The URI to get the metadata for
   * @param theJSON The new JSON to put as new Handle metadata
   * @param theClient The HDL web client
   * @param dryrun DRY RUN or NO DRY RUN
   * @return The Handle Metadata JSON for the given URI
   * @throws IOException
   */
  public static String putHDLMetadata(final URI theURI, final String theJSON,
      final WebClient theClient, boolean dryrun) throws IOException {

    // curl -v -u "USER:PASS" -H "Accept:application/json" -H "Content-Type:application/json" -X PUT
    // --data '[INSERT COMPLETE JSON OBJECT HERE, INCLUDING ADDED DELETED FLAG]'
    // https://pid.gwdg.de/handles/21.T11991/0000-001B-4650-A

    String result = "";

    if (!dryrun) {
      Response r = theClient.replacePath("/" + theURI.toString())
          .header(HttpHeaders.ACCEPT, MIMETYPE_JSON).put(theJSON);

      int statusCode = r.getStatus();
      if (statusCode != HttpStatus.SC_NO_CONTENT) {
        String errorMessage =
            ERROR_UPDATING_HDL_METADATA + statusCode + " " + r.getStatusInfo().getReasonPhrase();
        log.error(errorMessage);
        throw new IOException(errorMessage);
      }
      result = r.readEntity(String.class);
    } else {
      log.info(DRYRUN + "skipping hdl json metadata update");
    }

    return result;
  }

  /**
   * <p>
   * Get values from XPath query for file content.
   * </p>
   * 
   * @param theXPathExpression The XPath expression
   * @param theFile The file
   * @return The content of the given XPath expression
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException
   * @throws XPathExpressionException
   */
  public static String getFromXML(final String theXPathExpression, final File theFile)
      throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

    String result = "";

    DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = builderFactory.newDocumentBuilder();
    Document xmlDocument = builder.parse(theFile);

    XPath xPath = XPathFactory.newInstance().newXPath();
    result =
        (String) xPath.compile(theXPathExpression).evaluate(xmlDocument, XPathConstants.STRING);

    return result;
  }

  /**
   * <p>
   * Assembles a citation string for deleted metadata.
   * </p>
   * 
   * @param theCreators The list of creators
   * @param theTitles The list of titles
   * @param theDate The publication date
   * @param thePublisher The publisher
   * @param theDOIPrefix The DOI prefix
   * @param theDOIResolver The DOI resolver
   * @param theURI The URI
   * @return The assembled citation string
   */
  public static String assembleCitation(final List<String> theCreators,
      final List<String> theTitles, final String theDate, final String thePublisher,
      final String theDOIPrefix, final String theDOIResolver, final URI theURI) {
    String result = "";

    // Creators.
    for (String c : theCreators) {
      result += c + ", ";
    }
    result = result.substring(0, result.length() - 2);

    // Date.
    result += " (" + theDate + "). ";

    // Titles.
    for (String t : theTitles) {
      result += t + ". ";
    }
    result = result.substring(0, result.length() - 2);

    // Publisher and DOI.
    result += ". " + thePublisher + ". " + theDOIResolver + "/" + theDOIPrefix + "/"
        + omitDOIInstitution(theURI).toLowerCase();

    return result;
  }

  /**
   * <p>
   * Returns the first occurrence of a JSON type key.
   * </p>
   * 
   * @param theJSON The JSON as string
   * @param theType The JSON type value, such as PUBDATE, or DELETED
   * @return The parsed_data value for the given type, or null if not existing
   */
  public static String checkForJSONEntries(final String theJSON, final String theType) {

    String result = null;

    // Get JSON from string.
    JsonArray a = JSON.parseAny(theJSON).getAsArray();

    for (int i = 0; i < a.size(); i++) {
      JsonObject o = a.get(i).getAsObject();
      if (o.hasKey("type") && o.getString("type").equals(theType)) {
        result = o.getString("parsed_data");
        break;
      }
    }

    return result;
  }

  /**
   * <p>
   * Omit the institution AND the "/" from the PID, such as: "21.T11991/0000-001b-4650-A" to
   * "0000-001b-4650-A".
   * </p>
   * 
   * @param theURI The URI to get the "suffix" from
   * @return The pure PID suffix without institution prefix and without "/"
   */
  public static String omitDOIInstitution(final URI theURI) {
    return theURI.toString().substring(theURI.toString().indexOf("/") + 1);
  }

  /**
   * <p>
   * Delete files from file list.
   * </p>
   * 
   * @param theFiles The list of files to delete
   * @param dryrun DRY RUN or NO DRY RUN
   * @throws IOException
   */
  public static void deleteFiles(List<File> theFiles, boolean dryrun) throws IOException {

    // Loop and delete files.
    for (File f : theFiles) {
      if (!dryrun) {
        boolean deleted = f.delete();
        if (deleted) {
          log.debug("file deleted: " + f.getName());
        } else {
          log.warn("file " + f.getCanonicalPath() + " could not be deleted!");
        }
      } else {
        log.info(DRYRUN + "skipping iiif manifest file deletion: " + f.getName());
      }
    }
  }

  /**
   * <p>
   * Delete object from ElasticSearch.
   * </p>
   * 
   * @param theESClient The ElasticSearch client to use.
   * @param theURI The object's URI to delete from the index.
   * @param dryrun DRY RUN or NO DRY RUN
   * @throws IOException
   */
  public static void deleteObjectFromElasticSearch(WebClient theESClient, URI theURI,
      boolean dryrun) throws IOException {

    theESClient.replacePath("/" + URLEncoder.encode(theURI.toString(), "UTF-8"));

    // DOUBLE CHECK DELETION PATH HERE! THERE MUST BE THE CORRECT URI AS PATH!
    // http://localhost:9202/dariah-public/metadata/21.T11991%2F0000-001C-311D-B
    // check for: endsWith("/21.T11991%2F0000-001C-311D-B").
    String endingToCheck = "/" + URLEncoder.encode(theURI.toString(), "UTF-8");
    if (theESClient.getCurrentURI().toString().endsWith(endingToCheck)) {

      log.info("deletion approved: " + theESClient.getCurrentURI());

      if (!dryrun) {

        Response r = theESClient.delete();

        int statusCode = r.getStatus();
        if (statusCode == HttpStatus.SC_OK) {

          log.info(OK + "[" + theURI + "]");

        } else if (statusCode == HttpStatus.SC_NOT_FOUND) {

          log.warn(IGNORING_NOT_FOUND + "[" + theURI + "]");

        } else {
          String errorMessage =
              ERROR_DELETING_ES_DATA + statusCode + " " + r.getStatusInfo().getReasonPhrase();
          throw new IOException(errorMessage);
        }

      } else {
        log.info(DRYRUN + "skipping elasticsearch deletion: " + theESClient.getCurrentURI());
      }

    } else {
      String message =
          "ELASTICSEARCH PATH ENDING ERROR! PLEASE CONSULT YOUR FAVOURITE PROGRAMMER!! CHECK HTTP#DELETE URL: "
              + theESClient.getCurrentURI();
      throw new IOException(message);
    }
  }

  /**
   * <p>
   * Get the CRID from the Redis DB.
   * </p>
   * 
   * @param theSourceURI The source URI of the collection to get the CR ID for.
   * @param theRedisClient The Redis client
   * @param dryrun DRY RUN or NO DRY RUN
   * @return
   * @throws IOException
   */
  public static String getCRIDFromRedisDB(URI theSourceURI, Jedis theRedisClient, boolean dryrun)
      throws IOException {

    String result = "";

    // key must be like: crid_https://de.dariah.eu/storage/EAEA0-6B21-F75C-B78A-0

    if (theSourceURI == null) {
      return result;
    }

    String storageID =
        theSourceURI.toString().substring(theSourceURI.toString().lastIndexOf("/") + 1);
    String redisKey = REDIS_CRID_PREFIX + DARIAH_STORAGE_PREFIX + storageID;

    String message = "getting crid value for redis db key: " + redisKey;

    log.info(message);

    if (theRedisClient != null) {
      result = theRedisClient.get(redisKey);

      log.debug("crid from redis db: " + result);

    } else {
      message = "no redis client available!" + TRY_AGAIN_ON_SERVER;
      if (dryrun) {
        log.warn(message);
      } else {
        throw new IOException(message);
      }
    }

    return result;
  }

  /**
   * <p>
   * Gets a JSON from ElasticSearch.
   * </p>
   * 
   * @param theURI The URI to get information about.
   * @param theESClient The ElasticSearch client to use.
   * @return The JSON string from ElasticSearch.
   * @throws IOException
   */
  protected static String checkESQuery(URI theURI, WebClient theESClient) throws IOException {

    String result = "";

    theESClient.replacePath("/" + URLEncoder.encode(theURI.toString(), "UTF-8"));

    log.debug("fetching elasticsearch json metadata: " + theESClient.getCurrentURI());

    Response r = theESClient.get();
    int statusCode = r.getStatus();
    if (statusCode == HttpStatus.SC_OK) {

      log.info(OK + "[" + theURI + "]");

    } else if (statusCode == HttpStatus.SC_NOT_FOUND) {

      log.warn(IGNORING_NOT_FOUND + "[" + theURI + "]");

    } else {
      String message =
          ERROR_GETTING_DATA_FROM_ES + statusCode + " " + r.getStatusInfo().getReasonPhrase();
      throw new IOException(message);
    }
    result = r.readEntity(String.class);

    return result;
  }

  /**
   * @param theStageName The name of the processing stage
   */
  protected static void logStageStart(final String theStageName) {
    log.info(LOG_START_CHARS + theStageName.toUpperCase() + LOG_END_CHARS);
  }

  /**
   * @param thePathToManifests
   * @return
   * @throws IOException
   */
  protected static File[] assembleIIIFManifestFileList(String thePathToManifests)
      throws IOException {

    // Folder of files to delete.
    File f = new File(thePathToManifests);
    if (!f.getParentFile().exists()) {
      String message = "iiif manifest " + DHRepUtils.TRY_AGAIN_ON_SERVER + f.getCanonicalPath();
      throw new IOException(message);
    }
    File parent = f.getParentFile();

    // Filename prefix of files to delete.
    String filenamePrefix = f.getName();
    // Filename filter for files to delete.
    FilenameFilter filter = new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        if (name.startsWith(filenamePrefix)) {
          return true;
        }
        return false;
      }
    };

    // Get all files with prefix, to delete! :-)
    return parent.listFiles(filter);
  }

  /**
   * @param theCacheFolder
   * @param theObjectList
   * @return
   */
  protected static List<File> assembleDigilibFileList(File theCacheFolder,
      List<URI> theObjectList) {

    List<File> result = new ArrayList<File>();

    for (URI u : theObjectList) {
      // Get image file name prefix for files to delete (assuming we are using
      // usePrescaleSubdirs=false in Digilib config).
      String filenamePrefix = u.toString().replace("/", "-").replace("%2F", "-");

      // Filename filter for files to delete.
      FilenameFilter filter = new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
          if (name.startsWith(filenamePrefix)) {
            return true;
          }
          return false;
        }
      };

      File fileList[] = theCacheFolder.listFiles(filter);
      if (fileList.length > 0) {
        result.addAll(Arrays.asList(fileList));
      }
    }

    return result;
  }

}
