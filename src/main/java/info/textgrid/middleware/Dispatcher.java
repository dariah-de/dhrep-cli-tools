package info.textgrid.middleware;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.ScopeType;

/**
 *
 */
@Command(
    name = "cli-tools.jar",
    sortOptions = false,
    headerHeading = "Usage:%n%n",
    synopsisHeading = "%n",
    descriptionHeading = "%nDescription:%n%n",
    parameterListHeading = "%nParameters:%n",
    optionListHeading = "%nOptions:%n",
    commandListHeading = "%nCommands:%n",
    // mixinStandardHelpOptions = true,
    header = "DHREP CLI Tools",
    description = "Tools which are used for administrative work on the repositories",
    scope = ScopeType.INHERIT,
    subcommands = {
        AddWorkEditionMD.class,
        Sandboxer.class,
        TriggerPrescaleJob.class,
        TriggerDHPrescaleJob.class,
        DHRepDelete.class,
        DHRepInspect.class,
        TGRepFindObjects.class,
        TGRepCheckConsistency.class,
        TGRepFixInconsistencies.class
    })
public class Dispatcher {

  private static Logger log = LogManager.getLogger("CLITOOLS");

  /**
   * @param verbose
   */
  @Option(
      names = {"-v", "--verbose"},
      scope = ScopeType.INHERIT,
      // Option is shared with sub commands.
      description = "verbose, the more v's, the more verbosity!")
  public void setVerbose(boolean[] verbose) {
    // Configure log4j.
    // (This is a simplistic example: a real application may use more levels and
    // perhaps configure only the ConsoleAppender level instead of the root log level.)
    Configurator.setRootLevel(verbose.length > 0 ? Level.DEBUG : Level.INFO);
  }

  /**
   * @param args
   */
  public static void main(String... args) {
    int exitCode = new CommandLine(new Dispatcher()).execute(args);

    log.trace("yeah! dispatcher dispatching complete!");

    System.exit(exitCode);
  }

}
