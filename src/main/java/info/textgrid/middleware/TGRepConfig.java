package info.textgrid.middleware;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import org.eclipse.microprofile.rest.client.RestClientBuilder;
import info.textgrid.clients.CrudClient;
import info.textgrid.clients.SearchClient;
import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.middleware.pid.api.TGPidService;

/**
 *
 */
public class TGRepConfig {

  // **
  // FINALS
  // **

  protected static final String DRYRUN = "|DRY-RUN|";
  protected static final String PREFLIGHT = "PREFLIGHT";
  protected static final String PREFLIGHT_FAILED = "PREFLIGHT FAILED due to a ";
  protected static final String PREFLIGHT_COMPLETE = "preflight complete";
  protected static final String STAGE_I_FAILED = "STAGE I FAILED due to a ";
  protected static final String STAGE_II_FAILED = "STAGE II FAILED due to a ";
  protected static final String INPUT_FILE_NOT_FOUND = "input file not found: ";
  protected static final String INPUT_FILE_CORRUPT =
      "input file is not a valid check-consistency file: ";
  protected static final String PATH_TO_METADATA_NOT_FOUND = "path to metadata not found: ";
  protected static final String CONFIG_FILE_NOT_FOUND = "config file not found: ";
  protected static final String CONFIG_FILE_APPROVED = "config file location approved";
  protected static final String INPUT_FILE_APPROVED = "input file format approved";
  protected static final String PID_SERVICE_NOT_FOUND = "tgpid config path null or empty: ";
  protected static final String CC_LOG_SUFFIX = "-cc-log.csv";
  protected static final String FI_LOG_SUFFIX = "-fi-log.csv";
  protected static final String URILIST_SUFFIX = ".uris";
  protected static final String TGCRUD_ENDPOINT = "tgcrudPublic.endpoint";
  protected static final String TGCRUD_REINDEX_SECRET = "tgcrudPublic.reindexSecret";
  protected static final String TGPID_ENDPOINT = "tgpid.endpoint";
  protected static final String TGPID_SECRET_PROPERTY = "pid.secret";
  protected static final String TGPID_CONFIG_PATH = "tgpid.config";
  protected static final String TGSEARCH_ENDPOINT = "tgsearchPublic.endpoint";
  protected static final String TG_META_PATH = "tgmeta.path";
  protected static final String CSV_HEADER =
      "TextGrid URI, ePIC Handle, Filesize storage, Filesize metadata, Filesize Handle, Checksum storage, Checksum metadata, Checksum Handle, Case No, Mimetype, CreatedDate\n";
  protected static final String CSV_LOG_HEADER =
      "TextGrid URI, ePIC Handle, OLD Filesize storage, OLD Filesize metadata, OLD Filesize Handle, OLD Checksum storage, OLD Checksum metadata, OLD Checksum Handle, Date of Fixation, NEW Filesize storage, NEW Filesize metadata, NEW Filesize Handle, NEW Checksum storage, NEW Checksum metadata, NEW Checksum Handle, HDL successful, META successful\n";
  protected static final String LINES = "--------------------";
  protected static final String GOAWAY = "GOAWAY";
  protected static final String NIX = "--";
  protected static final String NOT_IN_METADATA = "NOT_IN_METADATA";
  protected static final int FILESIZE_EMPTY_OR_NOT_FOUND = -99;
  protected static final String HDL = "hdl";
  protected static final String STORAGE = "storage";
  protected static final String CHECKSUM = "checksum";
  protected static final String FILESIZE = "filesize";
  protected static final String ERROR = "ERROR";
  protected static final String FILESIZE_STATUS = "filesizeStatus";
  protected static final String CHECKSUM_STATUS = "checksumStatus";
  protected static final String METADATA = "metadata";
  protected static final String OK = "OK";
  protected static final String MIMETYPE = "mimetype";
  protected static final String CREATED = "created";
  protected static final String HANDLE = "handle";
  protected static final String HDL_CHECKSUM = "CHECKSUM";
  protected static final String HDL_FILESIZE = "FILESIZE";

  // **
  // STATICS
  // **

  protected static CrudClient tgcrudPublic;
  protected static String tgcrudReindexSecret;
  protected static TGPidService tgpid;
  protected static SearchClient tgsearchPublic;

  /**
   * @param theEndpoint
   * @throws CrudClientException
   * @throws IOException
   */
  protected static synchronized void initTgcrud(final String theEndpoint)
      throws CrudClientException, IOException {
    if (tgcrudPublic == null) {
      if (theEndpoint == null || theEndpoint.isEmpty()) {
        throw new IOException("tgcrud endpoint is not set!");
      }
      URL url = new URL(theEndpoint);
      String urlString = url.toExternalForm();
      tgcrudPublic = new CrudClient(urlString);
    }
  }

  /**
   * @param theEndpoint
   * @throws IOException
   */
  protected static synchronized void initTgpid(final String theEndpoint)
      throws IOException {
    if (tgpid == null) {
      if (theEndpoint == null || theEndpoint.isEmpty()) {
        throw new IOException("tgpid endpoint is not set!");
      }
      tgpid = RestClientBuilder.newBuilder()
          // use httpUrlConnection and http1, see
          // https://cwiki.apache.org/confluence/pages/viewpage.action?pageId=49941
          .property("force.urlconnection.http.conduit", true)
          .baseUri(URI.create(theEndpoint))
          .build(TGPidService.class);
    }
  }

  /**
   * @param theEndpoint
   * @throws IOException
   */
  protected static synchronized void initTgsearch(final String theEndpoint)
      throws IOException {
    if (tgsearchPublic == null) {
      if (theEndpoint == null || theEndpoint.isEmpty()) {
        throw new IOException("tgsearch endpoint is not set!");
      }
      URL url = new URL(theEndpoint);
      String urlString = url.toExternalForm();
      tgsearchPublic = new SearchClient(urlString);
    }
  }

  /**
   * <p>
   * Just normalises a NOT_FOUND string to empty string for better comparison later on.
   * </p>
   * 
   * @param toClean
   * @return
   */
  protected static String normalise(String toClean) {

    String result = toClean;

    if (toClean.equals("NOT_FOUND")) {
      result = "";
    }

    return result;
  }

  /**
   * @param storage
   * @param metadata
   * @param handle
   * @return
   */
  protected static String getCommaSeparatedFilesizeBlock(long storage, long metadata, long handle) {
    return (storage == FILESIZE_EMPTY_OR_NOT_FOUND ? NIX : storage) + ","
        + (metadata == FILESIZE_EMPTY_OR_NOT_FOUND ? NIX : metadata) + ","
        + (handle == FILESIZE_EMPTY_OR_NOT_FOUND ? NIX : handle);
  }

  /**
   * @param storage
   * @param metadata
   * @param handle
   * @return
   */
  protected static String getCommaSeparatedChecksumBlock(String storage, String metadata,
      String handle) {
    return (storage.isEmpty() ? NIX : storage) + ","
        + (metadata.isEmpty() ? NIX : metadata) + ","
        + (handle.isEmpty() ? NIX : handle);
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @return
   */
  public static String getTgcrudReindexSecret() {
    return tgcrudReindexSecret;
  }

  /**
   * @param tgcrudReindexSecret
   */
  public static void setTgcrudReindexSecret(String tgcrudReindexSecret) {
    TGRepConfig.tgcrudReindexSecret = tgcrudReindexSecret;
  }

}
